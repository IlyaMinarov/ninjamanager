--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-0ubuntu0.21.04.1)
-- Dumped by pg_dump version 13.5 (Ubuntu 13.5-0ubuntu0.21.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: equipment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipment (
    id integer NOT NULL,
    name character varying,
    url character varying,
    we_val integer,
    shop_qty integer DEFAULT 0 NOT NULL,
    shop_val integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.equipment OWNER TO postgres;

--
-- Name: item_location; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.item_location (
    id bigint NOT NULL,
    name character varying NOT NULL,
    location character varying NOT NULL,
    chance numeric NOT NULL,
    difficulty character varying,
    note character varying
);


ALTER TABLE public.item_location OWNER TO postgres;

--
-- Name: item_location_sampleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.item_location_sampleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_location_sampleid_seq OWNER TO postgres;

--
-- Name: item_location_sampleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.item_location_sampleid_seq OWNED BY public.item_location.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.items (
    id bigint NOT NULL,
    name character varying,
    url character varying,
    shop_qty integer DEFAULT 0 NOT NULL,
    shop_val integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.items OWNER TO postgres;

--
-- Name: items_sampleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.items_sampleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.items_sampleid_seq OWNER TO postgres;

--
-- Name: items_sampleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.items_sampleid_seq OWNED BY public.items.id;


--
-- Name: material_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.material_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.material_id_seq OWNER TO postgres;

--
-- Name: material_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.material_id_seq OWNED BY public.equipment.id;


--
-- Name: player; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.player (
    id integer NOT NULL,
    discord_id bigint,
    display_name character varying
);


ALTER TABLE public.player OWNER TO postgres;

--
-- Name: player_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.player_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.player_id_seq OWNER TO postgres;

--
-- Name: player_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.player_id_seq OWNED BY public.player.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.queue (
    id integer NOT NULL,
    player_id bigint,
    equipment_id integer,
    clan character varying
);


ALTER TABLE public.queue OWNER TO postgres;

--
-- Name: queue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.queue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queue_id_seq OWNER TO postgres;

--
-- Name: queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.queue_id_seq OWNED BY public.queue.id;


--
-- Name: recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe (
    sampleid bigint NOT NULL,
    product character varying NOT NULL,
    product_qty integer NOT NULL,
    ingr_1 character varying NOT NULL,
    qty_1 integer NOT NULL,
    ingr_2 character varying,
    qty_2 integer,
    ingr_3 character varying,
    qty_3 integer,
    ingr_4 character varying,
    qty_4 integer
);


ALTER TABLE public.recipe OWNER TO postgres;

--
-- Name: recipe_sampleid_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipe_sampleid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_sampleid_seq OWNER TO postgres;

--
-- Name: recipe_sampleid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipe_sampleid_seq OWNED BY public.recipe.sampleid;


--
-- Name: warzone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.warzone (
    id integer NOT NULL,
    grid integer,
    zone integer,
    name character varying,
    defeated boolean,
    health_percentage integer,
    image character varying,
    reported boolean,
    rewards character varying,
    condition character varying,
    duration character varying,
    veins character varying,
    valuation numeric
);


ALTER TABLE public.warzone OWNER TO postgres;

--
-- Name: warzone_damage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.warzone_damage (
    player_id bigint NOT NULL,
    warzone integer,
    damage integer,
    clan character varying(255) NOT NULL
);


ALTER TABLE public.warzone_damage OWNER TO postgres;

--
-- Name: warzone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.warzone_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.warzone_id_seq OWNER TO postgres;

--
-- Name: warzone_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.warzone_id_seq OWNED BY public.warzone.id;


--
-- Name: equipment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipment ALTER COLUMN id SET DEFAULT nextval('public.material_id_seq'::regclass);


--
-- Name: item_location id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_location ALTER COLUMN id SET DEFAULT nextval('public.item_location_sampleid_seq'::regclass);


--
-- Name: items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items ALTER COLUMN id SET DEFAULT nextval('public.items_sampleid_seq'::regclass);


--
-- Name: player id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.player ALTER COLUMN id SET DEFAULT nextval('public.player_id_seq'::regclass);


--
-- Name: queue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.queue ALTER COLUMN id SET DEFAULT nextval('public.queue_id_seq'::regclass);


--
-- Name: recipe sampleid; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe ALTER COLUMN sampleid SET DEFAULT nextval('public.recipe_sampleid_seq'::regclass);


--
-- Name: warzone id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warzone ALTER COLUMN id SET DEFAULT nextval('public.warzone_id_seq'::regclass);


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.equipment (id, name, url, we_val, shop_qty, shop_val) FROM stdin;
752	Anubi's Mask	\N	\N	0	1500
753	Anubi's Scale	\N	\N	0	1500
754	Anubi's Flail	\N	\N	0	1500
755	Divine Magic Cube	\N	\N	0	1500
312	Monkey Tail		\N	0	1500
685	Level 40 Shisui	\N	\N	0	1500
684	Level 40 Sasuke	\N	\N	0	1500
601	Krakens Tentacle		\N	0	1500
683	Level 25 Sakon	\N	\N	0	1500
682	Level 75 Nagato	\N	\N	0	1500
681	Level 10 Tailed Host: Gyuki	\N	\N	0	1500
680	Level 10 Tailed Host: Chomei	\N	\N	0	1500
679	Level 10 Tailed Host: Saiken	\N	\N	0	1500
678	Level 10 Tailed Host: Kokuo	\N	\N	0	1500
677	Level 10 Tailed Host: Son Goku	\N	\N	0	1500
676	Level 10 Tailed Host: Isobu	\N	\N	0	1500
269	Level 20 Hiashi		\N	0	1500
675	Level 10 Tailed Host: Matatabi	\N	\N	0	1500
674	Level 10 Tailed Host: Shukaku	\N	\N	0	1500
270	Level 28 Neji		\N	0	1500
271	Level 33 Hinata		\N	0	1500
272	Level 25 Hashirama		\N	0	1500
273	Level 10 Mangekyou Sharingan		\N	0	1500
606	Midgard Mail		\N	0	1500
607	Minotaurs Horn		\N	0	1500
608	Mjölnir		\N	0	1500
609	Munins Talon		\N	0	1500
610	Nibelungs Ring		\N	0	1500
611	Niddhoggrs Claw		\N	0	1500
612	Origin Bone Scepter		\N	0	1500
613	Pandemonium Flute		\N	0	1500
614	Piezoelectric Clock		\N	0	1500
615	Python Robe		\N	0	1500
616	Rhinoceros Shell		\N	0	1500
617	Scrolls of Sutra		\N	0	1500
618	Skull Helmet		\N	0	1500
673	Level 40 Obito	\N	\N	0	1500
619	Snakeskin Seal		\N	0	1500
620	Snakeskin Shako		\N	0	1500
621	Sunpierce Sword		\N	0	1500
622	Tiamats Tiara		\N	0	1500
238	Kabutowari		\N	0	1500
239	Kiba		\N	0	1500
655	Swift Strikes		\N	0	1500
240	Kohaku no Johei		\N	0	1500
241	Kokinjo		\N	0	1500
242	Kubikiribocho		\N	0	1500
243	Kurosawa		\N	0	1500
244	Kusanagi		\N	0	1500
245	Mage Masher		\N	0	1500
758	Houyi's Bow	\N	\N	0	1500
759	Black Dragon Spear	\N	\N	0	1500
247	Nuibari		\N	0	1500
266	Level 8 Giant Spider		\N	0	1500
267	Level 8 Wolf		\N	0	1500
268	Level 20 Hanabi		\N	0	1500
85	Octopus Arm	\N	750	0	375
90	Ouroboros Stone	\N	1250	0	625
107	Sea Gods Tear	\N	1667	0	834
111	Sharingan Eye	\N	1200	0	600
625	Websealed Talisman		\N	0	1500
626	Winged Mask		\N	0	1500
627	Xiantians Divine Axe		\N	0	1500
535	ANBU Mask		\N	0	1500
536	ANBU Vest		\N	0	1500
274	Level 10 Crow		\N	0	1500
275	Level 10 Genjutsu Crow		\N	0	1500
276	Level 20 Kakashi		\N	0	1500
672	Level 40 Madara	\N	\N	0	1500
277	Level 27 Shisui		\N	0	1500
278	Level 28 Sasuke		\N	0	1500
279	Level 33 Itachi		\N	0	1500
280	Level 37 Obito		\N	0	1500
281	Level 43 Tobi		\N	0	1500
630	Snake Perseverance		\N	0	800
53	Fiver Tail	\N	1500	0	750
54	Gaia Artifact	\N	1500	0	750
58	Hashiramas DNA	\N	2000	0	1000
296	Level 20 Hiruzen		\N	0	1500
671	Level 25 Kidomaru	\N	\N	0	1500
297	Level 20 Mei		\N	0	1500
298	Level 20 Nekomata		\N	0	1500
670	Level 25 Jirobu	\N	\N	0	1500
299	Level 20 Hanzo		\N	0	1500
669	Level 40 Itachi	\N	\N	0	1500
668	Level 20 Haku	\N	\N	0	1500
667	Level 40 Cursed Kimimaro	\N	\N	0	1500
666	Level 35 Orochimaru	\N	\N	0	1500
665	Level 15 Guren	\N	\N	0	1500
664	Level 20 Jugo	\N	\N	0	1500
696	Level 15 Sakon	\N	\N	0	1500
695	Level 65 Toneri	\N	\N	0	1500
694	Level 40 Killer Bee	\N	\N	0	1500
693	Level 40 Fu	\N	\N	0	1500
692	Level 40 Utakata	\N	\N	0	1500
691	Level 40 Han	\N	\N	0	1500
690	Level 40 Roshi	\N	\N	0	1500
689	Level 40 Yagura	\N	\N	0	1500
688	Level 40 Yugito	\N	\N	0	1500
300	Level 20 Danzo		\N	0	1500
687	Level 40 Gaara	\N	\N	0	1500
301	Level 20 Chiyo		\N	0	1500
686	Level 25 Tayuya	\N	\N	0	1500
751	Level 10 Susanoo Spirit	\N	\N	0	1500
302	Level 20 Orochimaru		\N	0	1500
599	Jingu Bang		\N	0	1500
294	Level 20 Sakumo		\N	0	1500
663	Level 37 Himawari	\N	\N	0	1500
34	Cracked ANBU Mask	\N	17	0	9
124	Stone Tablet	\N	1000	0	500
36	Curse Mark DNA	\N	625	0	313
284	Level 5 Giant Snake		\N	0	1500
602	Leviathans Fin		\N	0	1500
230	Crustacean Knuckles		\N	0	1500
231	Cursed Chakram		\N	0	1500
232	Cursed Scroll		\N	0	1500
234	Demonic Flute		\N	0	1500
235	Eye Scope		\N	0	1500
148	Dragon Bone	\N	500	0	250
6	Ancient Blood	\N	167	0	84
248	Nunoboko		\N	0	1500
249	Occult Grimoire		\N	0	1500
250	Oxygen Mask		\N	0	1500
251	Rhi Knuckles		\N	0	1500
252	Samehada		\N	0	1500
303	Level 20 Onoki		\N	0	1500
572	Chakra Exosuit		\N	0	1550
639	Regeneration		1600	0	800
594	Hugins Talon		\N	0	1500
603	Lichs Vestment		\N	0	1500
604	Mangekyo Seal		\N	0	1500
605	MBF-02		\N	0	1500
66	Inferno Artifact	\N	1500	0	750
71	Iron Tail	\N	1500	0	750
75	Lifestream	\N	1667	0	834
78	Mangekyo Eye	\N	3000	0	1500
80	Mimirwater	\N	1100	0	550
661	Crow Ephemera		3000	0	1500
656	Fiery Mane		\N	0	800
727	Scorch Release	\N	\N	0	1500
262	White Light Blade		\N	0	1500
726	Sasuke's Mangekyo Sharingan	\N	\N	0	1500
725	Sakon's Cursed Seal	\N	\N	0	1500
263	Yagyu Shuriken		\N	0	1500
264	Yata Mirror		\N	0	1500
305	Reincarnation Ether		3000	0	1500
724	Rinnegan	\N	\N	0	1500
723	Perfected Host: Gyuki	\N	\N	0	1500
722	Perfected Host: Chomei	\N	\N	0	1500
112	Slime Tail	\N	1400	0	700
16	Beast Soul	\N	1250	0	625
17	Beetle Tail	\N	1400	0	700
27	Celestial Silk	\N	1000	0	500
282	Level 9 Lesser Cursed Seal BL		\N	0	1500
283	Level 7 Chimera		\N	0	1500
591	Heart of Sound		\N	0	1500
592	Heart of Stone		\N	0	1500
593	Heretical God Seed		\N	0	1500
125	Storage Cell Key	\N	10	0	5
595	Icarus Wings		\N	0	1500
596	Inksealed Talisman		\N	0	1500
729	Level 10 Sharingan Bloodline	\N	\N	0	1500
730	Level 10 Byakugan	\N	\N	0	1500
217	Essence Of Origin		100	0	200
15	Babylonian Stone	\N	500	0	250
87	Osteoblast	\N	833	0	417
7	Ancient Coin	\N	500	0	250
637	Chakra Knead		\N	0	200
169	Vajra		\N	0	200
150	Dharma Spirit	\N	300	0	150
110	Serpent Soul	\N	333	0	167
60	Holy Water	\N	250	0	125
721	Perfected Host: Saiken	\N	\N	0	1500
720	Perfected Host: Kokuo	\N	\N	0	1500
719	Perfected Host: Son Goku	\N	\N	0	1500
718	Perfected Host: Isobu	\N	\N	0	1500
717	Perfected Host: Matatabi	\N	\N	0	1500
716	Perfected Host: Shukaku	\N	\N	0	1500
715	Obito's Mangekyo Sharingan	\N	\N	0	1500
714	Magnet Release	\N	\N	0	1500
713	Madara's Mangekyo Sharingan	\N	\N	0	1500
712	Lava Release	\N	\N	0	1500
711	Kidomaru's Cursed Seal	\N	\N	0	1500
710	Jirobo's Cursed Seal	\N	\N	0	1500
709	Itachi's Mangekyo Sharingan	\N	\N	0	1500
708	Ice Release	\N	\N	0	1500
707	Ghost Release	\N	\N	0	1500
706	Explosion Release	\N	\N	0	1500
705	Eternal Mangekyo Sharingan	\N	\N	0	1500
306	Czarwood		\N	0	1500
307	Divine Iron		\N	0	1500
600	Jörmungandrs Skull		\N	0	1500
4	Altuna Rune	\N	1500	0	750
704	Dust Release	\N	\N	0	1500
22	Bolt Artifact	\N	1500	0	750
57	Gundam Core	\N	1250	0	625
65	Indras Chakra	\N	2400	0	1200
105	Ryumyaku Source	\N	1400	0	700
143	Yliaster	\N	500	0	250
39	Damascus Steel	\N	1000	0	500
285	Level 5 Wolf		\N	0	1500
286	Level 8 Life Dragon		\N	0	1500
287	Level 10 Chomei		\N	0	1500
288	Level 10 Kokuo		\N	0	1500
289	Level 10 Shukaku		\N	0	1500
290	Level 10 Son Goku		\N	0	1500
291	Level 10 Mecha Mukade		\N	0	1500
292	Level 10 Umibozu		\N	0	1500
293	Level 10 Saiken		\N	0	1500
5	ANBU Paint	\N	20	0	10
703	Dead Bone Release	\N	\N	0	1500
8	Angel Feather	\N	1000	0	500
702	Dark Release	\N	\N	0	1500
701	Cursed Seal	\N	\N	0	1500
700	Crystal Release	\N	\N	0	1500
699	Byakugan	\N	\N	0	1500
698	Boil Release	\N	\N	0	1500
697	Berserker Release	\N	\N	0	1500
14	Atlantic Artifact	\N	1500	0	750
265	Alkahest		\N	0	1
295	Level 20 A		\N	0	1500
538	Cold Kunai		\N	0	75
42	Deathly Dust	\N	333	0	167
141	Yggdrasil Sap	\N	300	0	150
45	Dojutsu Catalyst	\N	200	0	150
597	Jade Emeperors Pagoda		\N	0	1500
219	Essence of Origin		100	0	200
220	Hyuuga Blood		50	0	25
598	Jibrils Garb		\N	0	1500
222	Ice Shard		100	0	50
224	Rune Ward		100	0	50
225	Arcane Grimoire		\N	0	1500
226	Bashosen		\N	0	1500
227	Benihisago		\N	0	1500
228	Chakra Fruit		\N	0	1500
229	Chimera Cloak		\N	0	1500
253	Sealing Sword Scroll 		\N	0	1500
254	Shibuki		\N	0	1500
255	Shichiseiken		\N	0	1500
256	Sinister Shuriken		\N	0	1500
257	Skeletal Staff		\N	0	1500
258	Spore Gauntlet		\N	0	1500
259	Steam Armor		\N	0	1500
260	Stone of Gelel		\N	0	1500
261	Totsuka		\N	0	1500
660	Biju Brownies	\N	150	0	75
571	Bone Armor		\N	0	1500
197	Chimera Burst		\N	0	200
579	Fafnirs Scales		\N	0	1500
580	Fenrirs Fang		\N	0	1500
581	Ghost Cape		\N	0	1500
582	Gungnir		\N	0	1500
583	Heart of ANBU		\N	0	1500
584	Heart of Cloud		\N	0	1500
585	Heart of Leaf		\N	0	1500
586	Heart of Mist		\N	0	1500
587	Heart of Neko		\N	0	1500
588	Heart of Rain		\N	0	1500
589	Heart of Root		\N	0	1500
590	Heart of Sand		\N	0	1500
728	Ink Tail	\N	\N	0	1400
157	Lucky Talisman		\N	0	100
30	Chimera DNA	\N	500	0	250
41	Darkwood	\N	625	0	313
59	Heart Container	\N	500	0	250
731	Coral Tail	\N	\N	0	1500
732	Blue Flame Tail	\N	\N	0	1500
311	Dao Dragon Pill		\N	0	500
151	Spirit Container	\N	1000	0	500
734	Wood Release	\N	\N	0	1500
735	Twin Release	\N	\N	0	1500
736	Tenseigan	\N	\N	0	1500
737	Tailed Host: Gyuki	\N	\N	0	1500
738	Tailed Host: Chomei	\N	\N	0	1500
739	Tailed Host: Saiken	\N	\N	0	1500
740	Tailed Host: Kokuo	\N	\N	0	1500
741	Tailed Host: Son Goku	\N	\N	0	1500
742	Tailed Host: Isobu	\N	\N	0	1500
743	Tailed Host: Matatabi	\N	\N	0	1500
744	Tailed Host: Shukaku	\N	\N	0	1500
745	Tayuya's Cursed Seal	\N	\N	0	1500
746	Swift Release	\N	\N	0	1500
747	Steel Release	\N	\N	0	1500
748	Smoke Release	\N	\N	0	1500
749	Shisui's Mangekyo Sharingan	\N	\N	0	1500
750	Sharingan	\N	\N	0	1500
733	Level 10 Crystal Dragon	\N	\N	0	1500
563	Abaddons Armory		\N	0	1500
564	Arachnes Arbalest		\N	0	1500
565	Arctic Battlearmor		\N	0	1500
566	Argent White Sword		\N	0	1500
567	Artificial Bloodline Core		\N	0	1500
568	Bahamuts Mane		\N	0	1500
569	Blades of Kali		\N	0	1500
570	Bloodsealed Talisman		\N	0	1500
573	Chakra Shield		\N	0	1500
574	Cursed Ring		\N	0	1500
575	Daedalus Cuirass		\N	0	1500
576	Divine Khakkara		\N	0	1500
577	Dracolichs Phylactery		\N	0	1500
146	Imperial Jade	\N	500	0	250
649	Beast Sealing Chains		\N	0	150
129	Thunder Stone	\N	250	0	125
93	Quartz Crystal	\N	100	0	50
73	Lava	\N	20	32	13
1	Adamantine Bone	\N	33	1	17
132	Uchiha Blood	\N	50	3	25
99	Roran Coin	\N	50	9	25
50	Essence of Smoke	\N	100	29	50
49	Essence of Darkness	\N	100	14	50
26	Celestial Dye	\N	100	6	75
543	Poison Shuriken		\N	3	175
541	Jade Dagger		\N	11	250
128	Talatat Stone	\N	150	4	75
183	Snakeskin Slippers		\N	4	75
534	Adamantine Kunai		\N	4	244
46	Dragon Blood	\N	100	1	100
210	Charm of Protection		\N	61	50
203	Weapons Quiver		\N	7	94
116	Soil Stone	\N	250	1	125
23	Bone	\N	6	1	3
44	Dice	\N	17	8	9
633	Cracked Anbu Mask		18	49	9
35	Crow Feather	\N	17	2	9
79	Mercury	\N	25	4	13
91	Poison Vial	\N	33	40	17
123	Stem Cell	\N	50	15	25
108	Serpent Horn	\N	33	13	17
119	Spider Silk	\N	500	5	250
102	Rune Word	\N	25	9	13
55	Ghost Spirit	\N	25	2	13
97	Robot Parts	\N	33	19	17
12	Artificial Chakra Core	\N	50	57	25
135	White Diamond	\N	100	17	50
51	Essence of Swiftness	\N	100	31	50
33	Clear Water	\N	7	1	4
109	Serpent Scales	\N	25	30	13
13	Ascension Ore	\N	50	5	50
89	Otsutsuki Cells	\N	100	2	100
212	Anbu Paint		\N	36	10
166	Sparkless Earth Shard		\N	6	15
19	Black Receiver	\N	33	90	17
153	Jinchuriki Chakra	\N	100	9	50
651	Anbu Vest		\N	3	75
182	Lightning Shard		\N	12	75
48	Elvenskin	\N	200	18	100
155	Serpent Burst		\N	5	200
52	Fish Scales	\N	6	22	3
32	Clay	\N	7	7	4
40	Dark Chakra Eel	\N	14	46	7
21	Blood Vessel	\N	33	30	17
20	Black Threads	\N	17	5	9
67	Ink Bottle	\N	20	9	10
96	Refined Chakra Plate	\N	83	11	42
145	Ghost Orb	\N	250	1	125
186	Harden		\N	3	200
138	Wood	\N	8	1	4
29	Chakra Plate	\N	10	7	5
24	Bronze Coin	\N	13	29	7
70	Iron Sand	\N	50	8	25
113	Snake Eye	\N	50	5	25
77	Magma	\N	200	9	100
144	Lotus Amulet	\N	400	11	200
628	Focus Counter		\N	8	400
137	Wolf Pelt	\N	10	3	5
149	Dharma Wheel	\N	50	19	25
72	Jutsu Scroll	\N	10	10	5
131	Turtle Shell	\N	11	10	6
657	Biju Coral		\N	26	75
56	Granite	\N	33	22	17
81	Mind Awakening Pill	\N	50	1	25
76	Lion Fur	\N	14	47	7
204	Spoiled Soldier Pill		\N	4	8
154	Jinchuriki Seal	\N	25	1	13
84	Nature Transformation Chakra	\N	100	42	50
652	Biju Silk		\N	27	75
650	Biju Meat		\N	26	75
86	Opal	\N	200	6	100
127	Tainted Chakra	\N	25	18	13
94	Quicklime	\N	50	10	25
37	Curse Mark Pattern	\N	50	1	25
214	Bezoar	\N	200	68	50
147	Jade	\N	50	1	25
43	Demon Wing	\N	100	24	50
64	Ice Shards	\N	100	39	50
18	Berserker Enzyme	\N	42	10	21
139	Wooden Talisman	\N	50	16	25
578	Electrum Coin		\N	0	375
213	Soldier Pill		\N	5	34
654	Biju Acid		150	12	75
206	Twisted Mask		\N	10	54
165	Earth Shard		\N	10	75
659	Biju Horn	\N	150	21	75
101	Runestone	\N	250	2	125
104	Rusty Spring	\N	6	5	3
63	Ice Horn	\N	10	13	5
156	Azoth		\N	5	50
537	Bionic Shinobi Gauntlet		\N	9	350
643	Memory Recall		800	12	400
106	Salamander Venom	\N	125	19	63
647	Biju Flame		150	22	75
539	Elemental Puppet		\N	7	100
167	Genjutsu Pill		\N	2	45
11	Arachne Web	\N	250	10	125
196	Hero Water		\N	11	53
114	Snake Serum	\N	500	2	250
83	Muga Silk	\N	100	13	50
115	Snakeskin	\N	13	2	7
100	Ruby	\N	100	26	50
68	Iron	\N	14	2	7
304	Panacea		\N	67	25
103	Runic Talisman	\N	150	5	75
662	Laser Beam		400	2	200
62	Hyuga Blood	\N	50	41	25
640	Wind Shard		150	10	75
631	Elemental Rune		\N	7	400
547	Snakeskin Suit		\N	1	206
47	Dragon Soul	\N	416	2	208
28	Ceremonial Mask	\N	200	11	100
658	Biju Fur	\N	150	19	75
187	Poison Puppet		\N	6	113
177	Detonating Clay		\N	4	125
74	Leather	\N	8	7	4
38	Cursed Feather	\N	25	6	13
188	Illusion Pendant		\N	8	55
134	Venoxin Mutagen	\N	420	8	210
546	Serpent Pike		\N	4	269
82	Molten Stone	\N	250	2	125
171	Chimera Gloves		\N	12	75
10	Aqua Stone	\N	250	16	125
121	Steam Stone	\N	250	1	125
9	Antique Mask	\N	20	35	10
176	Tortoise Carapace		\N	2	200
159	Dirty Dice		\N	1	20
540	Green Jumpsuit		\N	3	275
549	Tortoise Mask		\N	7	125
205	Jade Awl		\N	7	250
184	Thunder Discharge		\N	2	200
95	Red Sand	\N	10	2	5
172	Ghost Gloves		\N	9	95
211	Fuma Shuriken		\N	8	49
98	Robot Processor	\N	500	12	250
175	Flak Jacket		\N	6	25
142	Zephyr Leaf	\N	100	11	50
653	Biju Sand		\N	2	75
193	Snake Stamina		\N	5	200
69	Ironbark	\N	125	1	63
133	Uchiha Crest	\N	50	6	25
178	Prototype Chakra Helmet		\N	11	50
174	Sparkless Fire Shard		\N	5	15
140	Yggdrasil Bark	\N	200	1	100
126	Susanoo Residue	\N	200	1	200
545	Serpent Armor		\N	3	225
199	Water Shard		\N	9	75
2	Aero Stone	\N	250	4	125
623	Valhalla Helm		\N	6	775
641	Drench		1600	1	800
92	Puppet Part	\N	10	32	5
548	Tainted Shuriken		\N	6	63
756	Kraken Camouflage		1600	1	800
120	Spider Web	\N	10	2	5
189	Sparkless Water Shard		\N	4	15
550	Turtle Shield		\N	5	42
118	Spider Gland	\N	1000	2	500
757	Golden Gravity		1600	1	800
645	Adamantine Erosion		800	3	400
638	Guiding Wings		800	4	400
202	Magnetic Field		\N	6	400
551	Wolf Mantle		\N	7	125
180	Weapons Belt		\N	5	19
173	Anbu Mask		\N	10	133
648	Poison Wound		\N	3	200
209	Dire Howl		\N	4	200
201	Poison Coating		\N	2	200
168	Thunder God Kunai		\N	5	56
158	Fire Shard		\N	9	75
31	Chimera Heart	\N	250	2	125
152	Ivory	\N	25	21	13
190	Serpent Thrust		\N	1	200
163	Sparkless Lightning Shard		\N	4	15
544	Prototype Chakra Armor		\N	2	185
200	Blight Rod		\N	2	108
629	Snake Bite		\N	7	200
185	Thunder Rod		\N	3	103
208	Sour Hero Water		\N	2	11
25	Byakugan Eye	\N	1200	1	600
237	Ice Brand		\N	3	750
179	Bone Pendant		\N	2	550
122	Steel	\N	125	0	63
136	Wolf Eye	\N	1000	0	500
642	Ghost Touch		400	0	200
181	Illusionary Entrance		\N	0	200
207	Frozen Havoc		\N	2	200
308	Monkey King Fur		400	11	200
162	Undying Mask		\N	3	93
191	Turtle Defense		\N	3	200
170	Prototype Chakra Gauntlet		\N	5	20
542	Mecha Puppet		\N	2	375
194	Shuriken		\N	5	15
164	Bronze Token		\N	4	25
644	Chakra Detonation		400	2	200
161	Sparkless Wind Shard		\N	2	15
160	Reroll Counter		\N	5	400
195	Chimera Gauntlets		\N	4	206
632	Self-destruction Laser		\N	1	200
635	Infectious Strike		\N	4	200
636	Mind Recovery		\N	5	400
198	Tanto		\N	4	38
192	Artificial Rasengan		\N	3	400
233	Dark Orb		\N	2	750
309	Python Longevity Serum		\N	1	600
646	Muscle Hardening		800	4	400
246	Molten Hammer		\N	1	750
130	Tundra Artifact	\N	1500	1	750
624	Warg Talisman		\N	1	560
88	Osteoclast	\N	833	1	417
236	Firestorm Staff		\N	1	750
634	Last Luck		\N	2	200
\.


--
-- Data for Name: item_location; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.item_location (id, name, location, chance, difficulty, note) FROM stdin;
823	Adamantine Bone	Immortal Beast Cavern - Giant Multi-Headed Dog	15	\N	\N
824	Aero Stone	Forbidden Temple: Outer - Young and Temari	2	\N	\N
825	ANBU Paint	ANBU Hideout 3/7	25	\N	\N
826	Ancient Blood	Blood Prison 11/13	3	\N	\N
827	Ancient Coin	Akatsuki Temple 8/10	0.50	\N	Increased Drop on Hard/Impo
828	Antique Mask	Blood Prison 7/13	25	\N	\N
829	Artificial Chakra Core	Snow Country 7/7	8	\N	\N
830	Ascension Ore	Forbidden Temple: Inner - Madara	12	\N	\N
831	Berserker Enzyme	Frozen Island 5/10	10	\N	\N
832	Bezoar	Island Turtle - Hachibis Hut 6/6	3	\N	\N
833	Black Receiver	Island Turtle - Nagato	21	\N	\N
834	Black Threads	Akatsuki Temple 8/10	36	\N	\N
835	Blood Vessel	Forbidden Temple: Outer - Karin	18	\N	\N
836	Bone	Forest of Death 2/8	80	\N	\N
837	Bronze Coin	Hot Water Village 13/13	40	\N	\N
838	Celestial Dye	Ruins of Roran - Hard Path Sara	3	\N	\N
839	Celestial Dye	Kaguya Castle 1/6	6	\N	Increases 1% each Difficulty
840	Chakra Plate	Snow Country 4/7	50	\N	\N
841	Chimera DNA	Mount Shumisen 5/12	1	\N	\N
842	Chimera Heart	Mount Shumisen 11/12	2	\N	Increases 0.25% each Difficulty
843	Clay	Ruins of Roran 3/13	70	\N	\N
844	Clear Water	Hot Water Village 9/13	60	\N	\N
845	Cracked ANBU Mask	Frozen Island 8/10	24	\N	\N
846	Cracked ANBU Mask	ANBU Hideout 4/7	35	\N	\N
847	Crow Feather	ANBU Hideout 6/7	30	\N	\N
848	Curse Mark Pattern	Forest of Death 1/8	8	\N	\N
849	Cursed Feather	Moryo Shrine 13/13	20	\N	\N
850	Dark Chakra Eel	Moryo Shrine 2/13	35	\N	\N
851	Deathly Dust	The Sealed World 8/12	1.5	\N	\N
852	Demon Wing	Blood Prison 13/13	5	\N	\N
853	Dharma Wheel	Hidden Leaf - Fire Monastery 1/6	10	Extreme	\N
854	Dharma Wheel	Kamuis Dimension	15	Extreme	\N
855	Dice	Blood Prison 4/13	30	\N	\N
856	Dojutsu Catalyst	Kamuis Dimension 5/10	1	\N	Increases 1% each Difficulty
857	Dragon Blood	Forbidden Temple: Center - Kabuto	6	Hard	\N
858	Dragon Soul	Ryuichi Cave 10/10	1.75	\N	Increases 0.25% each Difficulty
859	Essence of Darkness	Frozen Island 7/10	4	\N	\N
860	Essence of Darkness	Mount Shumisen 12/12	3	\N	\N
861	Essence of Origin	Forbidden Temple: Inner - Naruto	6	Forbidden	\N
862	Essence of Smoke	Cafe Gero Gero - Gamabunta	12	\N	\N
863	Essence of Swiftness	Mount Shumisen 12/12	3	\N	\N
864	Essence of Swiftness	Island Turtle - Hachibis Hut 4/6	6	\N	\N
865	Fish Scales	Hot Water Village 1/13	80	\N	\N
866	Ghost Spirit	Hot Water Village 5/13	25	\N	\N
867	Granite	Moryo Shrine 1/13	15	\N	\N
868	Gundam Core	Ruins of Roran 7/13	0.40	\N	Increased drop chance on Medium and Extreme
869	Hashiramas DNA	Mountains Graveyard 5/10	0.20	\N	Increases to 0.40% on Medium
870	Holy Water	Valley of the End 1/4	2.80	\N	\N
872	Hyuuga Blood	Forbidden Temple: Outer - Young and Adorable	4	\N	Increases to 6% on easy
873	Hyuuga Blood	Forbidden Temple: Outer - Young and Shy	8	Medium	Increases to 10% on hard
874	Hyuuga Blood	Forbidden Temple: Outer - Young and Neji	12	Extreme	\N
875	Ice Horn	Frozen Island 1/10	40	\N	\N
876	Ice Shard	Frozen Island - Hard Path Haku	5	\N	\N
877	Indras Chakra	Forbidden Temple: Center - Madara	0.25	Hard	\N
878	Ink Bottle	Forbidden Temple: Outer - Young and Sai	30	\N	\N
879	Iron	Snow Country 5/7	28	\N	\N
880	Iron	Land of Iron 2/11	45	\N	\N
881	Ivory	Island Turtle - Hachibis Hut 5/6	24	\N	\N
882	Jade	Hot Water Village 7/13	10	Medium	\N
883	Jutsu Scroll	Forbidden Temple: Outer  - Young and Tenten	60	\N	\N
884	Lava	Moryo Shrine 5/13	25	\N	\N
885	Leather	Frozen Island 2/10	40	\N	\N
886	Lion Fur	Mount Shumisen 2/12	35	\N	\N
887	Mercury	Forbidden Temple: Outer - Young and Miserable	24	\N	\N
888	Mind Awakening Pill	Land of Iron 6/11	14	\N	\N
889	Mind Awakening Pill	Forbidden Temple: Outer - Young and Ambitious	12	\N	\N
890	Muga Silk	Snow Country - Hard Path Koyuki	4	\N	\N
891	Muga Silk	Ruins of Roran 2/13	5	\N	\N
892	Nature Transformation Chakra	Land of Iron 10/11	8	\N	\N
893	Nature Transformation Chakra	Forbidden Temple: Outer - Young and Yamato	6	\N	\N
894	Octopus Arm	Fort Jinchuuriki - Killer Bee, Blue B	0.1	\N	100% on first drop
895	Otsutsuki Cells	Kaguyas Castle 3/6	2	\N	Increases by 1% each Difficulty
896	Poison Vial	Frozen Island 3/10	12	\N	\N
897	Puppet Part	Ruins of Roran 13/13	50	\N	\N
898	Quartz Crystal	Ruins of Roran 8/13	5	\N	\N
899	Quicklime	Moryo Shrine 6/13	10	\N	\N
900	Red Sand	Ruins of Roran 4/13	50	\N	\N
901	Robot Parts	Ruins of Roran 6/13	15	\N	\N
902	Roran Coin	Ruins of Roran 5/13	10	\N	\N
903	Ruby	Forbidden Temple: Outer - Young and Sakura	6	\N	\N
905	Rusty Spring	Ruins of Roran 1/13	80	\N	\N
906	Salamander Venom	Land of Iron 7/11	4.5	\N	Increases 0.5% each Difficulty
907	Serpent Horn	Moryo Shrine 8/13	15	\N	\N
908	Serpent Scales	Moryo Shrine 4/13	20	\N	\N
909	Serpent Soul	Moryo Shrine 12/13	1.5	\N	\N
910	Snake Eye	Forest of Death 3/8	4	\N	\N
911	Snake Eye	Ryuichi Cave 5/10	22	\N	\N
912	Snake Serum	Forest of Death 8/8	0.8	\N	\N
913	Snake Serum	Ryuichi Cave 8/10	2	\N	Increases 0.25% each Difficulty
914	Snakeskin	Forest of Death 3/8	16	\N	\N
915	Snakeskin	Forest of Death 6/8	32	\N	\N
916	Snakeskin	Ryuichi Cave 6/10	75	\N	\N
904	Rune Word	Snow Country 6/7	16	Medium	\N
917	Soil Stone	Forbidden Temple: Outer - Young and Hated	2.4	\N	\N
918	Spider Web	Ruins of Roran 11/13	50	\N	\N
919	Steam Stone	Hot Water Village 3/13	2	\N	\N
920	Stem Cell	Forbidden Temple: Outer - Young and Kabuto	15	\N	\N
921	Stem Cell	Frozen Island 4/11	7	Medium	\N
922	Storage Cell Key	Island Turtle - Hachibis Hut	100	\N	Varies
923	Susanoo Residue	Mountains Graveyard 6/10	1.5	\N	Up to 3.5% on later difficulties
924	Tainted Chakra	Blood Prison 12/13	20	\N	\N
925	Thunder Stone	Bloody Mist Village 5/12	2	\N	\N
926	Turtle Shell	Hot Water Village 4/13	45	\N	\N
927	Uchiha Blood	Forbidden Temple: Outer - Young and Tainted	4	\N	6% on Easy
928	Uchiha Blood	Forbidden Temple: Outer - Young and Itachi	8	Medium	10% on Hard
929	Uchiha Blood	Forbidden Temple: Outer - Young and Vengeful	12	Extreme	\N
930	Uchiha Crest	Moryo Shrine 11/13	10	\N	\N
931	Uchiha Crest	Moryo Shrine 11/13 - Hard Path	15	\N	\N
932	White Diamond	Mount Shumisen 1/12	5	\N	\N
933	Wolf Pelt	Snow Country 2/7	40	\N	\N
934	Wolf Pelt	Land of Iron 1/11	72	\N	\N
935	Wood	ANBU Hideout 2/7	70	\N	\N
936	Wood	The Sealed World 7/12	88	\N	\N
937	Zephyr Leaf	ANBU Hideout 1/7	5	\N	\N
938	Arcane Grimoire	Island Turtle 2/15	0.12	Hard	\N
939	Bashosen	The Sealed World 2/11	0.08	\N	Drops to 0.04 in Medium
940	Benihisago	Mount Shumisen 10/12	0.06	Easy	\N
941	Chakra Fruit	God Tree 5/5	0.01	\N	\N
942	Chimera Cloak	Mount Shumisen 3/12	0.06	\N	\N
943	Crustacean Knuckles	Immortal Beast Cavern 1/16	0.12	Easy	\N
944	Cursed Chakram	Forest of Death 5/8	0.03	\N	\N
945	Cursed Scroll	Forest of Death 8/8 - Hard Path (Kabuto)	0.06	\N	\N
946	Dark Orb	Frozen Island 6/10	0.2	\N	\N
947	Demonic Flute	Ryuichi Cave 4/10	0.1	\N	\N
948	Eye Scope	Akatsuki Temple 2/11	0.1	\N	\N
949	Firestorm Staff	Moryo Shrine 9/13	0.17	\N	\N
950	Ice Brand	Frozen Island 9/11	0.15	\N	\N
951	Kabutowari	Tower of God - Jinin (2nd Mission)	100	\N	\N
952	Kiba	Tower of God - Ameyuri (2nd Mission)	100	\N	\N
953	Kohaku no Johei	God Tree 1/5	0.1	Extreme	\N
954	Kokinjo	Mount Shumisen 10/12	0.03	Hard	\N
955	Kubikiribocho	Tower of God - Zabuza (2nd Mission)	100	\N	\N
956	Kurosawa	Land of Iron 11/11	0.05	\N	\N
957	Kusanagi	Ryuichi Cave Easy Path 2/10	0.08	Medium	\N
958	Mage Masher	Blood Prison 10/13	0.08	\N	\N
959	Molten Hammer	Bloody Mist Village 1/12 - Jinin	0.2	\N	\N
960	Nuibari	Tower of God - Kushimaru (2nd Mission)	100	\N	\N
961	Nunoboko	Kamuis Dimension 10/10	0.03	Impossible	\N
962	Occult Grimoire	Moryo Shrine 10/13	0.12	Easy	\N
963	Oxygen Mask	Hot Water Village 6/13	0.17	\N	\N
964	Rhi Knuckles	Immortal Beast Cavern 6/16	0.1	Hard	\N
965	Samehada	Tower of God - Fuguki (2nd Mission)	100	\N	\N
966	Sealing Sword Scroll 	Tower of God - Mangetsu (2nd Mission)	100	\N	\N
967	Shibuki	Tower of God - Jinpachi (2nd Mission)	100	\N	\N
968	Shichiseiken	The Sealed World 2/11	0.04	\N	\N
969	Sinister Shuriken	Bloody Mist Village 6/12 - Kushimaru	0.12	Easy	\N
970	Skeletal Staff	Mountains Graveyard 7/10	0.1	\N	\N
971	Spore Gauntlet	Akatsuki Temple 7/11	0.1	\N	\N
972	Steam Armor	Ruins of Roran 10/13	0.1	\N	\N
973	Stone of Gelel	Ruins of Roran 9/13	0.1	\N	\N
974	Totsuka	Mountains Graveyard 8/10	0.03	Extreme	\N
975	White Light Blade	ANBU Hideout 7/7	0.08	\N	\N
976	Yagyu Shuriken	Bloody Mist Village 4/12 - Jinpachi	0.12	Hard	\N
977	Yata Mirror	Mountains Graveyard 8/10	0.06	Hard	Drops to 0.03 on Extreme
871	Holy Water	Mount Shumisen 7/12 - Hardpath (Stolen Hero Water)	2	\N	\N
978	Alkahest	Village Material Shop	100	\N	Costs 1 Gold
979	Azoth	Village Material Shop	100	\N	Costs 200 Gold
980	ANBU Paint	Village Material Shop	100	\N	Costs Gold (varies)
981	Charm of Protection	Village Material Shop	100	\N	Costs Gold (varies)
982	Yliaster	Village Material Shop	100	Forbidden	Costs 500 Gold
983	Panacea	Village Material Shop	100	\N	Costs 100 Gold
993	Biju Meat	Island Turtle - Gyuki	3	Easy	\N
992	Biju Silk	Island Turtle - Chomei	3	Easy	\N
991	Biju Acid	Island Turtle - Saiken	3	Easy	\N
990	Biju Horn	Island Turtle - Kokuo	3	Easy	\N
989	Biju Fur	Island Turtle - Son Goku	3	Easy	\N
988	Biju Coral	Island Turtle - Isobu	3	Easy	\N
987	Biju Flame	Island Turtle - Matatabi	3	Easy	\N
986	Biju Sand	Island Turtle - Shukaku	3	Easy	\N
985	Jinchuriki Seal	Forest of Death 7/8	16	Hard	\N
995	Beast Sealing Chains	Mountains Graveyard 2/10 	1.4	NG	Need more info
994	Beast Sealing Chains	Mountains Graveyard 1/10	2.8	Extreme	Increases 0.4% per diff
984	Jinchuriki Chakra	Kamuis Dimension 6/10	7	Hard	Increases 1% per diff
996	Jinchuriki Chakra	Kamuis Dimension 7/10	7	NG	Need more info
\.


--
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.items (id, name, url, shop_qty, shop_val) FROM stdin;
59	Serpent Burst		0	200
60	Azoth		0	50
1	Lucky Talisman		2	100
2	Fire Shard		3	75
3	Dirty Dice		6	20
4	Reroll Counter		1	400
5	Sparkless Wind Shard		4	15
6	Undying Mask		1	93
7	Sparkless Lightning Shard		2	15
8	Bronze Token		5	25
9	Earth Shard		1	75
10	Sparkless Earth Shard		2	15
11	Genjutsu Pill		7	45
12	Thunder God Kunai		1	56
13	Vajra		1	200
14	Prototype Chakra Gauntlet		1	20
15	Chimera Gloves		4	75
16	Ghost Gloves		3	95
17	Anbu Mask		2	133
18	Sparkless Fire Shard		2	15
19	Flak Jacket		3	25
20	Tortoise Carapace		1	200
21	Detonating Clay		1	125
22	Prototype Chakra Helmet		4	50
23	Bone Pendant		1	550
24	Weapons Belt		1	19
25	Illusionary Entrance		2	200
26	Lightning Shard		2	75
27	Snakeskin Slippers		1	75
28	Thunder Discharge		1	200
29	Thunder Rod		2	103
30	Harden		2	200
31	Poison Puppet		1	113
32	Illusion Pendant		3	55
33	Sparkless Water Shard		1	15
34	Serpent Thrust		1	200
35	Turtle Defense		1	200
36	Artificial Rasengan		2	400
37	Snake Stamina		2	200
38	Shuriken		1	15
39	Chimera Gauntlets		1	206
40	Hero Water		1	53
41	Chimera Burst		1	200
42	Tanto		2	38
43	Water Shard		1	75
44	Blight Rod		2	108
45	Poison Coating		1	200
46	Magnetic Field		1	400
47	Weapons Quiver		1	94
48	Spoiled Soldier Pill		1	8
49	Jade Awl		1	250
50	Twisted Mask		1	54
51	Frozen Havoc		1	200
61	Sour Hero Water		0	11
62	Dire Howl		0	200
63	Charm of Protection		0	50
64	Fuma Shuriken		0	49
65	Anbu Paint		0	10
66	Soldier Pill		0	34
\.


--
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.player (id, discord_id, display_name) FROM stdin;
3	315581177455181846	Swaggy GG5, HG3
7	196766486847553536	headbed
10	319318987563466752	mch wz5
11	169107840638648320	Kai 🧡 GG2 HG2
13	190516666998718464	Ortega c0 💀 ⏳  - GG3 HG2
15	144997676675629056	Certified Impo Runner WZ4
16	206588796198715392	Revenge
17	179802545705713674	Raphael
20	323137764776214529	WarPigsOz
21	244418372257841153	Dizzy Owl
1	449138085733728286	Carnificina
4	344717503441928203	𝓩𝔃𝓪𝓭
12	733622127030173707	Ylli (Nr 1 Uchiha) {GG}
19	304964079896756226	Jet the Red Nosed Pirate
22	219240160649281536	WolvesWithPie WZ2
25	197160269678379010	Apex
6	211282750135730176	soui
23	425718321124737045	Courage
14	171033233050304512	Soifon
5	466654204019212290	Givia
26	144912112060203008	hardensoul
2	155090097799299083	Jax
28	288712595312082944	Kizario - Pirate King
30	1009891939161739314	WingTheory
31	299603019920703488	Freaky
32	311279314098192385	V𝓸𝓻𝓣𝓮𝔁
33	145722488079253505	Swag
34	958277012911443968	Digital Jin
35	275799181774422036	Faberzaum
\.


--
-- Data for Name: queue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.queue (id, player_id, equipment_id, clan) FROM stdin;
2500	190516666998718464	26	GG
2364	466654204019212290	649	HG
2441	171033233050304512	6	GG
1831	155090097799299083	45	HG
2502	171033233050304512	108	GG
2367	211282750135730176	6	HG
2503	171033233050304512	69	GG
2369	211282750135730176	150	HG
2370	211282750135730176	149	HG
2372	211282750135730176	132	HG
2373	211282750135730176	89	HG
2374	211282750135730176	116	HG
2449	190516666998718464	41	GG
2376	211282750135730176	133	HG
2377	211282750135730176	153	HG
2508	958277012911443968	7	HG
2455	145722488079253505	89	GG
2254	466654204019212290	148	HG
2509	958277012911443968	70	HG
2510	958277012911443968	84	HG
2459	171033233050304512	42	GG
1532	206588796198715392	10	HG
2511	958277012911443968	48	HG
2512	958277012911443968	101	HG
2513	958277012911443968	1	HG
2514	958277012911443968	55	HG
2515	319318987563466752	37	GG
2388	197160269678379010	652	GG
2518	344717503441928203	46	HG
2068	344717503441928203	45	HG
2522	197160269678379010	42	GG
2397	211282750135730176	129	HG
2525	197160269678379010	153	GG
2527	206588796198715392	31	HG
2529	155090097799299083	649	GG
2530	197160269678379010	649	GG
2401	344717503441928203	126	GG
2532	275799181774422036	153	HG
2534	197160269678379010	1	GG
2535	344717503441928203	649	GG
2536	344717503441928203	653	GG
2537	344717503441928203	654	GG
2273	466654204019212290	42	HG
2539	319318987563466752	100	GG
2540	319318987563466752	101	GG
406	323137764776214529	148	HG
2541	319318987563466752	47	GG
2149	449138085733728286	31	GG
2001	206588796198715392	42	HG
2542	197160269678379010	154	GG
2543	171033233050304512	116	GG
2544	449138085733728286	150	GG
2485	155090097799299083	132	GG
2545	344717503441928203	60	HG
2411	155090097799299083	657	GG
2488	196766486847553536	26	GG
2489	196766486847553536	83	GG
2490	196766486847553536	38	GG
2546	155090097799299083	45	GG
2492	190516666998718464	83	GG
2493	206588796198715392	116	HG
2547	197160269678379010	45	GG
2548	466654204019212290	153	GG
2549	466654204019212290	126	GG
2550	190516666998718464	146	GG
2551	466654204019212290	47	HG
2552	466654204019212290	46	HG
2289	344717503441928203	126	HG
2553	449138085733728286	149	GG
2498	344717503441928203	77	GG
2431	449138085733728286	89	GG
2103	315581177455181846	657	HG
2433	319318987563466752	81	GG
2435	145722488079253505	46	GG
\.


--
-- Data for Name: recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipe (sampleid, product, product_qty, ingr_1, qty_1, ingr_2, qty_2, ingr_3, qty_3, ingr_4, qty_4) FROM stdin;
1405	Adamantine Kunai	1	Adamantine Bone	18	Steel	3	\N	\N	\N	\N
1406	ANBU Mask	1	ANBU Paint	10	Cracked ANBU Mask	20	\N	\N	\N	\N
1407	ANBU Vest	1	ANBU Paint	10	Flak Jacket	1	\N	\N	\N	\N
1408	Bionic Shinobi Gauntlet	1	Gundam Core	1	Jutsu Scroll	20	Prototype Chakra Gauntlet	1	\N	\N
1409	Blight Rod	1	Poison Vial	8	Wood	20	\N	\N	\N	\N
1410	Bronze Token	1	Bronze Coin	7	\N	\N	\N	\N	\N	\N
1411	Chimera Gauntlets	1	Lion Fur	20	Steel	5	\N	\N	\N	\N
1412	Chimera Gloves	1	Lion Fur	14	Leather	12	\N	\N	\N	\N
1413	Cold Kunai	1	Ice Horn	20	Iron	7	\N	\N	\N	\N
1414	Detonating Clay	1	Clay	28	Lava	4	Quicklime	4	\N	\N
1415	Dirty Dice	1	Dice	4	\N	\N	\N	\N	\N	\N
1416	Elemental Puppet	1	Iron Sand	2	Red Sand	10	Puppet Part	20	\N	\N
1417	Flak Jacket	1	Leather	12	\N	\N	\N	\N	\N	\N
1418	Ghost Gloves	1	Leather	10	Ghost Spirit	12	\N	\N	\N	\N
1419	Green Jumpsuit	1	Elvenskin	2	Essence of Swiftness	2	Muga Silk	5	\N	\N
1420	Illusion Pendant	1	Runic Talisman	1	Dark Chakra Eel	5	\N	\N	\N	\N
1421	Jade Awl	1	Opal	1	Jade	16	\N	\N	\N	\N
1422	Jade Dagger	1	Ruby	2	Jade	16	\N	\N	\N	\N
1423	Lucky Talisman	1	Dice	10	Wooden Talisman	4	\N	\N	\N	\N
1424	Mecha Puppet	1	Robot Processor	1	Robot Parts	30	\N	\N	\N	\N
1425	Poison Puppet	1	Puppet Part	10	Poison Vial	8	Iron Sand	2	\N	\N
1426	Poison Shuriken	1	Salamander Venom	1	Shuriken	1	Snake Serum	1	\N	\N
1427	Prototype Chakra Armor	1	Iron	10	Artificial Chakra Core	8	Chakra Plate	20	\N	\N
1428	Prototype Chakra Gauntlet	1	Chakra Plate	10	\N	\N	\N	\N	\N	\N
1429	Prototype Chakra Helmet	1	Chakra Plate	20	Iron	8	\N	\N	\N	\N
1430	Serpent Armor	1	Serpent Scales	26	Steel	2	\N	\N	\N	\N
1431	Serpent Pike	1	Dark Chakra Eel	10	Ironbark	3	Serpent Horn	17	\N	\N
1432	Shuriken	1	Iron	4	\N	\N	\N	\N	\N	\N
1433	Snakeskin Slippers	1	Leather	10	Snakeskin	16	\N	\N	\N	\N
1434	Snakeskin Suit	1	Snake Eye	1	Leather	50	Snakeskin	30	\N	\N
1435	Tainted Shuriken	1	Shuriken	1	Tainted Chakra	8	\N	\N	\N	\N
1436	Thunder Rod	1	Thunder Stone	1	Wood	20	\N	\N	\N	\N
1437	Tortoise Mask	1	Antique Mask	1	Lava	4	Turtle Shell	18	\N	\N
1438	Turtle Shield	1	Turtle Shell	15	\N	\N	\N	\N	\N	\N
1439	Twisted Mask	1	Antique Mask	7	Tainted Chakra	3	\N	\N	\N	\N
1440	Undying Mask	1	Cracked ANBU Mask	1	Ghost Orb	1	Ice Shard	1	\N	\N
1441	Vajra	1	Lightning Shard	1	White Diamond	5	\N	\N	\N	\N
1442	Wolf Mantle	1	Wolf Pelt	50	\N	\N	\N	\N	\N	\N
1058	Ice Shard	1	Ice Horn	15	Alkahest	1	\N	\N	\N	\N
1059	Ironbark	1	Alkahest	1	Iron	2	Wood	8	\N	\N
1060	Refined Chakra Plate	1	Alkahest	1	Chakra Plate	10	\N	\N	\N	\N
1061	Runic Talisman	1	Alkahest	1	Rune Ward	5	Wooden Talisman	1	\N	\N
1062	Spider Gland	1	Level 8 Giant Spider	1	Panacea	1	\N	\N	\N	\N
1063	Talatat Stone	1	Alkahest	1	Quicklime	3	\N	\N	\N	\N
1064	Wolf Eye	1	Level 8 Wolf	1	Panacea	1	\N	\N	\N	\N
1065	Imperial Jade	1	Alkahest	1	Jade	10	\N	\N	\N	\N
1066	Byakugan Eye	1	Panacea	1	Level 20 Hanabi	1	\N	\N	\N	\N
1067	Byakugan Eye	1	Panacea	1	Level 20 Hiashi	1	\N	\N	\N	\N
1068	Byakugan Eye	2	Panacea	1	Level 28 Neji	1	\N	\N	\N	\N
1069	Byakugan Eye	3	Panacea	1	Level 33 Hinata	1	\N	\N	\N	\N
1070	Mangekyo Eye	10	Panacea	1	Level 10 Mangekyou Sharingan	1	\N	\N	\N	\N
1071	Hashiramas DNA	1	Panacea	1	Level 25 Hashirama	1	\N	\N	\N	\N
1072	Sharingan Eye	1	Panacea	1	Level 10 Crow	1	\N	\N	\N	\N
1073	Sharingan Eye	1	Panacea	1	Level 10 Genjutsu Crow	1	\N	\N	\N	\N
1074	Sharingan Eye	1	Panacea	1	Level 20 Kakashi	1	\N	\N	\N	\N
1075	Sharingan Eye	2	Panacea	1	Level 27 Shisui	1	\N	\N	\N	\N
1076	Sharingan Eye	2	Panacea	1	Level 28 Sasuke	1	\N	\N	\N	\N
1077	Sharingan Eye	3	Panacea	1	Level 33 Itachi	1	\N	\N	\N	\N
1078	Sharingan Eye	4	Panacea	1	Level 37 Obito	1	\N	\N	\N	\N
1079	Sharingan Eye	5	Panacea	1	Level 43 Tobi	1	\N	\N	\N	\N
1080	Iron Sand	1	Alkahest	1	Iron	2	Red Sand	2	\N	\N
1081	Wooden Talisman	1	Alkahest	1	Ink Bottle	1	Wood	4	\N	\N
1082	Angel Feather	1	Alkahest	1	Cursed Feather	10	Holy Water	3	\N	\N
1083	Beast Soul	1	Azoth	1	Chimera DNA	1	Dragon Soul	1	Serpent Soul	1
1084	Celestial Silk	1	Alkahest	1	Celestial Dye	4	Muga Silk	6	\N	\N
1085	Mimirwater	1	Alkahest	1	Holy Water	2	Yggdrasil Sap	2	\N	\N
1086	Stone Tablet	1	Curse Mark Pattern	4	Otsutsuki Cells	3	Soil Stone	1	Uchiha Crest	5
1087	Curse Mark DNA	10	Level 9 Lesser Cursed Seal BL	1	Panacea	1	\N	\N	\N	\N
1088	Reincarnation Ether	1	Ascension Ore	10	Azoth	1	Beast Soul	1	Mimirwater	1
1089	Altuna Rune	1	Azoth	1	Runestone	6	\N	\N	\N	\N
1090	Atlantic Artifact	1	Aqua Stone	4	Azoth	1	Babylonian Stone	1	\N	\N
1091	Bolt Artifact	1	Azoth	1	Babylonian Stone	1	Thunder Stone	4	\N	\N
1092	Czarwood	1	Ancient Coin	1	Darkwood	1	Yliaster	1	\N	\N
1093	Divine Iron	1	Angel Feather	1	Damascus Steel	1	Yliaster	1	\N	\N
1094	Tundra Artifact	1	Azoth	1	Babylonian Stone	1	Ice Shard	10	\N	\N
1095	Gaia Artifact	1	Azoth	1	Babylonian Stone	1	Soil Stone	4	\N	\N
1096	Heart Container	1	Alkahest	1	Black Threads	30	\N	\N	\N	\N
1097	Inferno Artifact	1	Azoth	1	Molten Stone	4	Babylonian Stone	1	\N	\N
1098	Osteoblast	1	Alkahest	1	Stem Cell	10	Bone	50	\N	\N
1099	Osteoclast	1	Alkahest	1	Stem Cell	10	Blood Vessel	10	\N	\N
1100	Chimera Heart	2	Level 7 Chimera	1	\N	\N	\N	\N	\N	\N
1101	Snakeskin	25	Level 5 Giant Snake	1	\N	\N	\N	\N	\N	\N
1102	Wolf Pelt	50	Level 5 Wolf	1	\N	\N	\N	\N	\N	\N
1103	Holy Water	1	Alkahest	1	Clear Water	35	\N	\N	\N	\N
1104	Ouroboros Stone	1	Azoth	1	Dragon Blood	4	Mercury	18	Snake Eye	8
1105	Spirit Container	1	Heart Container	2	Yliaster	1	\N	\N	\N	\N
1106	Babylonian Stone	1	Alkahest	1	Iron Sand	5	Roran Coin	5	\N	\N
1107	Dragon Bone	1	Alkahest	1	Adamantine Bone	3	Bone	30	Dragon Blood	2
1108	Monkey King Fur	1	Alkahest	1	Bezoar	2	Lion Fur	15	\N	\N
1109	Robot Processor	1	Alkahest	1	Robot Parts	15	\N	\N	\N	\N
1110	Spider Silk	1	Alkahest	1	Muga Silk	2	Spider Web	30	\N	\N
1111	Venoxin Mutagen	1	Alkahest	1	Berserker Enzyme	4	Poison Vial	4	Salamander Venom	1
1112	Lifestream	1	Panacea	1	Level 8 Life Dragon	1	\N	\N	\N	\N
1113	Dharma Spirit	1	Alkahest	1	Dharma Wheel	5	Ghost Spirit	2	\N	\N
1114	Python Longevity Serum	1	Yliaster	1	Snake Serum	1	Snakeskin	30	Snake Eye	6
1115	Yggdrasil Sap	1	Alkahest	1	Yggdrasil Bark	1	Zephyr Leaf	1	\N	\N
1116	Aero Stone	1	Alkahest	1	Granite	2	Zephyr Leaf	2	\N	\N
1117	Aqua Stone	1	Alkahest	1	Granite	2	Clear Water	25	\N	\N
1118	Arachne Web	1	Alkahest	1	Spider Web	25	\N	\N	\N	\N
1119	Beetle Tail	1	Panacea	1	Level 10 Chomei	1	\N	\N	\N	\N
1120	Damascus Steel	1	Azoth	1	Steel	10	\N	\N	\N	\N
1121	Dao Dragon Pill	1	Alkahest	1	Dragon Blood	4	Mind Awakening Pill	10	\N	\N
1122	Elvenskin	1	Alkahest	1	Leather	12	Zephyr Leaf	1	\N	\N
1123	Fiver Tail	1	Level 10 Kokuo	1	Panacea	1	\N	\N	\N	\N
1124	Ghost Orb	1	Alkahest	1	Ghost Spirit	10	\N	\N	\N	\N
1125	Iron Tail	1	Level 10 Shukaku	1	Panacea	1	\N	\N	\N	\N
1126	Molten Stone	1	Alkahest	1	Granite	2	Magma	1	\N	\N
1127	Monkey Tail	1	Panacea	1	Level 10 Son Goku	1	\N	\N	\N	\N
1128	Runestone	1	Alkahest	1	Rune Ward	5	Talatat Stone	1	\N	\N
1129	Ryumyaku Source	1	Level 10 Mecha Mukade	1	Panacea	1	\N	\N	\N	\N
1130	Sea Gods Tear	1	Level 10 Umibozu	1	Panacea	1	\N	\N	\N	\N
1131	Slime Tail	1	Level 10 Saiken	1	Panacea	1	\N	\N	\N	\N
1132	Soil Stone	1	Alkahest	1	Clay	30	Granite	2	\N	\N
1133	Steel	1	Alkahest	1	Iron	10	\N	\N	\N	\N
1134	Ceremonial Mask	1	Alkahest	1	Antique Mask	4	Ironbark	1	\N	\N
1135	Darkwood	1	Azoth	1	Ironbark	7	\N	\N	\N	\N
1136	Magma	1	Alkahest	1	Lava	4	Granite	3	\N	\N
1137	Opal	1	Alkahest	1	Quartz Crystal	1	White Diamond	1	\N	\N
1138	Yggdrasil Bark	1	Alkahest	1	Ironbark	1	Rune Ward	4	\N	\N
1509	Abaddons Armory	1	Ancient Blood	1	Darkwood	2	Demon Wing	15	Tainted Shuriken	1
1510	Arachnes Arbalest	1	Arachne Web	10	Darkwood	4	Salamander Venom	10	Spider Gland	1
1511	Arctic Battlearmor	1	Refined Chakra Plate	20	Tundra Artifact	1	\N	\N	\N	\N
1512	Argent White Sword	1	Ascension Ore	15	Ghost Orb	5	Thunder Stone	14	\N	\N
1513	Artificial Bloodline Core	1	Hyuuga Blood	12	Artificial Chakra Core	6	Uchiha Blood	12	\N	\N
1514	Bahamuts Mane	1	Chimera DNA	3	Chimera Heart	4	Dragon Soul	4	Lion Fur	100
1515	Blades of Kali	1	Ancient Blood	15	Damascus Steel	2	Dharma Spirit	15	Essence of Darkness	20
1516	Bloodsealed Talisman	1	Blood Vessel	30	Chimera Heart	3	Wooden Talisman	5	\N	\N
1517	Bone Armor	1	Bone	150	Adamantine Bone	20	Ghost Spirit	40	\N	\N
1518	Bone Pendant	1	Osteoclast	1	Bone	20	Runic Talisman	1	\N	\N
1519	Chakra Exosuit	1	Artificial Chakra Core	1	Black Receiver	12	Chakra Shield	1	Robot Processor	2
1520	Chakra Shield	1	Steel	5	Refined Chakra Plate	7	\N	\N	\N	\N
1521	Cursed Ring	1	Ascension Ore	1	Curse Mark DNA	3	\N	\N	\N	\N
1522	Daedalus Cuirass	1	Curse Mark Pattern	5	Damascus Steel	2	Molten Stone	5	Elvenskin	8
1523	Divine Khakkara	1	Dharma Spirit	10	Gaia Artifact	2	Holy Water	10	Lotus Amulet	6
1524	Dracolichs Phylactery	1	Ancient Blood	25	Deathly Dust	10	Dragon Bone	8	Ghost Orb	10
1525	Electrum Coin	1	Ancient Coin	1	Bronze Coin	4	Roran Coin	4	\N	\N
1526	Fafnirs Scales	1	Dragon Soul	2	Ruby	20	Runestone	6	Serpent Scales	50
1527	Fenrirs Fang	1	Damascus Steel	1	Warg Talisman	1	Wolf Eye	1	\N	\N
1528	Ghost Cape	1	Wolf Mantle	1	Muga Silk	8	\N	\N	\N	\N
1529	Gungnir	1	Runestone	10	Runic Talisman	10	Yggdrasil Bark	10	Ouroboros Stone	3
1530	Heart of Stone	1	Heart Container	1	Level 20 Onoki	1	Black Threads	12	Talatat Stone	3
1531	Heart of Sound	1	Heart Container	1	Level 20 Orochimaru	1	Black Threads	12	Curse Mark Pattern	10
1532	Heart of Sand	1	Heart Container	1	Level 20 Chiyo	1	Black Threads	12	Red Sand	50
1533	Heart of Root	1	Heart Container	1	Level 20 Danzo	1	Black Threads	12	Uchiha Blood	5
1534	Heart of Rain	1	Heart Container	1	Level 20 Hanzo	1	Black Threads	12	Aqua Stone	2
1535	Heart of Neko	1	Heart Container	1	Level 20 Nekomata	1	Black Threads	12	Lion Fur	35
1536	Heart of Mist	1	Heart Container	1	Level 20 Mei	1	Black Threads	12	Steam Stone	2
1537	Heart of Leaf	1	Heart Container	1	Level 20 Hiruzen	1	Black Threads	12	Zephyr Leaf	5
1538	Heart of Cloud	1	Heart Container	1	Level 20 A	1	Black Threads	12	Thunder Stone	2
1539	Heart of ANBU	1	Heart Container	1	Level 20 Sakumo	1	Black Threads	12	Anbu Mask	1
1541	Hugins Talon	1	Rune Word	10	Ruby	4	Crow Feather	25	\N	\N
1542	Icarus Wings	1	Elvenskin	10	Demon Wing	20	Arachne Web	4	Angel Feather	3
1543	Inksealed Talisman	1	Ink Bottle	50	Chimera Heart	3	Wooden Talisman	5	\N	\N
1544	Jade Emeperors Pagoda	1	Celestial Silk	3	Czarwood	2	Imperial Jade	16	Lotus Amulet	3
1545	Jibrils Garb	1	Angel Feather	2	Celestial Silk	3	Charm of Protection	5	Vajra	1
1546	Jingu Bang	1	Monkey Tail	1	Czarwood	4	Monkey King Fur	6	Hashiramas DNA	4
1547	Jörmungandrs Skull	1	Mimirwater	4	Ouroboros Stone	2	Runestone	8	Serpent Soul	3
1548	Krakens Tentacle	1	Atlantic Artifact	1	Ink Bottle	100	Octopus Arm	2	Sea Gods Tear	1
1549	Leviathans Fin	1	Fish Scales	100	Sea Gods Tear	1	Serpent Soul	2	\N	\N
1550	Lichs Vestment	1	Spider Silk	2	Deathly Dust	5	Ghost Spirit	60	\N	\N
1551	Mangekyo Seal	1	Ascension Ore	1	Mangekyo Eye	3	\N	\N	\N	\N
1552	MBF-02	1	Ryumyaku Source	1	Gundam Core	2	Steel	12	Robot Parts	50
1553	Midgard Mail	1	Runestone	4	Elvenskin	3	\N	\N	\N	\N
1554	Minotaurs Horn	1	Sea Gods Tear	1	Gaia Artifact	3	Serpent Horn	15	Osteoblast	2
1555	Mjölnir	1	Yggdrasil Bark	15	Altuna Rune	2	Ascension Ore	40	Bolt Artifact	4
1556	Munins Talon	1	Rune Word	10	Opal	2	Crow Feather	25	\N	\N
1557	Nibelungs Ring	1	Opal	20	Ouroboros Stone	2	Runic Talisman	5	\N	\N
1558	Niddhoggrs Claw	1	Beast Soul	1	Venoxin Mutagen	4	Yggdrasil Bark	5	\N	\N
1559	Origin Bone Scepter	1	Dragon Bone	18	Essence of Origin	15	Dao Dragon Pill	2	Beast Soul	6
1560	Pandemonium Flute	1	Darkwood	1	Deathly Dust	6	Serpent Horn	50	Slime Tail	1
1561	Piezoelectric Clock	1	Ancient Coin	1	Iron Sand	10	Quartz Crystal	14	Rusty Spring	16
1562	Python Robe	1	Serpent Soul	2	Serpent Scales	40	Snakeskin	40	Spider Silk	2
1563	Rhinoceros Shell	1	Beetle Tail	1	Dice	90	Gaia Artifact	1	Turtle Shell	90
1564	Scrolls of Sutra	1	Aero Stone	10	Dharma Spirit	15	Essence of Smoke	20	Jutsu Scroll	400
1565	Skull Helmet	1	Osteoblast	3	Ghost Orb	6	Ancient Coin	6	\N	\N
1566	Snakeskin Seal	1	Snakeskin	120	Snake Serum	5	\N	\N	\N	\N
1567	Snakeskin Shako	1	Snake Serum	2	Snake Eye	5	Snakeskin	50	\N	\N
1568	Sunpierce Sword	1	Ruby	7	Magma	12	\N	\N	\N	\N
1569	Tiamats Tiara	1	Lifestream	1	Serpent Soul	10	Clear Water	300	Babylonian Stone	4
1570	Valhalla Helm	1	Ceremonial Mask	4	Rune Ward	30	\N	\N	\N	\N
1571	Warg Talisman	1	Ancient Blood	4	Bone	30	Wolf Pelt	20	\N	\N
1572	Websealed Talisman	1	Spider Web	100	Wooden Talisman	5	Chimera Heart	3	\N	\N
1573	Winged Mask	1	Twisted Mask	1	Demon Wing	10	Dark Chakra Eel	20	\N	\N
1574	Xiantians Divine Axe	1	Ivory	100	Python Longevity Serum	3	Divine Iron	2	Dao Dragon Pill	5
1540	Heretical God Seed	1	Bolt Artifact	4	Gaia Artifact	4	Inferno Artifact	4	Spirit Container	2
1300	Byakugan Eye	4	Panacea	1	Level 37 Himawari	1	\N	\N	\N	\N
1575	Berserker Release	1	Level 20 Jugo	1	Berserker Enzyme	50	Mind Awakening Pill	10	\N	\N
1624	Lotus Amulet	1	Alkahest	5	Charm of Protection	7	White Diamond	1	Mercury	8
1606	Wood Release	1	Atlantic Artifact	5	Gaia Artifact	5	Hashiramas DNA	5	Nature Transformation Chakra	40
1607	Twin Release	1	Level 15 Sakon	1	Heart Container	2	Mind Awakening Pill	45	Stem Cell	10
1609	Tailed Host: Gyuki	1	Level 40 Killer Bee	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1610	Tailed Host: Chomei	1	Level 40 Fu	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1611	Tailed Host: Saiken	1	Level 40 Utakata	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1612	Tailed Host: Kokuo	1	Level 40 Han	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1613	Tailed Host: Son Goku	1	Level 40 Roshi	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1614	Tailed Host: Isobu	1	Level 40 Yagura	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1615	Tailed Host: Matatabi	1	Level 40 Yugito	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1616	Tailed Host: Shukaku	1	Level 40 Gaara	1	Jinchuriki Chakra	20	Jinchuriki Seal	120	\N	\N
1617	Tayuya's Cursed Seal	1	Level 25 Tayuya	1	Curse Mark Pattern	30	Mind Awakening Pill	10	Tainted Chakra	60
1618	Swift Release	1	Cursed Feather	20	Demon Wing	10	Essence of Swiftness	20	\N	\N
1619	Steel Release	1	Damascus Steel	1	Roran Coin	10	Rusty Spring	100	Steel	10
1620	Smoke Release	1	Mind Awakening Pill	10	Essence of Smoke	25	\N	\N	\N	\N
1621	Shisui's Mangekyo Sharingan	1	Level 40 Shisui	1	Level 10 Sharingan Bloodline	1	Cracked ANBU Mask	120	Susanoo Residue	10
1622	Sharingan	1	Sharingan Eye	5	Dojutsu Catalyst	20	Uchiha Blood	100	\N	\N
1608	Tenseigan	1	Level 65 Toneri	1	Level 10 Byakugan	1	Otsutsuki Cells	100	Reincarnation Ether	2
1625	Divine Magic Cube	1	Spirit Container	10	Reincarnation Ether	5	\N	\N	\N	\N
1626	Anubi's Flail	1	Wolf Eye	1	Talatat Stone	15	\N	\N	\N	\N
1627	Anubi's Scale	1	Wolf Eye	1	Bone	170	Roran Coin	20	\N	\N
1628	Anubi's Mask	1	Wolf Eye	1	Ceremonial Mask	7	Adamantine Bone	30	Deathly Dust	3
1576	Scorch Release	1	Aero Stone	5	Lava	30	Nature Transformation Chakra	15	\N	\N
1577	Sasuke's Mangekyo Sharingan	1	Level 40 Sasuke	1	Level 10 Sharingan Bloodline	1	Curse Mark Pattern	40	Susanoo Residue	10
1578	Sakon's Cursed Seal	1	Level 25 Sakon	1	Curse Mark Pattern	30	Mind Awakening Pill	10	Stem Cell	30
1579	Rinnegan	1	Level 75 Nagato	1	Dojutsu Catalyst	40	Hashiramas DNA	8	Indras Chakra	8
1580	Perfected Host: Gyuki	1	Level 10 Tailed Host: Gyuki	1	Beast Sealing Chains	15	Biju Meat	55	Ink Tail	1
1581	Perfected Host: Chomei	1	Level 10 Tailed Host: Chomei	1	Beast Sealing Chains	15	Biju Silk	55	Beetle Tail	1
1582	Perfected Host: Saiken	1	Level 10 Tailed Host: Saiken	1	Beast Sealing Chains	15	Biju Acid	55	Slime Tail	1
1583	Perfected Host: Kokuo	1	Level 10 Tailed Host: Kokuo	1	Beast Sealing Chains	15	Biju Horn	55	Fiver Tail	1
1584	Perfected Host: Son Goku	1	Level 10 Tailed Host: Son Goku	1	Beast Sealing Chains	15	Biju Fur	55	Monkey Tail	1
1585	Perfected Host: Isobu	1	Level 10 Tailed Host: Isobu	1	Beast Sealing Chains	15	Biju Coral	55	Coral Tail	1
1586	Perfected Host: Matatabi	1	Level 10 Tailed Host: Matatabi	1	Beast Sealing Chains	15	Biju Flame	55	Blue Flame Tail	1
1587	Perfected Host: Shukaku	1	Level 10 Tailed Host: Shukaku	1	Beast Sealing Chains	15	Biju Sand	55	Iron Tail	1
1588	Obito's Mangekyo Sharingan	1	Level 40 Obito	1	Level 10 Sharingan Bloodline	1	Essence of Darkness	20	Susanoo Residue	10
1589	Magnet Release	1	Ancient Coin	2	Iron Sand	70	Iron Tail	1	Nature Transformation Chakra	5
1590	Madara's Mangekyo Sharingan	1	Level 40 Madara	1	Level 10 Sharingan Bloodline	1	Uchiha Crest	40	Susanoo Residue	10
1591	Lava Release	1	Magma	15	Nature Transformation Chakra	10	Quicklime	60	\N	\N
1592	Kidomaru's Cursed Seal	1	Level 25 Kidomaru	1	Curse Mark Pattern	30	Mind Awakening Pill	10	Spider Web	150
1593	Jirobo's Cursed Seal	1	Level 25 Jirobu	1	Curse Mark Pattern	30	Mind Awakening Pill	10	Soil Stone	6
1594	Itachi's Mangekyo Sharingan	1	Level 40 Itachi	1	Level 10 Sharingan Bloodline	1	Crow Feather	120	Susanoo Residue	10
1595	Ice Release	1	Level 20 Haku	1	Aero Stone	3	Ice Shard	40	Nature Transformation Chakra	5
1596	Ghost Release	1	Ghost Spirit	90	Snake Eye	15	Tainted Chakra	60	\N	\N
1597	Explosion Release	1	Clay	280	Earth Shard	1	Lightning Shard	1	Nature Transformation Chakra	10
1598	Eternal Mangekyo Sharingan	1	Mangekyo Eye	8	Stone Tablet	9	Uchiha Blood	200	\N	\N
1599	Dust Release	1	Aero Stone	14	Inferno Artifact	2	Nature Transformation Chakra	50	Soil Stone	14
1600	Dead Bone Release	1	Level 40 Cursed Kimimaro	1	Adamantine Bone	150	Osteoblast	6	Osteoclast	6
1601	Dark Release	1	Dark Chakra Eel	200	Deathly Dust	3	Essence of Darkness	30	\N	\N
1602	Cursed Seal	1	Level 35 Orochimaru	35	Berserker Enzyme	30	Curse Mark DNA	8	Essence of Darkness	8
1603	Crystal Release	1	Level 10 Crystal Dragon	1	Level 15 Guren	1	Quartz Crystal	15	\N	\N
1604	Byakugan	1	Byakugan Eye	5	Dojutsu Catalyst	20	Hyuuga Blood	100	\N	\N
1605	Boil Release	1	Clear Water	60	Fiver Tail	1	Nature Transformation Chakra	10	Steam Stone	16
1623	Susanoo Residue	3	Level 10 Susanoo Spirit	1	Panacea	1	\N	\N	\N	\N
1629	Black Dragon Spear	1	Dragon Blood	100	Black Threads	300	Black Receiver	150	\N	\N
1630	Houyi's Bow	1	Crow Feather	230	Ice Shards	80	\N	\N	\N	\N
\.


--
-- Data for Name: warzone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.warzone (id, grid, zone, name, defeated, health_percentage, image, reported, rewards, condition, duration, veins, valuation) FROM stdin;
2	1	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
308	307	0	Orochimaru Chunin Genin	f	9	https://www.ninjamanager.com/img/cards/upload/20/small/orochimaru-chunin-genin.png	t	Deathly Dust$167, Quartz Crystal$50, Ice Shards$50, Berserker Enzyme$21, Anbu Paint$10, Bronze Coin$7, Fish Scales$3	170,000 of 1,925,000 health.	Battle ends in 2 days 21h 38m.		1.8
369	368	0	Experimental Snake Summon	t	41	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Serpent Soul$167, Muga Silk$50, Bezoar$50, Uchiha Blood$25, Mercury$13, Wood$4, Fish Scales$3	1,010,000 of 2,475,000 health.	Battle ends in 5 days 17h 38m.		10.37
604	603	1	Satori Summon	f	23	https://www.ninjamanager.com/img/cards/upload/20/small/satori-summon.png	t	Frozen Havoc, Aero Stone, Lucky Talisman, Salamander Venom, Ruby	180,000 of 800,000 health.	Battle ends in 0 days 21h 59m.		0.0
2608	2607	5	Guy 8Gates Kage	f	64	https://www.ninjamanager.com/img/cards/upload/20/small/guy-8gates-kage.png	t	Snake Serum, Elvenskin, Zephyr Leaf, Black Receiver, Dice, Rusty Spring	3,760,000 of 5,950,000 health.	Battle ends in 0 days 00h 02m.		0.0
120	119	0	Kabuto Kage	f	90	https://www.ninjamanager.com/img/cards/upload/20/medium/kabuto-kage.png	t	Serpent Pike, Chimera DNA, Elvenskin, Ironbark, Uchiha Crest, Granite, Bronze Coin, Clay	5,650,000 of 6,300,000 health.	Battle ends in 3 days 04h 00m.		0.0
381	380	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
383	382	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
384	383	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
394	393	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
494	493	1	Deidara Fn Jounin	f	30	https://www.ninjamanager.com/img/cards/upload/20/medium/deidara-fn-jounin.png	t	Robot Processor, Elvenskin, Ice Shards, Berserker Enzyme, Iron, Clear Water	4,520,000 of 15,172,500 health.	Battle ends in 0 days 00h 02m.		0.0
819	818	1	Mother Puppet Summon	f	37	https://www.ninjamanager.com/img/cards/upload/20/small/mother-puppet-summon.png	t	Ascension Ore, Essence of Smoke, Ruby	120,000 of 320,000 health.	Battle ends in 0 days 00h 03m.		0.0
628	627	1	Mother Puppet Summon	f	100	https://www.ninjamanager.com/img/cards/upload/20/small/mother-puppet-summon.png	t	Green Jumpsuit, Tainted Shuriken, Zephyr Leaf, Azoth, Ice Shards, Curse Mark Pattern, Stem Cell, Poison Vial, Mercury	1,440,000 of 1,440,000 health.	Battle ends in 8 days 19h 59m.		0.0
748	747	1	Choji Evolved Genin	f	76	https://www.ninjamanager.com/img/cards/upload/20/small/choji-evolved-genin.png	t	Blight Rod, Bezoar, Ruby, Essence of Darkness, Iron Sand	610,000 of 800,000 health.	Battle ends in 0 days 00h 05m.		0.0
2238	2237	4	Neji Evolved Genin	f	89	https://www.ninjamanager.com/img/cards/upload/20/small/neji-evolved-genin.png	t	Adamantine Erosion, Babylonian Stone, Chimera Heart, Jinchuriki Chakra, Fuma Shuriken, Artificial Chakra Core, Tainted Chakra, Crow Feather, Jutsu Scroll	7,770,000 of 8,820,000 health.	Battle ends in 16 days 08h 56m.		0.0
451	450	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
452	451	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
454	453	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
455	454	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
456	455	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
457	456	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
458	457	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
459	458	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
460	459	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
461	460	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
462	461	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
463	462	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
464	463	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
465	464	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
466	465	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
467	466	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
468	467	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
469	468	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
470	469	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
471	470	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
472	471	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
473	472	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
476	475	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
477	476	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
382	381	0	Orochimaru Chunin Genin	f	40	https://www.ninjamanager.com/img/cards/upload/20/small/orochimaru-chunin-genin.png	t	Rune Word$13, Ink Bottle$10, Antique Mask$10	20,000 of 40,000 health.	Battle ends in 1 days 09h 28m.		3.09
521	520	1	Ebizo Kage	f	6	https://www.ninjamanager.com/img/cards/upload/20/small/ebizo-kage.png	t	Reroll Counter$400, Otsutsuki Cells$100, Earth Shard$75, Azoth$50, Panacea$25	70,000 of 1,375,000 health.	Battle ends in 1 days 21h 28m.		2.23
588	587	1	Tenten Evolved Genin	f	3	https://www.ninjamanager.com/img/cards/upload/20/small/tenten-evolved-genin.png	t	Infectious Strike$200, Salamander Venom$63, Ice Shards$50, Demon Wing$50, Soldier Pill$34, Jade$25, Panacea$25, Serpent Horn$17, Poison Vial$17	90,000 of 3,200,000 health.	Battle ends in 14 days 06h 28m.	Mercury	1.05
530	529	1	Chiyo Kage	f	66	https://www.ninjamanager.com/img/cards/upload/20/small/chiyo-kage.png	t	Thunder Stone$125, Earth Shard$75, Talatat Stone$75, Curse Mark Pattern$25, Uchiha Blood$25, Ink Bottle$10, Bronze Coin$7, Bone$3	2,720,000 of 4,125,000 health.	Battle ends in 10 days 10h 28m.		1.28
1050	1049	2	A Kage	f	27	https://www.ninjamanager.com/img/cards/upload/20/small/a-kage.png	t	Serpent Pike$269, Yggdrasil Bark$100, Chimera Gloves$75, Biju Sand$75, Ruby$50, Quicklime$25	250,000 of 960,000 health.	Battle ends in 4 days 13h 17m.		4.13
686	685	1	Gaara Jinchuriki Genin	f	6	https://www.ninjamanager.com/img/cards/upload/20/small/gaara-jinchuriki-genin.png	t	Runic Talisman$75, Charm of Protection$50, Hyuga Blood$25, Poison Vial$17, Granite$17, Cursed Feather$13, Dice$9	40,000 of 675,000 health.	Battle ends in 4 days 07h 28m.		0.0
52	51	0	Kidomaru Jounin	t	42	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Dharma Spirit$150, Detonating Clay$125, Runic Talisman$75, Bezoar$50, Black Receiver$17	570,000 of 1,375,000 health.	Battle ends in 4 days 13h 11m.		5.45
478	477	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
479	478	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
480	479	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
481	480	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
482	481	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
483	482	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
484	483	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
485	484	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
486	485	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
487	486	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
488	487	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
489	488	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
490	489	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
491	490	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
492	491	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
493	492	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
495	494	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
496	495	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
497	496	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
498	497	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
500	499	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
501	500	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
503	502	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
504	503	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
505	504	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
506	505	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
507	506	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
508	507	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2645	2644	5	Top Transformed Buddha Summon	f	72	https://www.ninjamanager.com/img/cards/upload/20/small/top-transformed-buddha-summon.png	t	Heart of Root, Dao Dragon Pill, Dragon Soul, Talatat Stone, Serpent Scales, Jutsu Scroll	8,060,000 of 11,200,000 health.	Battle ends in 0 days 00h 00m.		0.0
251	250	0	Choji Chunin Genin	f	45	https://www.ninjamanager.com/img/cards/upload/20/small/choji-chunin-genin.png	t	Nature Transformation Chakra$50, Jade$25, Hyuga Blood$25, Antique Mask$10, Crow Feather$9, Bronze Coin$7, Red Sand$5	180,000 of 400,000 health.	Battle ends in 19 days 10h 32m.		0.69
1028	1027	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
889	888	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
890	889	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
891	890	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
892	891	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
893	892	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
894	893	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
895	894	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
896	895	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
897	896	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
898	897	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
899	898	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
900	899	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
901	900	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
902	901	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
904	903	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
905	904	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
906	905	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
907	906	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
908	907	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
909	908	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
910	909	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
911	910	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
912	911	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
913	912	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
914	913	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
915	914	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
916	915	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
917	916	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
918	917	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
919	918	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
920	919	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
921	920	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
922	921	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
923	922	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
926	925	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
927	926	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
928	927	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
929	928	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
930	929	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
931	930	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
932	931	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
933	932	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
934	933	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
935	934	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
936	935	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
937	936	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
938	937	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
939	938	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
940	939	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
941	940	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
942	941	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
943	942	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
945	944	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
946	945	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
947	946	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
948	947	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
950	949	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
951	950	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
953	952	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
954	953	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
955	954	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
956	955	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
957	956	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
958	957	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
959	958	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
960	959	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
961	960	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
962	961	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
963	962	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
964	963	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
965	964	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
966	965	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
967	966	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
968	967	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
969	968	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
970	969	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
973	972	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
974	973	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
975	974	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
976	975	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
977	976	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
978	977	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
979	978	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
981	980	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
982	981	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
983	982	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
985	984	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
986	985	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
987	986	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
988	987	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
989	988	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
990	989	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
991	990	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
992	991	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
993	992	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
994	993	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
996	995	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
997	996	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
998	997	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1001	1000	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1002	1001	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1004	1003	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1005	1004	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1006	1005	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1007	1006	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1008	1007	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1009	1008	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1011	1010	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1012	1011	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1013	1012	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1014	1013	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1015	1014	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1016	1015	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1017	1016	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1018	1017	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1019	1018	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1021	1020	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1022	1021	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1023	1022	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1024	1023	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1025	1024	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1026	1025	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1027	1026	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1029	1028	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1030	1029	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1031	1030	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1032	1031	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1033	1032	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1034	1033	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1035	1034	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1036	1035	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1037	1036	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1039	1038	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1040	1039	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1041	1040	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1042	1041	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1043	1042	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1044	1043	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1045	1044	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1046	1045	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1047	1046	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1048	1047	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1049	1048	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1055	1054	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1056	1055	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1057	1056	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1058	1057	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1059	1058	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1060	1059	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1061	1060	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1062	1061	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1063	1062	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1064	1063	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1065	1064	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1066	1065	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1067	1066	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1068	1067	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1069	1068	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1070	1069	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1071	1070	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1072	1071	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1073	1072	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1074	1073	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1075	1074	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1076	1075	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1077	1076	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1079	1078	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1080	1079	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1081	1080	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1082	1081	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1083	1082	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1084	1083	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1085	1084	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1086	1085	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1087	1086	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1088	1087	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1089	1088	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1090	1089	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1091	1090	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1092	1091	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1094	1093	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1095	1094	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1096	1095	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1097	1096	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1098	1097	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1051	1050	2	Onoki Kage	t	4	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Lucky Talisman, Charm of Protection, Essence of Swiftness, Ascension Ore, Mind Awakening Pill	30,000 of 800,000 health.	Battle ends in 3 days 19h 02m.		\N
1101	1100	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1102	1101	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1103	1102	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1104	1103	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1105	1104	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1106	1105	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1107	1106	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1108	1107	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1109	1108	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1110	1109	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1111	1110	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1112	1111	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1113	1112	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1114	1113	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1115	1114	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1116	1115	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1118	1117	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1119	1118	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1120	1119	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1121	1120	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1122	1121	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1123	1122	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1124	1123	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1125	1124	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1126	1125	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1127	1126	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1128	1127	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1129	1128	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1130	1129	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1131	1130	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1132	1131	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1133	1132	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1134	1133	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1135	1134	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1137	1136	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1138	1137	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1139	1138	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1140	1139	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1141	1140	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1142	1141	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1143	1142	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1144	1143	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1145	1144	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1146	1145	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1147	1146	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1148	1147	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1149	1148	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1150	1149	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1153	1152	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1154	1153	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1155	1154	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1156	1155	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1157	1156	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1158	1157	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1159	1158	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1160	1159	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1161	1160	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1162	1161	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1163	1162	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1164	1163	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
90	89	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
91	90	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
92	91	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
93	92	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
135	134	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
136	135	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1165	1164	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1166	1165	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1167	1166	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1168	1167	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1169	1168	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1170	1169	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1171	1170	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1172	1171	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1173	1172	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1174	1173	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1175	1174	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1176	1175	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1384	1383	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1385	1384	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1386	1385	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1387	1386	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1388	1387	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1549	1548	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1551	1550	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1552	1551	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1553	1552	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1554	1553	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1555	1554	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1556	1555	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1557	1556	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1558	1557	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1559	1558	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1560	1559	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1561	1560	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1562	1561	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1563	1562	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1564	1563	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1565	1564	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1566	1565	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1568	1567	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1569	1568	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1570	1569	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
84	83	0	Neji Chunin Genin	f	18	https://www.ninjamanager.com/img/cards/upload/20/small/neji-chunin-genin.png	t	Dire Howl$200, Ruby$50, Charm of Protection$50, Azoth$50, Uchiha Blood$25, Jade$25, Dirty Dice$20, Robot Parts$17, Serpent Scales$13	300,000 of 1,760,000 health.	Battle ends in 7 days 22h 04m.		3.01
164	163	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
165	164	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
166	165	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1693	1692	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1694	1693	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1695	1694	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1696	1695	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1697	1696	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1698	1697	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1699	1698	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1700	1699	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1702	1701	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1703	1702	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1704	1703	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1705	1704	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1706	1705	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1707	1706	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
167	166	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
168	167	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
169	168	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
9	8	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
10	9	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
30	29	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
31	30	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1875	1874	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1876	1875	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1877	1876	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1878	1877	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1879	1878	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1881	1880	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1882	1881	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1883	1882	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2573	2572	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2574	2573	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2575	2574	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1825	1824	4	Utakata Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Ancient Coin, Molten Stone, White Diamond, Dharma Wheel, Adamantine Bone	60,000 of 2,100,000 health.	Battle ends in 0 days 09h 53m.		\N
1885	1884	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1886	1885	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1887	1886	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1888	1887	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1889	1888	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1890	1889	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1891	1890	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1892	1891	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1893	1892	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1894	1893	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1896	1895	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1897	1896	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1898	1897	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1901	1900	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1902	1901	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1904	1903	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1905	1904	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1906	1905	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1907	1906	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1908	1907	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1909	1908	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1911	1910	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1912	1911	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1913	1912	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1914	1913	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2247	2246	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2248	2247	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2249	2248	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2250	2249	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2251	2250	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2252	2251	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2254	2253	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2255	2254	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2256	2255	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2257	2256	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2413	2412	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2414	2413	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2415	2414	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2416	2415	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2417	2416	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2418	2417	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
594	593	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
595	594	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
596	595	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
513	512	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
514	513	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
515	514	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
516	515	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
517	516	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
518	517	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
519	518	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
520	519	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
523	522	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
524	523	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
525	524	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
526	525	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
527	526	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
528	527	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
529	528	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
531	530	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
532	531	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
533	532	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
535	534	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
536	535	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
537	536	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
538	537	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
539	538	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
540	539	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
541	540	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
542	541	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
543	542	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
544	543	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
546	545	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
547	546	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
548	547	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
551	550	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
552	551	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
554	553	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
555	554	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
556	555	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
557	556	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
558	557	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
559	558	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
561	560	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
562	561	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
563	562	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
564	563	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
565	564	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
566	565	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
567	566	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
568	567	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
569	568	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
571	570	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
572	571	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
573	572	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
574	573	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
575	574	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
576	575	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
577	576	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
578	577	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
579	578	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
580	579	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
581	580	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
582	581	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
583	582	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
584	583	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
585	584	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
586	585	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
587	586	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
589	588	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
590	589	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
591	590	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
592	591	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
593	592	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
597	596	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
598	597	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
599	598	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
605	604	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
606	605	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
607	606	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
608	607	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
609	608	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
610	609	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
611	610	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
612	611	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
613	612	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
614	613	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
615	614	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
616	615	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
617	616	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
618	617	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
619	618	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
620	619	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
621	620	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
622	621	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
623	622	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
624	623	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
625	624	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
626	625	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
627	626	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
629	628	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
630	629	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
631	630	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
632	631	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
633	632	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
634	633	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
635	634	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
636	635	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
637	636	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
638	637	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
639	638	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
640	639	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
641	640	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
642	641	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
644	643	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
645	644	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
646	645	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
647	646	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
648	647	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
649	648	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
651	650	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
652	651	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
653	652	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
654	653	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
655	654	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
656	655	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
657	656	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
658	657	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
659	658	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
660	659	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
661	660	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
662	661	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
663	662	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
664	663	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
665	664	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
666	665	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
668	667	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
669	668	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
670	669	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
671	670	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
672	671	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
673	672	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
674	673	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
675	674	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
676	675	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
677	676	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
678	677	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
679	678	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
680	679	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
681	680	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
682	681	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
683	682	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
684	683	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
685	684	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
687	686	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
688	687	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
689	688	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
690	689	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
691	690	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
692	691	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
693	692	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
694	693	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
695	694	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
696	695	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
697	696	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
698	697	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
699	698	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
700	699	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
703	702	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
704	703	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
705	704	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
706	705	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
707	706	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
708	707	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
709	708	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
710	709	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
711	710	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
712	711	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
713	712	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
714	713	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
715	714	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
716	715	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
717	716	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
718	717	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
719	718	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
720	719	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
721	720	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
722	721	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
723	722	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
724	723	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
725	724	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
726	725	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
727	726	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
729	728	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
730	729	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
731	730	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
732	731	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
733	732	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
734	733	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
735	734	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
736	735	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
737	736	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
738	737	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
739	738	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
740	739	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
741	740	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
742	741	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
744	743	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
745	744	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
746	745	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
747	746	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
752	751	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
753	752	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
754	753	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
755	754	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
756	755	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
757	756	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
759	758	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
760	759	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
761	760	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
762	761	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
763	762	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
764	763	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
765	764	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
766	765	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
767	766	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
768	767	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
770	769	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
771	770	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
772	771	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
773	772	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
774	773	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
775	774	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
776	775	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
777	776	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
778	777	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
779	778	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
780	779	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
781	780	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
782	781	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
783	782	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
784	783	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
785	784	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
667	666	1	Rock Lee Fn Jounin	t	2	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Bone Pendant, Spider Silk, Steam Stone, White Diamond, Roran Coin, Lion Fur, Rusty Spring	100,000 of 9,520,000 health.	Battle ends in 1 days 08h 31m.	Wooden Talisman, Hyuga Blood	\N
786	785	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
787	786	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
788	787	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
789	788	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
790	789	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
791	790	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
792	791	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
793	792	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
794	793	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
795	794	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
796	795	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
797	796	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
798	797	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
799	798	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
800	799	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
802	801	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
803	802	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
804	803	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
805	804	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
806	805	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
807	806	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
810	809	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
811	810	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
813	812	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
814	813	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
815	814	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
816	815	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
817	816	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
818	817	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
820	819	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
821	820	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
822	821	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
824	823	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
825	824	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
826	825	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
827	826	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
828	827	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
829	828	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
830	829	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
831	830	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
833	832	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
834	833	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
835	834	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
836	835	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
837	836	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
838	837	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
839	838	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
840	839	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
841	840	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
842	841	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
843	842	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
844	843	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
846	845	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
847	846	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
848	847	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
849	848	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
850	849	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
851	850	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
509	508	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
510	509	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
511	510	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
512	511	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
852	851	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
853	852	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
854	853	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
855	854	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
856	855	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
857	856	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
858	857	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
859	858	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
860	859	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
861	860	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
862	861	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
863	862	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
864	863	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
865	864	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
866	865	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
867	866	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
868	867	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
869	868	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
870	869	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
871	870	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
872	871	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
873	872	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
874	873	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
875	874	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
876	875	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
877	876	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
878	877	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
879	878	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
880	879	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
881	880	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
882	881	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
883	882	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
884	883	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
885	884	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
886	885	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
887	886	1	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1177	1176	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1179	1178	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1180	1179	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1181	1180	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1182	1181	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1183	1182	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1184	1183	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1185	1184	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1186	1185	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1187	1186	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1188	1187	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1189	1188	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1190	1189	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1191	1190	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1192	1191	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1194	1193	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1195	1194	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1196	1195	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1197	1196	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1202	1201	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1203	1202	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1204	1203	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1205	1204	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1206	1205	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1207	1206	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1209	1208	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1210	1209	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1211	1210	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1212	1211	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
137	136	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
139	138	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
140	139	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
141	140	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
142	141	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
143	142	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
144	143	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
145	144	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
146	145	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
147	146	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
148	147	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
149	148	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
155	154	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
156	155	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
157	156	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
158	157	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
159	158	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
160	159	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
161	160	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1213	1212	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1214	1213	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1215	1214	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1216	1215	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1217	1216	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1218	1217	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1220	1219	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1221	1220	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1222	1221	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1223	1222	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1224	1223	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1225	1224	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1226	1225	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1227	1226	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1228	1227	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1229	1228	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1230	1229	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1231	1230	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1232	1231	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1233	1232	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1234	1233	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1235	1234	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1236	1235	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1237	1236	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1238	1237	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1239	1238	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1240	1239	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1241	1240	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1242	1241	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1243	1242	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1244	1243	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1245	1244	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1246	1245	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1247	1246	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1248	1247	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1249	1248	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1250	1249	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1252	1251	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1253	1252	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1254	1253	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1255	1254	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1256	1255	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1257	1256	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1260	1259	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1261	1260	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1263	1262	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1264	1263	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1265	1264	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1266	1265	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1267	1266	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1268	1267	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1270	1269	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1271	1270	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1272	1271	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1274	1273	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1275	1274	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1276	1275	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1277	1276	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1278	1277	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1279	1278	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1280	1279	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1281	1280	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1283	1282	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1284	1283	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1285	1284	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1286	1285	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1287	1286	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1288	1287	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1289	1288	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1290	1289	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1291	1290	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1292	1291	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1293	1292	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1294	1293	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1296	1295	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1297	1296	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1298	1297	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1299	1298	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1300	1299	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1301	1300	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1302	1301	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1303	1302	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1304	1303	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1305	1304	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1306	1305	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1307	1306	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1308	1307	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1309	1308	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1310	1309	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1311	1310	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1312	1311	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1313	1312	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1314	1313	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1315	1314	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1316	1315	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1317	1316	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1318	1317	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1320	1319	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1321	1320	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1322	1321	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1323	1322	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1324	1323	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1325	1324	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1326	1325	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1327	1326	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1328	1327	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1329	1328	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1330	1329	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1331	1330	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1332	1331	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1333	1332	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1334	1333	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1335	1334	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1336	1335	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1337	1336	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1339	1338	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1340	1339	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1341	1340	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1342	1341	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1343	1342	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1344	1343	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1345	1344	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1346	1345	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1347	1346	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1348	1347	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1349	1348	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1350	1349	2	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1351	1350	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1352	1351	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1354	1353	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1355	1354	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1356	1355	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1357	1356	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1358	1357	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1359	1358	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1360	1359	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1361	1360	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1362	1361	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1363	1362	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1364	1363	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1365	1364	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1366	1365	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1367	1366	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1368	1367	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1369	1368	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1370	1369	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1371	1370	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1372	1371	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1373	1372	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1376	1375	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1377	1376	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1378	1377	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1379	1378	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1380	1379	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1381	1380	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1382	1381	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1383	1382	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1389	1388	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1390	1389	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1391	1390	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1392	1391	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1393	1392	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1395	1394	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1396	1395	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1397	1396	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1398	1397	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1400	1399	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1401	1400	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1403	1402	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1404	1403	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1405	1404	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1406	1405	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1407	1406	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1408	1407	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1409	1408	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1410	1409	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1411	1410	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1412	1411	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1413	1412	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1273	1272	2	Baku Summon	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Snake Serum, Dragon Blood, Azoth, Mind Awakening Pill, Serpent Horn, Cracked Anbu Mask, Clear Water	90,000 of 4,200,000 health.	Battle ends in 4 days 02h 57m.	Bezoar, Serpent Scales, Granite, Panacea, Adamantine Bone	\N
1414	1413	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1415	1414	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1416	1415	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1417	1416	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1418	1417	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1419	1418	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1420	1419	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1423	1422	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1424	1423	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1425	1424	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1426	1425	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1427	1426	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1428	1427	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1429	1428	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1431	1430	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1432	1431	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1433	1432	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1435	1434	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1436	1435	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1437	1436	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1438	1437	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1439	1438	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1440	1439	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1441	1440	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1442	1441	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1443	1442	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1444	1443	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1446	1445	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1447	1446	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1448	1447	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1451	1450	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1452	1451	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1454	1453	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1455	1454	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1456	1455	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1457	1456	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1458	1457	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1459	1458	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1461	1460	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1462	1461	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1463	1462	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1464	1463	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1465	1464	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1466	1465	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1467	1466	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1468	1467	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1469	1468	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1471	1470	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1472	1471	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1473	1472	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1474	1473	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1475	1474	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1476	1475	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1477	1476	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1478	1477	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1479	1478	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1480	1479	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1481	1480	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1482	1481	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1483	1482	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1484	1483	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1485	1484	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1486	1485	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1487	1486	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1489	1488	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1490	1489	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1491	1490	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1492	1491	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1493	1492	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1494	1493	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1495	1494	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1496	1495	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1497	1496	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1498	1497	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1499	1498	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1504	1503	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1505	1504	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1506	1505	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1507	1506	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1508	1507	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1509	1508	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1510	1509	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1511	1510	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1512	1511	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1513	1512	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1514	1513	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1515	1514	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1516	1515	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1517	1516	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1518	1517	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1519	1518	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1520	1519	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1521	1520	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1522	1521	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1523	1522	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1524	1523	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1525	1524	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1526	1525	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1527	1526	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1529	1528	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1530	1529	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1531	1530	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1532	1531	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1533	1532	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1534	1533	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1535	1534	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1536	1535	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1537	1536	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1538	1537	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1539	1538	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1540	1539	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1541	1540	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1542	1541	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1544	1543	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1545	1544	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1546	1545	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1547	1546	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1548	1547	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1502	1501	3	Killer B Evolved Jounin	t	4	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Runestone, Jinchuriki Chakra, Hyuga Blood	20,000 of 550,000 health.	Battle ends in 1 days 05h 19m.		\N
1571	1570	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1572	1571	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1573	1572	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1574	1573	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1575	1574	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1576	1575	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1577	1576	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1578	1577	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1579	1578	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1580	1579	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1581	1580	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1582	1581	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1583	1582	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1584	1583	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1585	1584	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1587	1586	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1588	1587	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1589	1588	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1590	1589	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1591	1590	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1592	1591	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1593	1592	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1594	1593	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1595	1594	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1596	1595	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1597	1596	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1598	1597	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1599	1598	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1600	1599	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1603	1602	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1604	1603	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1605	1604	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1606	1605	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1607	1606	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1608	1607	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1609	1608	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1610	1609	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1611	1610	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1612	1611	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1613	1612	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1614	1613	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1615	1614	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1616	1615	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1617	1616	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1618	1617	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1619	1618	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1620	1619	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1621	1620	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1622	1621	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1623	1622	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1624	1623	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1625	1624	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1626	1625	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1627	1626	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1629	1628	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1630	1629	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1631	1630	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1632	1631	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1633	1632	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1634	1633	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1635	1634	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1636	1635	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1637	1636	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1638	1637	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1639	1638	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1640	1639	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1641	1640	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1642	1641	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1644	1643	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1645	1644	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1646	1645	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1647	1646	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1652	1651	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1653	1652	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1654	1653	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1655	1654	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1656	1655	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1657	1656	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1659	1658	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1660	1659	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1661	1660	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1662	1661	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1663	1662	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1664	1663	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1665	1664	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1666	1665	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1667	1666	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1668	1667	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1670	1669	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1671	1670	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1672	1671	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1673	1672	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1674	1673	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1675	1674	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1676	1675	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1677	1676	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1678	1677	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1679	1678	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1680	1679	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1681	1680	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1682	1681	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1683	1682	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1684	1683	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1685	1684	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1686	1685	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1687	1686	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1688	1687	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1689	1688	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1690	1689	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1691	1690	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1692	1691	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1710	1709	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1711	1710	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1713	1712	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1714	1713	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1715	1714	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1716	1715	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1717	1716	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1718	1717	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1720	1719	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1721	1720	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1722	1721	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1724	1723	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1725	1724	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1726	1725	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1727	1726	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1601	1600	3	Pakura Jounin	t	4	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Elemental Rune, Deathly Dust, Steel, Essence of Smoke, Serpent Scales, Dark Chakra Eel, Red Sand, Clay	70,000 of 2,475,000 health.	Battle ends in 7 days 18h 26m.	Quartz Crystal, Adamantine Bone	\N
1728	1727	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1729	1728	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1730	1729	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1731	1730	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1733	1732	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1734	1733	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1735	1734	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1736	1735	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1737	1736	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1738	1737	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1739	1738	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1740	1739	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1741	1740	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1742	1741	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1743	1742	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1744	1743	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1746	1745	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1747	1746	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1748	1747	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1749	1748	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1750	1749	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1751	1750	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1752	1751	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1753	1752	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1754	1753	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1755	1754	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1756	1755	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1757	1756	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1758	1757	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1759	1758	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1760	1759	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1761	1760	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1762	1761	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1763	1762	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1764	1763	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1765	1764	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1766	1765	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1767	1766	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1768	1767	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1770	1769	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1771	1770	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1772	1771	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1773	1772	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1774	1773	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1775	1774	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1776	1775	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1777	1776	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1778	1777	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1779	1778	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1780	1779	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1781	1780	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1782	1781	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1783	1782	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1784	1783	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1785	1784	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1786	1785	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1787	1786	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1789	1788	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1790	1789	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1791	1790	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1792	1791	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1793	1792	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1794	1793	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1795	1794	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1796	1795	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1797	1796	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1798	1797	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1799	1798	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1800	1799	3	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1801	1800	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1802	1801	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1804	1803	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1805	1804	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1806	1805	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1807	1806	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1808	1807	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1809	1808	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1810	1809	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1811	1810	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1812	1811	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1813	1812	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1814	1813	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1815	1814	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1816	1815	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1817	1816	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1818	1817	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1819	1818	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1820	1819	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1821	1820	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1822	1821	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1823	1822	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1826	1825	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1827	1826	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1828	1827	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1829	1828	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1830	1829	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1831	1830	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1832	1831	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1833	1832	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1834	1833	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1835	1834	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1836	1835	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1837	1836	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1838	1837	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1839	1838	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1840	1839	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1841	1840	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1842	1841	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1843	1842	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1845	1844	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1846	1845	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1847	1846	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1848	1847	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1850	1849	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1851	1850	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1853	1852	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1854	1853	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1855	1854	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1856	1855	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1857	1856	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1858	1857	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1859	1858	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1860	1859	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1861	1860	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1862	1861	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1863	1862	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1864	1863	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1865	1864	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1866	1865	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1867	1866	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1868	1867	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1869	1868	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1870	1869	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1873	1872	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1874	1873	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1915	1914	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1916	1915	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1917	1916	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1918	1917	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1919	1918	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1921	1920	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1922	1921	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1923	1922	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1924	1923	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1925	1924	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1926	1925	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1927	1926	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1928	1927	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1929	1928	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1930	1929	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1931	1930	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1932	1931	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1933	1932	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1934	1933	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1935	1934	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1936	1935	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1937	1936	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1939	1938	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1940	1939	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1941	1940	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1942	1941	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1943	1942	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1944	1943	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1945	1944	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1946	1945	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1947	1946	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1948	1947	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1949	1948	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1955	1954	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1956	1955	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1957	1956	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1958	1957	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1959	1958	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1960	1959	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1961	1960	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1962	1961	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1963	1962	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1964	1963	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1965	1964	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1966	1965	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1967	1966	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1968	1967	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1969	1968	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1970	1969	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1971	1970	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1972	1971	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1973	1972	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1974	1973	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1975	1974	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1976	1975	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1977	1976	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1979	1978	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1980	1979	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1981	1980	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1982	1981	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1983	1982	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1984	1983	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1985	1984	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1986	1985	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1987	1986	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1988	1987	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1989	1988	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1990	1989	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1991	1990	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1992	1991	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1994	1993	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1995	1994	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1996	1995	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1997	1996	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1998	1997	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1999	1998	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2001	2000	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2002	2001	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2003	2002	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2004	2003	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2005	2004	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2006	2005	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2007	2006	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2008	2007	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2009	2008	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2010	2009	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2011	2010	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2012	2011	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2013	2012	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2014	2013	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2015	2014	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2016	2015	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2018	2017	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2019	2018	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2020	2019	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2021	2020	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2022	2021	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2023	2022	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2024	2023	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2025	2024	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2026	2025	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2027	2026	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2028	2027	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2029	2028	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2030	2029	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2031	2030	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2032	2031	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2033	2032	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2034	2033	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2035	2034	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2037	2036	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2038	2037	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2039	2038	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2040	2039	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2041	2040	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2042	2041	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2043	2042	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2044	2043	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2045	2044	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2046	2045	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2047	2046	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2048	2047	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2049	2048	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2050	2049	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2053	2052	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2054	2053	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2055	2054	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2056	2055	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2057	2056	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2058	2057	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2059	2058	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2060	2059	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2061	2060	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2062	2061	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2063	2062	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2064	2063	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2065	2064	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2066	2065	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2067	2066	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2068	2067	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2069	2068	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2070	2069	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2071	2070	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2072	2071	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2073	2072	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2074	2073	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2075	2074	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2076	2075	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2077	2076	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2079	2078	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2080	2079	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2081	2080	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2082	2081	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2083	2082	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2084	2083	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2085	2084	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2086	2085	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2087	2086	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2088	2087	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2089	2088	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2090	2089	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2091	2090	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2092	2091	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2094	2093	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2095	2094	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2096	2095	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2097	2096	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2102	2101	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2103	2102	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2104	2103	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2105	2104	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2106	2105	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2107	2106	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2109	2108	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2110	2109	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2111	2110	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2112	2111	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2113	2112	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2114	2113	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2115	2114	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2116	2115	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2117	2116	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2118	2117	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2120	2119	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2121	2120	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2122	2121	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2123	2122	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2124	2123	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2125	2124	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2126	2125	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2127	2126	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2128	2127	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2129	2128	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2130	2129	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2131	2130	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2132	2131	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2133	2132	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2134	2133	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2135	2134	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2136	2135	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2137	2136	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2138	2137	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2139	2138	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2140	2139	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2141	2140	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2142	2141	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2143	2142	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2144	2143	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2145	2144	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2146	2145	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2147	2146	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2148	2147	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2149	2148	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2150	2149	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2152	2151	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2153	2152	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2154	2153	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2155	2154	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2156	2155	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2157	2156	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2160	2159	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2161	2160	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2163	2162	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2164	2163	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2165	2164	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2166	2165	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2167	2166	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2168	2167	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2170	2169	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2171	2170	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2172	2171	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2174	2173	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2175	2174	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2176	2175	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2177	2176	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2178	2177	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2179	2178	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2180	2179	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2181	2180	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2183	2182	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2184	2183	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2185	2184	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2186	2185	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2187	2186	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2188	2187	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2189	2188	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2190	2189	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2191	2190	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2192	2191	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2193	2192	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2194	2193	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2196	2195	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2197	2196	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2198	2197	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2099	2098	4	Gedo Mazo Dormant Summon	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Mind Recovery, Lotus Amulet, Yggdrasil Bark, Fire Shard, Ice Shards, Roran Coin, Granite, Dice, Chakra Plate	130,000 of 6,720,000 health.	Battle ends in 7 days 07h 24m.		\N
2199	2198	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2200	2199	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2201	2200	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2202	2201	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2203	2202	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2204	2203	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2205	2204	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2206	2205	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2207	2206	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2208	2207	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2209	2208	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2210	2209	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2211	2210	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2212	2211	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2213	2212	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2214	2213	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2215	2214	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2216	2215	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2217	2216	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2218	2217	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2219	2218	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2220	2219	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2221	2220	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2222	2221	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2223	2222	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2224	2223	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2225	2224	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2226	2225	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2227	2226	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2228	2227	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2229	2228	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2230	2229	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2231	2230	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2232	2231	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2233	2232	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2234	2233	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2235	2234	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2236	2235	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2237	2236	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2239	2238	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2240	2239	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2241	2240	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2242	2241	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2243	2242	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2244	2243	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2245	2244	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2246	2245	4	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2258	2257	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2259	2258	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2260	2259	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2261	2260	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2262	2261	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2263	2262	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2264	2263	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2265	2264	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2266	2265	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2267	2266	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2268	2267	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2269	2268	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2270	2269	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2271	2270	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2272	2271	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2273	2272	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2276	2275	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2277	2276	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2278	2277	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2279	2278	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2280	2279	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2281	2280	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2282	2281	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2283	2282	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2284	2283	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2285	2284	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2286	2285	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2287	2286	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2288	2287	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2289	2288	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2290	2289	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2291	2290	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2292	2291	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2293	2292	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2295	2294	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2296	2295	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2297	2296	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2298	2297	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2300	2299	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2301	2300	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2303	2302	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2304	2303	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2305	2304	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2306	2305	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2307	2306	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2308	2307	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2309	2308	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2310	2309	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2311	2310	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2312	2311	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2313	2312	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2314	2313	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2315	2314	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2316	2315	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2317	2316	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2318	2317	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2319	2318	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2320	2319	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2323	2322	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2324	2323	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2325	2324	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2326	2325	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2327	2326	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2328	2327	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2329	2328	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2331	2330	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2332	2331	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2333	2332	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2335	2334	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2336	2335	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2337	2336	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2338	2337	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2339	2338	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2340	2339	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2341	2340	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2342	2341	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2343	2342	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2344	2343	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2345	2344	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2346	2345	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2347	2346	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2348	2347	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2350	2349	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2351	2350	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2352	2351	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2354	2353	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2355	2354	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2356	2355	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2357	2356	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2358	2357	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2359	2358	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2361	2360	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2362	2361	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2363	2362	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2364	2363	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2365	2364	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2366	2365	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2367	2366	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2368	2367	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2369	2368	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2371	2370	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2372	2371	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2373	2372	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2374	2373	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2375	2374	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2376	2375	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2377	2376	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2378	2377	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2379	2378	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2380	2379	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2381	2380	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2382	2381	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2383	2382	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2384	2383	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2385	2384	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2386	2385	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2387	2386	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2389	2388	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2390	2389	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2391	2390	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2392	2391	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2393	2392	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2394	2393	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2395	2394	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2396	2395	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2397	2396	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2398	2397	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2399	2398	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2405	2404	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2406	2405	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2407	2406	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2408	2407	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2409	2408	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2410	2409	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2411	2410	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2412	2411	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2419	2418	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2420	2419	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2421	2420	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2422	2421	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2423	2422	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2424	2423	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2425	2424	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2426	2425	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2427	2426	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2429	2428	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2430	2429	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2431	2430	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2432	2431	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2433	2432	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2434	2433	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2435	2434	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2436	2435	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2437	2436	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2438	2437	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2439	2438	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2440	2439	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2441	2440	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2442	2441	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2444	2443	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2445	2444	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2446	2445	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2447	2446	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2448	2447	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2449	2448	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2451	2450	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2452	2451	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2453	2452	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2454	2453	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2455	2454	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2456	2455	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2457	2456	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2458	2457	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2459	2458	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2460	2459	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2461	2460	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2462	2461	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2463	2462	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2464	2463	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2465	2464	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2466	2465	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2468	2467	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2469	2468	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2470	2469	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2471	2470	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2472	2471	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2473	2472	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2474	2473	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2475	2474	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2476	2475	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2477	2476	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2478	2477	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2479	2478	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2480	2479	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2481	2480	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2482	2481	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2483	2482	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2484	2483	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2485	2484	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2487	2486	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2488	2487	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2489	2488	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2490	2489	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2491	2490	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2492	2491	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2493	2492	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2494	2493	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2495	2494	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2496	2495	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2497	2496	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2498	2497	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2499	2498	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2500	2499	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2503	2502	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2504	2503	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2505	2504	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2506	2505	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2507	2506	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2508	2507	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2509	2508	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2510	2509	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2511	2510	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2512	2511	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2388	2387	5	Kakashi Sharingan Jounin	t	4	https://www.ninjamanager.com/clans/warzones/god-tree	t	Heart Container, Runestone, Quartz Crystal, Hyuga Blood	70,000 of 1,680,000 health.	Battle ends in 2 days 08h 09m.		\N
2513	2512	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2514	2513	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2515	2514	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2516	2515	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2517	2516	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2518	2517	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2519	2518	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2520	2519	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2521	2520	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2522	2521	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2523	2522	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2524	2523	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2525	2524	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2526	2525	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2527	2526	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2529	2528	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2530	2529	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2531	2530	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2532	2531	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2533	2532	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2534	2533	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2535	2534	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2536	2535	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2537	2536	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2538	2537	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2539	2538	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2540	2539	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2541	2540	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2542	2541	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2544	2543	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2545	2544	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2546	2545	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2547	2546	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2549	2548	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2552	2551	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2553	2552	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2554	2553	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2555	2554	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2556	2555	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2557	2556	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2559	2558	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2560	2559	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2561	2560	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2562	2561	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2563	2562	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2564	2563	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2565	2564	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2566	2565	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2567	2566	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2568	2567	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2570	2569	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2571	2570	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2572	2571	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2576	2575	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2577	2576	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2578	2577	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2579	2578	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2580	2579	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2581	2580	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2582	2581	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2583	2582	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2584	2583	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2585	2584	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2586	2585	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2587	2586	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2588	2587	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2589	2588	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2590	2589	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2591	2590	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2592	2591	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2593	2592	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2594	2593	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2595	2594	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2596	2595	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2597	2596	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2598	2597	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2599	2598	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2600	2599	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2602	2601	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2603	2602	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2604	2603	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2605	2604	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2606	2605	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2607	2606	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2610	2609	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2611	2610	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2613	2612	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2614	2613	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2615	2614	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2616	2615	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2617	2616	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2618	2617	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2620	2619	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2621	2620	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2622	2621	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2624	2623	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2625	2624	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2626	2625	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2627	2626	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2628	2627	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2629	2628	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2630	2629	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2631	2630	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2633	2632	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2634	2633	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2635	2634	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2636	2635	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2637	2636	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2638	2637	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2639	2638	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2640	2639	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2641	2640	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2642	2641	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2643	2642	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2644	2643	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2646	2645	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2647	2646	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2648	2647	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2649	2648	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2650	2649	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2651	2650	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2652	2651	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2653	2652	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2654	2653	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2655	2654	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2656	2655	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2657	2656	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2658	2657	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2659	2658	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2660	2659	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2661	2660	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2662	2661	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2663	2662	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2664	2663	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2665	2664	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2666	2665	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2667	2666	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2668	2667	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2632	2631	5	Obito Jinchuriki Unstable Kage	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Steam Armor, Darkwood, Otsutsuki Cells, Charm of Protection, Curse Mark Pattern, Crow Feather, Fish Scales	200,000 of 7,140,000 health.	Battle ends in 0 days 22h 32m.		\N
2670	2669	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2671	2670	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2672	2671	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2673	2672	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2674	2673	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2675	2674	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2676	2675	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2677	2676	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2678	2677	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2679	2678	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2680	2679	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2681	2680	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2682	2681	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2683	2682	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2684	2683	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2685	2684	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2686	2685	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2687	2686	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2689	2688	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2690	2689	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2691	2690	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2692	2691	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2693	2692	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2694	2693	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2695	2694	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2696	2695	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2697	2696	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2698	2697	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2699	2698	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2700	2699	5	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
499	498	1	Gaara Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Otsutsuki Cells, Zephyr Leaf, Turtle Shield, Refined Chakra Plate, Mind Awakening Pill, Wooden Talisman, Granite	20,000 of 1,120,000 health.	Battle ends in 4 days 17h 38m.		\N
751	750	1	Sakura Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Ghost Gloves, Ironbark, Charm of Protection, Azoth	10,000 of 640,000 health.	Battle ends in 2 days 22h 21m.	Ivory	\N
218	217	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
329	328	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
334	333	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
219	218	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2688	2687	5	Top Transformed Buddha Summon	f	86	https://www.ninjamanager.com/img/cards/upload/20/small/top-transformed-buddha-summon.png	t	Steam Armor, Heart Container, Yggdrasil Sap, Essence of Smoke, Poison Vial, Dark Chakra Eel, Leather	13,810,000 of 16,065,000 health.	Battle ends in 0 days 00h 03m.		\N
1	0	0	Itachi	\N	\N	\N	f	\N	\N	\N	\N	\N
153	152	0	Shino Chunin Genin	t	9	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Zephyr Leaf, Panacea, Artificial Chakra Core, Serpent Scales, Ink Bottle, Crow Feather, Turtle Shell	20,000 of 300,000 health.	Battle ends in 13 days 19h 07m.		\N
1422	1421	3	Fuguki Kage	t	4	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Spider Silk, Deathly Dust, Zephyr Leaf, Robot Parts	80,000 of 2,380,000 health.	Battle ends in 2 days 03h 34m.	Curse Mark Pattern, Quicklime, White Diamond, Iron Sand, Serpent Horn	\N
1701	1700	3	Killer B Evolved Jounin	f	79	https://www.ninjamanager.com/img/cards/upload/20/small/killer-b-evolved-jounin.png	t	Spider Silk, Anbu Mask, Molten Stone, Runic Talisman, Dharma Wheel, Serpent Scales	1,970,000 of 2,520,000 health.	Battle ends in 0 days 00h 05m.		\N
14	13	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
15	14	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
16	15	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
643	642	1	Sakura Evolved Genin	f	14	https://www.ninjamanager.com/img/cards/upload/20/small/sakura-evolved-genin.png	t	Opal, Azoth, Ice Shards	40,000 of 320,000 health.	Battle ends in 0 days 00h 03m.		0.0
801	800	1	Tenten Evolved Genin	f	90	https://www.ninjamanager.com/img/cards/upload/20/small/tenten-evolved-genin.png	t	Earth Shard, Ironbark, Ruby, Nature Transformation Chakra, Hyuga Blood	720,000 of 800,000 health.	Battle ends in 3 days 15h 59m.		0.0
769	768	1	Fuu Jounin	f	46	https://www.ninjamanager.com/img/cards/upload/20/small/fuu-jounin.png	t	Jade Dagger, Last Luck, Quartz Crystal, Essence of Smoke, Jinchuriki Chakra, Curse Mark Pattern	440,000 of 960,000 health.	Battle ends in 2 days 17h 59m.		0.0
71	70	0	Sasuke Chunin Genin	t	3	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Frozen Havoc, Quartz Crystal, Bezoar, Curse Mark Pattern, Robot Parts	10,000 of 375,000 health.	Battle ends in 3 days 10h 37m.		\N
1093	1092	2	Phantom Dragon Summon	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Thunder Discharge, Essence of Darkness, Zephyr Leaf, Quartz Crystal, Mind Awakening Pill, Uchiha Blood, Dirty Dice, Blood Vessel, Adamantine Bone	20,000 of 1,440,000 health.	Battle ends in 6 days 03h 21m.		\N
1650	1649	3	Mangetsu Kage	f	87	https://www.ninjamanager.com/img/cards/upload/20/small/mangetsu-kage.png	t	Darkwood, Dojutsu Catalyst, Charm of Protection, Jade, Dice, Wood	19,370,000 of 22,440,000 health.	Battle ends in 0 days 04h 27m.		\N
1099	1098	2	Ilya	\N	\N	\N	f	\N	\N	\N	\N	\N
924	923	2	Menma Chunin Genin	t	4	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Yggdrasil Bark, Weapons Quiver, Ice Shards, Charm of Protection, Black Receiver	40,000 of 1,375,000 health.	Battle ends in 1 days 15h 37m.		\N
1543	1542	3	Blue B Jounin	t	7	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Memory Recall, Snakeskin Suit, Arachne Web, Ceremonial Mask, Roran Coin	90,000 of 1,375,000 health.	Battle ends in 2 days 23h 48m.	Stem Cell, Artificial Chakra Core	\N
1500	1499	3	Ameyuri Kage	f	46	https://www.ninjamanager.com/img/cards/upload/20/small/ameyuri-kage.png	t	Valhalla Helm, Darkwood, Ghost Orb, White Diamond, Stem Cell, Dice, Bone	3,220,000 of 7,140,000 health.	Battle ends in 0 days 00h 00m.	Essence Of Swiftness, Adamantine Bone, Ruby, Essence Of Smoke, Artificial Chakra Core, Poison Vial	\N
1488	1487	3	Omoi Evolved Genin	f	45	https://www.ninjamanager.com/img/cards/upload/20/small/omoi-evolved-genin.png	t	Spider Silk, Opal, Nature Transformation Chakra, Snake Eye, Tainted Chakra, Snakeskin	1,130,000 of 2,520,000 health.	Battle ends in 0 days 00h 04m.		\N
1567	1566	3	Jinin Edo Jounin	f	66	https://www.ninjamanager.com/img/cards/upload/20/medium/jinin-edo-jounin.gif	t	Swift Strikes, Spirit Container, Dragon Soul, Talatat Stone, Mercury, Chakra Plate	18,770,000 of 28,800,000 health.	Battle ends in 0 days 00h 03m.	Jade	\N
220	219	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
221	220	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
222	221	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
223	222	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
224	223	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
570	569	1	Deidara Fn Jounin	t	17	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Suicide Bombing Clone, Dark Orb, Snake Serum, Aero Stone, Nature Transformation Chakra, Snake Eye, Ink Bottle, Wood	4,220,000 of 24,990,000 health.	Battle ends in 7 days 22h 04m.		\N
2052	2051	4	Obito Mask Kage	f	64	https://www.ninjamanager.com/img/cards/upload/20/small/obito-mask-kage.png	t	Molten Stone, Fire Shard, Muga Silk, Roran Coin, Wooden Talisman, Snakeskin, Chakra Plate, Rusty Spring	1,410,000 of 2,200,000 health.	Battle ends in 0 days 00h 00m.		\N
17	16	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
728	727	1	Kimimaro Cs2 Jounin	f	100	https://www.ninjamanager.com/img/cards/upload/20/small/kimimaro-cs2-jounin.png	t	Wolf Mantle, Celestial Dye, Essence of Smoke	320,000 of 320,000 health.	Battle ends in 1 days 23h 59m.		0.0
534	533	1	Ino Evolved Genin	f	9	https://www.ninjamanager.com/img/cards/upload/20/small/ino-evolved-genin.png	t	Serpent Armor, Dharma Spirit, Blight Rod, Jinchuriki Chakra, Azoth	70,000 of 800,000 health.	Battle ends in 2 days 19h 59m.	Cracked Anbu Mask	0.0
1100	1099	2	Kurotsuchi Evolved Jounin	f	37	https://www.ninjamanager.com/img/cards/upload/20/small/kurotsuchi-evolved-jounin.png	t	Runestone, Azoth, Charm of Protection, Tainted Chakra, Serpent Scales, Turtle Shell, Rusty Spring	800,000 of 2,200,000 health.	Battle ends in 0 days 00h 00m.		0.0
602	601	1	Sakura Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Twisted Mask, White Diamond, Muga Silk, Ice Shards, Mind Awakening Pill, Black Receiver	20,000 of 960,000 health.	Battle ends in 1 days 15h 21m.		\N
1745	1744	3	Ginkaku Jounin	f	93	https://www.ninjamanager.com/img/cards/upload/20/small/ginkaku-jounin.png	t	Babylonian Stone$250, Elvenskin$100, Hero Water$53, Ruby$50, Quicklime$25, Granite$17, Crow Feather$9	2,710,000 of 2,940,000 health.	Battle ends in 6 days 06h 05m.		3.52
502	501	1	Kimimaro Cs2 Jounin	f	9	https://www.ninjamanager.com/img/cards/upload/20/small/kimimaro-cs2-jounin.png	t	Water Shard$75, Talatat Stone$75, White Diamond$50, Ascension Ore$50, Wooden Talisman$25, Panacea$25, Sparkless Wind Shard$15	40,000 of 1,120,000 health.	Battle ends in 4 days 12h 28m.		0.0
1712	1711	3	Kushimaru Kage	f	74	https://www.ninjamanager.com/img/cards/upload/20/small/kushimaru-kage.png	t	Ice Brand$750, Darkwood$313, Deathly Dust$167, Muga Silk$50, Snake Eye$25, Dark Chakra Eel$7, Clay$4	3,270,000 of 4,462,500 health.	Battle ends in 3 days 11h 05m.		5.23
832	831	1	Gaara Evolved Genin	f	71	https://www.ninjamanager.com/img/cards/upload/20/small/gaara-evolved-genin.png	t	Ghost Touch$200, Magma$100, Essence of Darkness$50, Charm of Protection$50, Refined Chakra Plate$42, Curse Mark Pattern$25, Dirty Dice$20, Granite$17, Jinchuriki Seal$13	1,230,000 of 1,760,000 health.	Battle ends in 8 days 21h 28m.		4.34
972	971	2	Onoki Kage	f	64	https://www.ninjamanager.com/img/cards/upload/20/small/onoki-kage.png	t	Jade Dagger$250, Susanoo Residue$200, Runestone$125, Biju Silk$75, Iron Sand$25, Lava$13, Dice$9	1,880,000 of 2,940,000 health.	Battle ends in 6 days 00h 17m.	Panacea	6.3
999	998	2	Temari Evolved Genin	f	5	https://www.ninjamanager.com/img/cards/upload/20/small/temari-evolved-genin.png	t	Aqua Stone$125, Ice Shards$50, Muga Silk$50, Snake Eye$25, Iron$7, Turtle Shell$6, Rusty Spring$3	240,000 of 5,500,000 health.	Battle ends in 1 days 16h 17m.		0.17
1152	1151	2	Phantom Dragon Summon	f	64	https://www.ninjamanager.com/img/cards/upload/20/small/phantom-dragon-summon.png	t	Adamantine Erosion$400, Heart Container$250, Aero Stone$125, Demon Wing$50, Jade$25, Adamantine Bone$17	1,590,000 of 2,520,000 health.	Battle ends in 1 days 14h 16m.		4.44
995	994	2	Reibi Summon	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Elemental Rune, Ghost Orb, Ceremonial Mask, Muga Silk	30,000 of 1,100,000 health.	Battle ends in 2 days 08h 31m.		\N
1895	1894	4	Han Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Serpent Pike, Ancient Coin, Dojutsu Catalyst, Ice Shards, Roran Coin, Robot Parts, Snakeskin, Spider Web	80,000 of 3,780,000 health.	Battle ends in 5 days 21h 35m.	Ghost Spirit, Essence Of Swiftness, Mercury	\N
2275	2274	5	Kakashi Sharingan Jounin	t	4	https://www.ninjamanager.com/clans/warzones/god-tree	t	Mecha Puppet, Imperial Jade, Soil Stone, Essence of Smoke, Uchiha Blood, Granite, Dice, Red Sand	140,000 of 5,040,000 health.	Battle ends in 9 days 05h 02m.	Mind Awakening Pill	\N
1950	1949	4	Yagura Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Memory Recall, Snake Serum, Jade Dagger, Aqua Stone, Essence of Smoke, Panacea, Granite, Bronze Coin, Jutsu Scroll	120,000 of 4,200,000 health.	Battle ends in 4 days 03h 56m.		\N
2162	2161	4	Gengetsu Kage	f	30	https://www.ninjamanager.com/img/cards/upload/20/small/gengetsu-kage.png	t	Electrum Coin, Babylonian Stone, Arachne Web, Bezoar, Curse Mark Pattern, Ink Bottle, Fish Scales	2,090,000 of 7,140,000 health.	Battle ends in 0 days 00h 00m.		\N
1803	1802	4	Madara Fn Kage	f	40	https://www.ninjamanager.com/img/cards/upload/20/medium/madara-fn-kage.gif	t	Inksealed Talisman, Atlantic Artifact, Dragon Bone, Opal, Robot Parts, Iron, Bone	15,240,000 of 38,812,500 health.	Battle ends in 0 days 00h 02m.		\N
1394	1393	3	Ameyuri Edo Jounin	t	2	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Dark Orb, Wolf Eye, Venoxin Mutagen, Runic Talisman, Cursed Feather, Puppet Part	100,000 of 9,800,000 health.	Battle ends in 1 days 07h 37m.		\N
4	3	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
5	4	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
6	5	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
7	6	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
8	7	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
823	822	1	Rock Lee Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Poison Coating, Celestial Dye, Steel, Azoth, Berserker Enzyme, Granite, Sparkless Lightning Shard, Ghost Spirit, Cursed Feather	50,000 of 2,400,000 health.	Battle ends in 8 days 17h 56m.		\N
2353	2352	5	Minato Kage	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Ice Brand, Robot Processor, Dragon Blood, Zephyr Leaf, Jade, Lion Fur, Leather	160,000 of 7,140,000 health.	Battle ends in 4 days 09h 58m.		\N
1658	1657	3	Gari Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Babylonian Stone, Aero Stone, Nature Transformation Chakra, Jade, Cursed Feather, Crow Feather	60,000 of 2,520,000 health.	Battle ends in 1 days 20h 58m.		\N
1900	1899	4	Utakata Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Reroll Counter, Lotus Amulet, Yggdrasil Sap, Talatat Stone, Stem Cell, Ivory, Snakeskin, Red Sand	110,000 of 5,040,000 health.	Battle ends in 3 days 19h 12m.		\N
2404	2403	5	Guy 8Gates Kage	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Drench, Python Longevity Serum, Monkey King Fur, Celestial Dye, Mercury, Chakra Plate	200,000 of 9,000,000 health.	Battle ends in 2 days 20h 17m.		\N
1374	1373	3	Omoi Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Elemental Rune, Snake Serum, Detonating Clay, Runestone	40,000 of 1,680,000 health.	Battle ends in 2 days 13h 35m.	Mercury, Artificial Chakra Core	\N
1052	1051	2	Chimera Summon	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Serpent Pike, Aero Stone, Dragon Blood, Quartz Crystal, Berserker Enzyme, Anbu Paint, Clay	40,000 of 1,925,000 health.	Battle ends in 2 days 18h 11m.		\N
1445	1444	3	Killer B Jinchuriki Genin	f	53	https://www.ninjamanager.com/img/cards/upload/20/small/killer-b-jinchuriki-genin.png	t	Darkwood, Ghost Orb, White Diamond, Granite, Lion Fur, Bone	6,810,000 of 13,090,000 health.	Battle ends in 4 days 08h 57m.		\N
11	10	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
12	11	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
13	12	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1193	1192	2	Onoki Kage	f	85	https://www.ninjamanager.com/img/cards/upload/20/small/onoki-kage.png	t	Spider Silk, Runestone, Azoth, Jade, Cursed Feather, Black Threads	2,120,000 of 2,520,000 health.	Battle ends in 0 days 00h 04m.		0.0
1078	1077	2	Torune Jounin	f	54	https://www.ninjamanager.com/img/cards/upload/20/small/torune-jounin.png	t	Molten Stone, Demon Wing, Roran Coin	150,000 of 275,000 health.	Battle ends in 0 days 00h 01m.		0.0
18	17	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
19	18	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
20	19	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
21	20	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
808	807	1	Rock Lee Evolved Genin	t	11	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Chakra Detonation, Ghost Gloves, Talatat Stone	20,000 of 160,000 health.	Battle ends in 0 days 02h 52m.		\N
163	162	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
32	31	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
33	32	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1453	1452	3	Killer B Evolved Jounin	f	57	https://www.ninjamanager.com/img/cards/upload/20/small/killer-b-evolved-jounin.png	t	Mind Recovery$400, Monkey King Fur$200, Magma$100, Ruby$50, Hyuga Blood$25, Poison Vial$17, Black Threads$9	1,660,000 of 2,940,000 health.	Battle ends in 4 days 20h 08m.		2.13
1282	1281	2	Danzo Kage	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Opal, Illusion Pendant, Quartz Crystal, Essence of Darkness, Hyuga Blood	10,000 of 800,000 health.	Battle ends in 2 days 01h 41m.		\N
1550	1549	3	Gari Jounin	f	98	https://www.ninjamanager.com/img/cards/upload/20/small/gari-jounin.png	t	Artificial Rasengan$400, Lotus Amulet$200, Elvenskin$100	1,220,000 of 1,260,000 health.	Battle ends in 2 days 13h 05m.		5.29
1338	1337	2	Danzo Kage	t	97	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Focus Counter$400, Snakeskin Suit$206, Susanoo Residue$200, Thunder Stone$125, Bezoar$50, Snake Eye$25, Tainted Chakra$13, Crow Feather$9, Chakra Plate$5	8,540,000 of 8,820,000 health.	Battle ends in 20 days 06h 35m.		1.74
903	902	2	Jugo Fn Jounin	f	70	https://www.ninjamanager.com/img/cards/upload/20/medium/jugo-fn-jounin.png	t	Spider Silk$250, Otsutsuki Cells$100, Essence of Smoke$50, Quicklime$25, Ink Bottle$10, Fish Scales$3	11,380,000 of 16,362,500 health.	Battle ends in 18 days 21h 04m.		0.31
1020	1019	2	Jugo Fn Jounin	t	2	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Style One, Ancient Coin, Aero Stone, Refined Chakra Plate, Black Receiver, Black Threads, Leather	100,000 of 10,710,000 health.	Battle ends in 5 days 19h 56m.	Essence Of Darkness, Ice Shards, Nature Transformation Chakra, Quartz Crystal, Demon Wing, Jinchuriki Chakra	\N
1199	1198	2	Samui Evolved Genin	t	5	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Detonating Clay, Chimera Heart, Steel	30,000 of 550,000 health.	Battle ends in 0 days 19h 47m.		\N
560	559	1	Mother Puppet Summon	t	4	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Bionic Shinobi Gauntlet, Steel, Demon Wing	10,000 of 320,000 health.	Battle ends in 0 days 12h 21m.		\N
949	948	2	Menma Chunin Genin	t	5	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Yggdrasil Sap, Lightning Shard, Bezoar, Curse Mark Pattern, Poison Vial, Serpent Scales, Wood, Rusty Spring	150,000 of 4,125,000 health.	Battle ends in 5 days 15h 36m.		\N
453	452	1	Sakura Fn Jounin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Bone Pendant, Babylonian Stone, Deathly Dust, Ascension Ore, Jade, Dark Chakra Eel, Fish Scales	110,000 of 5,950,000 health.	Battle ends in 3 days 16h 36m.	Nature Transformation Chakra	\N
522	521	1	Ryuzetsu Jounin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Steel, Demon Wing, Charm of Protection, Turtle Shield, Uchiha Blood, Jade, Berserker Enzyme, Cursed Feather	60,000 of 2,400,000 health.	Battle ends in 9 days 09h 51m.	Ghost Spirit, Panacea, Ink Bottle, Blood Vessel	\N
22	21	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1732	1731	3	Fuguki Kage	f	96	https://www.ninjamanager.com/img/cards/upload/20/small/fuguki-kage.png	t	Osteoclast, Opal, Steel, Artificial Chakra Core, Snakeskin, Fish Scales	6,230,000 of 6,545,000 health.	Battle ends in 7 days 04h 57m.		\N
2569	2568	5	Minato Kage	f	78	https://www.ninjamanager.com/img/cards/upload/20/small/minato-kage.png	t	Molten Hammer, Chakra Shield, Spirit Container, Dragon Soul, Talatat Stone, Ghost Spirit, Turtle Shell	32,700,000 of 42,000,000 health.	Battle ends in 0 days 00h 01m.	Ruby, Uchiha Blood, Jade, Panacea, Roran Coin, Nature Transformation Chakra	\N
23	22	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
202	201	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
203	202	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
26	25	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
27	26	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
28	27	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
29	28	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1038	1037	2	Ao Jounin	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Mecha Puppet, Deathly Dust, Essence of Smoke, Ice Shards, Black Receiver	30,000 of 1,375,000 health.	Battle ends in 3 days 23h 10m.	Quicklime, Blood Vessel, Iron Sand	\N
1788	1787	3	Chukichi Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Chimera DNA, Vajra, Aero Stone, Quartz Crystal, Uchiha Crest, Adamantine Bone, Cracked Anbu Mask, Spider Web	90,000 of 4,200,000 health.	Battle ends in 5 days 18h 04m.		\N
259	258	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
260	259	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
261	260	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
262	261	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
138	137	0	Gozu Evolved Genin	t	4	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Undying Mask, Ice Shards, Ascension Ore, Wooden Talisman	10,000 of 300,000 health.	Battle ends in 2 days 21h 48m.		\N
743	742	1	Fuu Jinchuriki Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Essence of Darkness, Essence of Smoke, Turtle Shield, Hyuga Blood, Dharma Wheel	10,000 of 375,000 health.	Battle ends in 3 days 17h 54m.		\N
178	177	0	Kabuto Jounin	t	4	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Chimera Heart, Nature Transformation Chakra, Azoth, Uchiha Blood, Blood Vessel, Ivory, Ghost Spirit, Antique Mask	20,000 of 600,000 health.	Battle ends in 7 days 08h 24m.		\N
34	33	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
35	34	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
36	35	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
37	36	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
38	37	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1219	1218	2	Mei Kage	f	98	https://www.ninjamanager.com/img/cards/upload/20/small/mei-kage.png	t	Adamantine Erosion$400, Ancient Coin$250, Jade Dagger$250, Steam Stone$125, Ironbark$63, Snake Eye$25, Tainted Chakra$13, Crow Feather$9, Puppet Part$5	6,540,000 of 6,720,000 health.	Battle ends in 15 days 13h 49m.		3.29
2502	2501	5	Kakashi Sharingan Jounin	t	6	https://www.ninjamanager.com/clans/warzones/god-tree	t	Monkey King Fur, Yggdrasil Sap, White Diamond	50,000 of 1,260,000 health.	Battle ends in 1 days 11h 06m.		\N
1399	1398	3	Kinkaku Jounin	f	93	https://www.ninjamanager.com/img/cards/upload/20/small/kinkaku-jounin.png	t	Reroll Counter$400, Imperial Jade$250, Dojutsu Catalyst$150, Detonating Clay$125, Bezoar$50, Uchiha Blood$25	2,330,000 of 2,520,000 health.	Battle ends in 5 days 15h 05m.		3.01
1200	1199	2	Baku Summon	f	25	https://www.ninjamanager.com/img/cards/upload/20/small/baku-summon.png	t	Charm of Protection$50, Bezoar$50, Quartz Crystal$50, Artificial Chakra Core$25, Wooden Talisman$25, Berserker Enzyme$21, Prototype Chakra Gauntlet$20, Serpent Horn$17	430,000 of 1,760,000 health.	Battle ends in 1 days 09h 35m.		0.26
925	924	2	Torune Jounin	t	36	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Serpent Soul$167, White Diamond$50, Jinchuriki Chakra$50	290,000 of 825,000 health.	Battle ends in 2 days 14h 35m.		47.76
1003	1002	2	Tobi Jounin	f	46	https://www.ninjamanager.com/img/cards/upload/20/small/tobi-jounin.png	t	Runestone$125, Magma$100, Demon Wing$50, Uchiha Blood$25, Antique Mask$10, Puppet Part$5, Fish Scales$3	1,860,000 of 4,125,000 health.	Battle ends in 9 days 12h 17m.		1.64
474	473	1	Shukaku Summon	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Dharma Spirit, Celestial Dye, Illusion Pendant, Bezoar, White Diamond	20,000 of 800,000 health.	Battle ends in 4 days 08h 01m.	Rune Word, Robot Parts	\N
1450	1449	3	Ameyuri Kage	f	98	https://www.ninjamanager.com/img/cards/upload/20/small/ameyuri-kage.png	t	Electrum Coin$375, Heart Container$250, Thunder Stone$125, Ruby$50, Mind Awakening Pill$25, Bronze Coin$7, Clear Water$4	12,240,000 of 12,495,000 health.	Battle ends in 12 days 20h 08m.	Quartz Crystal	3.51
2195	2194	4	Tsunade Kage	f	85	https://www.ninjamanager.com/img/cards/upload/20/small/tsunade-kage.png	t	Heart Container, Arachne Web, Ironbark, Black Receiver, Antique Mask, Fish Scales	3,500,000 of 4,165,000 health.	Battle ends in 0 days 00h 00m.		0.0
1054	1053	2	Darui Jounin	t	5	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Soil Stone, Ancient Blood, Uchiha Crest, Uchiha Blood, Serpent Scales	50,000 of 1,375,000 health.	Battle ends in 2 days 23h 02m.	Serpent Horn, Cursed Feather, Granite	\N
603	602	1	Kiba Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Tainted Shuriken, Demon Wing, Ice Shards, Charm of Protection, Fuma Shuriken, Uchiha Blood, Jade, Robot Parts, Black Receiver	70,000 of 3,200,000 health.	Battle ends in 11 days 00h 27m.		\N
1643	1642	3	Ginkaku Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Muscle Hardening, Robot Processor, Dharma Spirit, Runic Talisman, Uchiha Blood, Cursed Feather	50,000 of 2,520,000 health.	Battle ends in 4 days 14h 04m.		\N
1953	1952	4	Madara Kage	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Adamantine Kunai, Runestone, Opal	10,000 of 440,000 health.	Battle ends in 0 days 19h 05m.		\N
1849	1848	4	Yugito Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Guiding Wings, Adamantine Kunai, Venoxin Mutagen, Elvenskin, Essence of Darkness, Panacea	60,000 of 2,520,000 health.	Battle ends in 1 days 22h 06m.		\N
1010	1009	2	Onoki Kage	t	7	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Tortoise Carapace, Ghost Orb, Thunder Rod	20,000 of 320,000 health.	Battle ends in 1 days 07h 46m.		\N
702	701	1	Chiyo Kage	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Chimera Heart, Nature Transformation Chakra, Refined Chakra Plate, Uchiha Blood	10,000 of 300,000 health.	Battle ends in 2 days 22h 28m.		\N
2467	2466	5	Guy Fn Kage	f	64	https://www.ninjamanager.com/img/cards/upload/20/medium/guy-fn-kage.gif	t	Hashirama's DNA, Steam Stone, Muga Silk, Iron Sand, Black Threads, Clear Water	27,830,000 of 43,680,000 health.	Battle ends in 0 days 00h 03m.		\N
545	544	1	Fuu Jinchuriki Genin	t	4	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Serpent Soul, Essence of Darkness, Quartz Crystal	10,000 of 275,000 health.	Battle ends in 0 days 09h 31m.	Black Receiver	\N
204	203	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
205	204	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1460	1459	3	Kinkaku Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Jade Awl, Lotus Amulet, Ghost Orb, Muga Silk, Iron Sand	40,000 of 2,100,000 health.	Battle ends in 2 days 02h 39m.	Ruby, Ice Shards	\N
1258	1257	2	A Kage	f	4	https://www.ninjamanager.com/img/cards/upload/20/small/a-kage.png	t	Dragon Bone, Magma, Bezoar, Jade, Rune Word, Snakeskin, Clay	120,000 of 3,780,000 health.	Battle ends in 0 days 00h 03m.	Robot Parts	\N
2173	2172	4	Mu Kage	t	8	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Aero Stone, Ceremonial Mask, Iron Sand	30,000 of 550,000 health.	Battle ends in 1 days 03h 58m.		\N
1528	1527	3	Omoi Evolved Genin	f	96	https://www.ninjamanager.com/img/cards/upload/20/small/omoi-evolved-genin.png	t	Steam Stone, Water Shard, Runic Talisman, Ascension Ore, Rune Word, Black Threads, Bronze Coin, Bone	2,370,000 of 2,475,000 health.	Battle ends in 5 days 13h 57m.		\N
1208	1207	2	A Kage	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Serpent Pike, Chimera Burst, Opal, Salamander Venom, Nature Transformation Chakra	20,000 of 800,000 health.	Battle ends in 1 days 20h 04m.		\N
1421	1420	3	Kushimaru Kage	f	60	https://www.ninjamanager.com/img/cards/upload/20/small/kushimaru-kage.png	t	Chakra Compress, Electrum Coin, Heart Container, Molten Stone, Essence of Darkness, Dharma Wheel, Antique Mask, Leather	5,600,000 of 9,371,250 health.	Battle ends in 1 days 18h 57m.		\N
263	262	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
264	263	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
265	264	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
888	887	1	Rock Lee Evolved Genin	f	96	https://www.ninjamanager.com/img/cards/upload/20/small/rock-lee-evolved-genin.png	t	Chakra Detonation, Tainted Shuriken, Salamander Venom, Quartz Crystal, White Diamond	760,000 of 800,000 health.	Battle ends in 3 days 07h 58m.		0.0
2182	2181	4	Utakata Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Artificial Rasengan, Heart Container, Dojutsu Catalyst, Fire Shard, Ascension Ore, Artificial Chakra Core	60,000 of 2,520,000 health.	Battle ends in 4 days 16h 40m.	Curse Mark Pattern	\N
1723	1722	3	Karui Evolved Genin	f	15	https://www.ninjamanager.com/img/cards/upload/20/small/karui-evolved-genin.png	t	Spider Silk, Yggdrasil Bark, Salamander Venom, Roran Coin, Black Receiver, Cracked Anbu Mask, Wolf Pelt	600,000 of 4,200,000 health.	Battle ends in 2 days 16h 57m.	Azoth, Ghost Spirit, Adamantine Bone, Zephyr Leaf	\N
1649	1648	3	Pakura Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Reroll Counter, Serpent Soul, Anbu Mask, Ascension Ore, Charm of Protection	30,000 of 1,375,000 health.	Battle ends in 3 days 06h 16m.		\N
1951	1950	4	Neji Evolved Genin	t	4	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Heart Container, Otsutsuki Cells, White Diamond	30,000 of 840,000 health.	Battle ends in 1 days 05h 21m.	Bezoar	\N
2159	2158	4	Itachi Jounin	t	4	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Reroll Counter, Snake Serum, Vajra, Arachne Web, Azoth	70,000 of 2,100,000 health.	Battle ends in 2 days 19h 21m.		\N
39	38	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
40	39	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
170	169	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
171	170	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1719	1718	3	Pakura Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Green Jumpsuit, Lotus Amulet, Dojutsu Catalyst, Nature Transformation Chakra, Mind Awakening Pill, Black Receiver	70,000 of 2,520,000 health.	Battle ends in 5 days 05h 02m.	Curse Mark Pattern	\N
1319	1318	2	Sasuke Susanoo Kage	f	72	https://www.ninjamanager.com/img/cards/upload/20/medium/sasuke-susanoo-kage.png	t	Molten Hammer$750, Osteoblast$417, Yggdrasil Sap$150, Refined Chakra Plate$42, Poison Vial$17, Cracked Anbu Mask$9, Clear Water$4	13,360,000 of 18,742,500 health.	Battle ends in 12 days 15h 16m.		1.15
300	299	0	Reibi Summon	t	26	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Artificial Chakra Core, Shuriken, Tainted Chakra	10,000 of 60,000 health.	Battle ends in 2 days 22h 14m.		\N
1434	1433	3	Killer B Evolved Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Adamantine Erosion, Spider Silk, Thunder Stone, Ironbark, Iron Sand, Robot Parts	60,000 of 2,520,000 health.	Battle ends in 4 days 03h 13m.	Quicklime, Tainted Chakra, Charm Of Protection, Zephyr Leaf	\N
1449	1448	3	Chukichi Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Reroll Counter, Snake Serum, Ghost Orb, Charm of Protection, Snake Eye, Tainted Chakra, Lion Fur, Ice Horn	80,000 of 3,780,000 health.	Battle ends in 3 days 23h 16m.		\N
1586	1585	3	Killer B Evolved Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Monkey King Fur, Holy Water, Charm of Protection, Dharma Wheel, Serpent Horn, Crow Feather, Red Sand	140,000 of 6,720,000 health.	Battle ends in 4 days 20h 03m.	Black Receiver, Iron Sand, Stem Cell, Uchiha Blood, Snake Eye	\N
2528	2527	5	Minato Kage	f	91	https://www.ninjamanager.com/img/cards/upload/20/small/minato-kage.png	t	Osteoclast, Aero Stone, Ironbark, Berserker Enzyme, Antique Mask, Bone	5,940,000 of 6,545,000 health.	Battle ends in 7 days 04h 56m.		\N
2000	1999	4	Yagura Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Monkey King Fur, Dragon Blood, Fire Shard, Ascension Ore, Stem Cell, Blood Vessel	50,000 of 2,520,000 health.	Battle ends in 2 days 11h 05m.		\N
1871	1870	4	Obito Cracked Jounin	f	22	https://www.ninjamanager.com/img/cards/upload/20/small/obito-cracked-jounin.png	t	Snake Perseverance, Chimera DNA, Runestone, White Diamond, Robot Parts, Black Threads, Clay	1,110,000 of 5,206,250 health.	Battle ends in 3 days 00h 57m.	Iron Sand, Artificial Chakra Core, Snake Eye, Uchiha Blood	0.0
1178	1177	2	Chojuro Evolved Genin	t	4	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Memory Recall, Dragon Blood, Ceremonial Mask, Hyuga Blood, Uchiha Blood	50,000 of 1,375,000 health.	Battle ends in 4 days 02h 23m.		\N
2623	2622	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Susanoo Residue, Chimera Heart, Thunder God Kunai, White Diamond, Artificial Chakra Core, Black Receiver	40,000 of 2,520,000 health.	Battle ends in 4 days 03h 19m.		\N
206	205	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
207	206	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2612	2611	5	Guy Crimson Jounin	t	6	https://www.ninjamanager.com/clans/warzones/god-tree	t	Magnetic Field, Spider Silk, Dharma Spirit, Azoth	90,000 of 1,680,000 health.	Battle ends in 2 days 15h 03m.		\N
2619	2618	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Elemental Rune, Chimera DNA, Ceremonial Mask	30,000 of 1,260,000 health.	Battle ends in 0 days 17h 03m.		\N
550	549	1	Ino Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Dojutsu Catalyst, Dragon Blood, Celestial Dye, Refined Chakra Plate, Stem Cell, Artificial Chakra Core, Adamantine Bone, Sparkless Water Shard, Mercury	30,000 of 1,440,000 health.	Battle ends in 7 days 05h 48m.		\N
1708	1707	3	Omoi Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Magnetic Field, Robot Processor, Adamantine Kunai, Ceremonial Mask, Steel, Uchiha Blood, Adamantine Bone, Black Threads, Red Sand	180,000 of 8,820,000 health.	Battle ends in 2 days 08h 30m.	Quicklime, Salamander Venom, Wooden Talisman, Ruby, Rune Word	\N
266	265	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
267	266	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
600	599	1	Father Puppet Summon	f	39	https://www.ninjamanager.com/img/cards/upload/20/small/father-puppet-summon.png	t	Steel, Zephyr Leaf, Essence of Swiftness, Curse Mark Pattern, Iron Sand	310,000 of 800,000 health.	Battle ends in 1 days 21h 59m.		0.0
475	474	1	Ebizo Kage	f	62	https://www.ninjamanager.com/img/cards/upload/20/small/ebizo-kage.png	t	Magma, Azoth, Refined Chakra Plate, Wooden Talisman, Ivory, Red Sand, Rusty Spring	1,520,000 of 2,475,000 health.	Battle ends in 5 days 11h 59m.		0.0
2321	2320	5	Guy Crimson Jounin	f	80	https://www.ninjamanager.com/img/cards/upload/20/small/guy-crimson-jounin.png	t	Ancient Coin, Dragon Blood, Wind Shard, Ruby, Quicklime, Blood Vessel, Crow Feather	2,340,000 of 2,940,000 health.	Battle ends in 0 days 00h 04m.		\N
1852	1851	4	Roshi Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Robot Processor, Jade Awl, Elvenskin, Essence of Smoke	40,000 of 1,680,000 health.	Battle ends in 2 days 01h 34m.		\N
1430	1429	3	Gari Jounin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Chimera DNA, Aqua Stone, Lightning Shard, Nature Transformation Chakra, Wooden Talisman, Adamantine Bone, Dice, Spider Web	140,000 of 6,720,000 health.	Battle ends in 1 days 03h 38m.	Iron Sand	\N
2403	2402	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Chimera Gauntlets, Venoxin Mutagen, Ghost Orb, Bezoar, Jade, Granite, Black Threads, Leather	130,000 of 5,600,000 health.	Battle ends in 1 days 00h 42m.	Blood Vessel, Magma, Lava	\N
2302	2301	5	Kakashi Sharingan Jounin	f	38	https://www.ninjamanager.com/img/cards/upload/20/small/kakashi-sharingan-jounin.png	t	Snakeskin Suit, Lotus Amulet, Dojutsu Catalyst, Talatat Stone, Jade, Poison Vial, Bronze Coin, Puppet Part	1,910,000 of 5,040,000 health.	Battle ends in 10 days 18h 56m.		\N
1993	1992	4	Nagato Kage	f	49	https://www.ninjamanager.com/img/cards/upload/20/small/nagato-kage.png	t	Chimera DNA, Anbu Mask, Elvenskin, Quartz Crystal, Panacea, Cursed Feather	1,220,000 of 2,520,000 health.	Battle ends in 0 days 00h 03m.		\N
172	171	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
173	172	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1899	1898	4	Crow Genjutsu Summon	t	4	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Imperial Jade, Jade Dagger, Thunder Stone	30,000 of 840,000 health.	Battle ends in 1 days 20h 20m.		\N
1651	1650	3	Karui Evolved Genin	f	5	https://www.ninjamanager.com/img/cards/upload/20/small/karui-evolved-genin.png	t	Lotus Amulet$200, Dojutsu Catalyst$150, Runic Talisman$75, Iron Sand$25, Lava$13	90,000 of 2,100,000 health.	Battle ends in 2 days 06h 08m.		\N
1602	1601	3	Blue B Jounin	f	52	https://www.ninjamanager.com/img/cards/upload/20/small/blue-b-jounin.png	t	Spider Silk$250, Otsutsuki Cells$100, White Diamond$50	430,000 of 840,000 health.	Battle ends in 1 days 15h 05m.		7.74
1353	1352	3	Jinpachi Edo Jounin	f	89	https://www.ninjamanager.com/img/cards/upload/20/medium/jinpachi-edo-jounin.gif	t	Swift Strikes$1,500, Drench$800, Molten Hammer$750, Spider Gland$500, Susanoo Residue$200, Ancient Blood$84, Rune Word$13, Spider Web$5	25,520,000 of 28,800,000 health.	Battle ends in 8 days 08h 08m.		3.51
749	748	1	Gaara Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Turtle Defense, Hero Water, Nature Transformation Chakra, Bezoar, Charm of Protection, Uchiha Blood, Quicklime, Berserker Enzyme, Sparkless Wind Shard, Cursed Feather	80,000 of 3,200,000 health.	Battle ends in 12 days 11h 34m.	Jade, Crow Feather	\N
2360	2359	5	Guy Crimson Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Snake Serum, Dojutsu Catalyst, Charm of Protection, Hyuga Blood, Mercury, Snakeskin, Ice Horn	190,000 of 8,820,000 health.	Battle ends in 16 days 14h 19m.		\N
2299	2298	5	Kakashi Sharingan Jounin	t	5	https://www.ninjamanager.com/clans/warzones/god-tree	t	Reroll Counter, Snake Serum, Otsutsuki Cells, Hero Water, Demon Wing, Curse Mark Pattern	100,000 of 2,520,000 health.	Battle ends in 0 days 02h 42m.	Roran Coin	\N
2017	2016	4	Itachi Fn Kage	f	65	https://www.ninjamanager.com/img/cards/upload/20/medium/itachi-fn-kage.gif	t	Heart of Mist, Inferno Artifact, Snake Serum, Elvenskin, Poison Vial, Snakeskin, Rusty Spring	16,750,000 of 25,875,000 health.	Battle ends in 0 days 00h 02m.		0.0
2119	2118	4	Han Jounin	f	15	https://www.ninjamanager.com/img/cards/upload/20/small/han-jounin.png	t	Heart Container, Aero Stone, Ascension Ore, Fuma Shuriken, Jade, Ivory, Crow Feather, Wolf Pelt	1,300,000 of 8,820,000 health.	Battle ends in 8 days 11h 56m.		\N
1136	1135	2	Reibi Summon	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Mecha Puppet, Susanoo Residue, Elvenskin	20,000 of 550,000 health.	Battle ends in 1 days 08h 54m.		\N
758	757	1	Ebizo Kage	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Serpent Pike, Monkey King Fur, Ruby, Jade, Mercury	30,000 of 1,375,000 health.	Battle ends in 1 days 20h 56m.	Wooden Talisman, Curse Mark Pattern, Blood Vessel, Snake Eye	\N
2486	2485	5	Hashirama Kage	f	70	https://www.ninjamanager.com/img/cards/upload/20/small/hashirama-kage.png	t	Electrum Coin, Imperial Jade, Holy Water, Refined Chakra Plate, Uchiha Blood, Snakeskin, Leather	4,960,000 of 7,140,000 health.	Battle ends in 0 days 00h 02m.		\N
2151	2150	4	Madara Kage	f	62	https://www.ninjamanager.com/img/cards/upload/20/small/madara-kage.png	t	Valhalla Helm, Robot Processor, Molten Stone, White Diamond, Uchiha Crest, Lion Fur, Rusty Spring	4,380,000 of 7,140,000 health.	Battle ends in 0 days 00h 04m.		\N
2601	2600	5	Guy Crimson Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Muscle Hardening, Dragon Soul, Chimera Gauntlets, Dojutsu Catalyst, Zephyr Leaf, Mind Awakening Pill	40,000 of 2,520,000 health.	Battle ends in 2 days 13h 01m.		\N
984	983	2	Mifune Kage	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Robot Processor, Vajra, Chimera Heart, Celestial Dye, Stem Cell, Black Receiver, Lion Fur, Puppet Part	100,000 of 4,200,000 health.	Battle ends in 0 days 09h 05m.	Robot Parts	\N
208	207	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
209	208	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
210	209	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
211	210	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
212	211	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
213	212	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
2443	2442	5	Obito Jinchuriki Unstable Kage	f	73	https://www.ninjamanager.com/img/cards/upload/20/small/obito-jinchuriki-unstable-kage.png	t	Darkwood, Soil Stone, Charm of Protection, Curse Mark Pattern, Black Threads, Bone	12,590,000 of 17,340,000 health.	Battle ends in 0 days 00h 03m.		\N
2550	2549	5	Top Transformed Buddha Summon	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Dark Orb, Python Longevity Serum, Monkey King Fur, Celestial Dye, Cursed Feather, Puppet Part	140,000 of 6,000,000 health.	Battle ends in 3 days 02h 59m.	Nature Transformation Chakra	\N
2101	2100	4	Gedo Mazo Dormant Summon	f	67	https://www.ninjamanager.com/img/cards/upload/20/small/gedo-mazo-dormant-summon.png	t	Jade Awl, Lotus Amulet, Magma, Salamander Venom, Curse Mark Pattern, Serpent Scales, Snakeskin, Wolf Pelt	3,340,000 of 5,040,000 health.	Battle ends in 0 days 00h 01m.		\N
1880	1879	4	Onoki Kage	f	32	https://www.ninjamanager.com/img/cards/upload/20/small/onoki-kage.png	t	Drench, Snake Serum, Arachne Web, Charm of Protection, Robot Parts, Lion Fur, Clay	1,900,000 of 5,950,000 health.	Battle ends in 3 days 17h 57m.		\N
2078	2077	4	Itachi Jounin	t	4	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Elemental Rune, Jade Dagger, Susanoo Residue, Steam Stone, Ironbark, Stem Cell	40,000 of 2,520,000 health.	Battle ends in 5 days 02h 32m.	Snake Eye	\N
2543	2542	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Adamantine Kunai, Susanoo Residue, Otsutsuki Cells, Talatat Stone	30,000 of 1,680,000 health.	Battle ends in 3 days 12h 15m.	Mind Awakening Pill, White Diamond	\N
2294	2293	5	Madara Rinne Sharingan Kage	f	75	https://www.ninjamanager.com/img/cards/upload/20/medium/madara-rinne-sharingan-kage.png	t	Inferno Artifact, Ghost Cape, Dharma Spirit, Bezoar, Hyuga Blood, Dice, Wood	53,260,000 of 71,500,000 health.	Battle ends in 0 days 00h 01m.		\N
1844	1843	4	Onoki Fn Kage	t	2	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Unholy Strike, Beast Soul, Robot Processor, Otsutsuki Cells, Granite, Iron, Rusty Spring	190,000 of 16,560,000 health.	Battle ends in 1 days 08h 54m.	Uchiha Crest, Elvenskin, Bezoar, Curse Mark Pattern, Jade, Ascension Ore, Essence Of Darkness	\N
1920	1919	4	Tsunade Fn Kage	f	56	https://www.ninjamanager.com/img/cards/upload/20/medium/tsunade-fn-kage.gif	t	Atlantic Artifact, Spider Silk, Dragon Blood, Blood Vessel, Snakeskin, Rusty Spring	11,190,000 of 20,182,500 health.	Battle ends in 0 days 00h 04m.		\N
549	548	1	Mother Puppet Summon	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Vajra, Runestone, Undying Mask, Zephyr Leaf, Quartz Crystal, Bezoar, Uchiha Crest, Panacea, Adamantine Bone, Ghost Spirit	40,000 of 1,760,000 health.	Battle ends in 8 days 14h 26m.		\N
1884	1883	4	Gedo Mazo Dormant Summon	f	71	https://www.ninjamanager.com/img/cards/upload/20/small/gedo-mazo-dormant-summon.png	t	Robot Processor, Jade Awl, Opal	590,000 of 840,000 health.	Battle ends in 0 days 04h 57m.		\N
1648	1647	3	Omoi Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Otsutsuki Cells, Azoth, Genjutsu Pill, Mind Awakening Pill, Serpent Horn	30,000 of 1,375,000 health.	Battle ends in 2 days 22h 49m.	Snake Eye, Mercury, Hyuga Blood, Dharma Wheel	\N
94	93	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
96	95	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
97	96	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
98	97	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1501	1500	3	Chukichi Jounin	f	71	https://www.ninjamanager.com/img/cards/upload/20/small/chukichi-jounin.png	t	Lotus Amulet, Aqua Stone, Zephyr Leaf, Panacea, Cursed Feather	1,480,000 of 2,100,000 health.	Battle ends in 1 days 18h 57m.		\N
1402	1401	3	Kushimaru Kage	f	71	https://www.ninjamanager.com/img/cards/upload/20/small/kushimaru-kage.png	t	Imperial Jade, Steam Stone, Ironbark, Stem Cell, Cracked Anbu Mask, Clear Water	3,780,000 of 5,355,000 health.	Battle ends in 0 days 00h 01m.		\N
1000	999	2	C Jounin	f	3	https://www.ninjamanager.com/img/cards/upload/20/small/c-jounin.png	t	Opal, Demon Wing, Quartz Crystal, Cursed Feather, Dark Chakra Eel, Puppet Part, Rusty Spring	60,000 of 2,475,000 health.	Battle ends in 0 days 00h 00m.	Blood Vessel	\N
2501	2500	5	Minato Kage	f	65	https://www.ninjamanager.com/img/cards/upload/20/small/minato-kage.png	t	Ice Brand, Spider Gland, Lotus Amulet, Ancient Blood, Rune Word, Jutsu Scroll	18,630,000 of 28,800,000 health.	Battle ends in 0 days 00h 03m.		\N
2169	2168	4	Yugito Jounin	f	85	https://www.ninjamanager.com/img/cards/upload/20/small/yugito-jounin.png	t	Monkey King Fur, Magma, Essence of Smoke, Uchiha Crest, Rune Word, Black Threads, Clay	3,540,000 of 4,200,000 health.	Battle ends in 6 days 12h 56m.		\N
1470	1469	3	Kinkaku Tailed Kage	t	100	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Mimirwater, Electrum Coin, Lotus Amulet, Celestial Dye, Mercury, Turtle Shell	18,110,000 of 18,200,000 health.	Battle ends in 12 days 21h 38m.		\N
2108	2107	4	Obito Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Venoxin Mutagen, Vajra, Aqua Stone, Celestial Dye, Roran Coin, Serpent Scales	70,000 of 2,520,000 health.	Battle ends in 2 days 20h 42m.		\N
809	808	1	Shukaku Summon	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Elvenskin, Charm of Protection, Azoth, Snake Eye, Berserker Enzyme, Sparkless Wind Shard, Ivory, Serpent Scales	40,000 of 1,760,000 health.	Battle ends in 5 days 12h 58m.		\N
2450	2449	5	Guy Crimson Jounin	t	4	https://www.ninjamanager.com/clans/warzones/god-tree	t	Magnetic Field, Babylonian Stone, Chimera Heart, Zephyr Leaf, Dharma Wheel, Granite, Crow Feather, Puppet Part	100,000 of 3,360,000 health.	Battle ends in 2 days 20h 45m.		\N
1198	1197	2	Menma Jounin	t	4	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Adamantine Erosion, Dragon Soul, Anbu Mask, Molten Stone, Celestial Dye, Snake Eye	80,000 of 2,520,000 health.	Battle ends in 2 days 18h 58m.	Quicklime, Mind Awakening Pill	\N
1375	1374	3	Chukichi Jounin	f	5	https://www.ninjamanager.com/img/cards/upload/20/small/chukichi-jounin.png	t	Arachne Web, Essence of Smoke, Muga Silk, Serpent Scales, Dice, Wood, Bone	110,000 of 2,200,000 health.	Battle ends in 0 days 00h 01m.		\N
2370	2369	5	Madara Rinne Sharingan Kage	t	100	https://www.ninjamanager.com/clans/warzones/god-tree	t	Infinite Tsukuyomi, Inferno Artifact, Chimera Heart, Bezoar, Jade, Cracked Anbu Mask, Clay	35,450,000 of 35,750,000 health.	Battle ends in 10 days 23h 07m.		\N
2093	2092	4	Itachi Jounin	f	58	https://www.ninjamanager.com/img/cards/upload/20/small/itachi-jounin.png	t	Memory Recall, Babylonian Stone, Chimera Gauntlets, Runestone, Zephyr Leaf, Panacea	1,450,000 of 2,520,000 health.	Battle ends in 0 days 00h 02m.		\N
2349	2348	5	Guy Crimson Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Snake Serum, Anbu Mask, Opal, Nature Transformation Chakra, Panacea, Cursed Feather	50,000 of 2,520,000 health.	Battle ends in 1 days 13h 09m.		\N
553	552	1	Satori Summon	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Infectious Strike, Otsutsuki Cells, Ghost Gloves, Quartz Crystal, Charm of Protection, Hyuga Blood, Roran Coin, Granite	30,000 of 1,280,000 health.	Battle ends in 4 days 16h 02m.		\N
2551	2550	5	Obito Jinchuriki Unstable Kage	f	65	https://www.ninjamanager.com/img/cards/upload/20/small/obito-jinchuriki-unstable-kage.png	t	Kraken Camouflage, Dark Orb, Snake Serum, Steam Stone, Essence of Swiftness, Mind Awakening Pill, Lion Fur, Wood	6,050,000 of 9,371,250 health.	Battle ends in 0 days 00h 04m.		\N
2402	2401	5	Kakashi Sharingan Jounin	t	4	https://www.ninjamanager.com/clans/warzones/god-tree	t	Ancient Coin, Arachne Web, Fire Shard, White Diamond, Curse Mark Pattern	80,000 of 2,100,000 health.	Battle ends in 2 days 11h 45m.	Iron Sand, Robot Parts, Adamantine Bone, Granite	\N
2036	2035	4	Roshi Jounin	f	20	https://www.ninjamanager.com/img/cards/upload/20/small/roshi-jounin.png	t	Heart Container, Magma, Salamander Venom, Tanto, Roran Coin, Lava, Black Threads, Puppet Part	1,760,000 of 8,820,000 health.	Battle ends in 0 days 00h 00m.		\N
2322	2321	5	Hashirama Kage	f	80	https://www.ninjamanager.com/img/cards/upload/20/small/hashirama-kage.png	t	Kraken Camouflage, Warg Talisman, Wolf Eye, Dragon Soul, Talatat Stone, Serpent Scales, Wolf Pelt	11,140,000 of 14,000,000 health.	Battle ends in 0 days 00h 02m.		\N
2100	2099	4	Obito Mask Kage	t	6	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Reroll Counter, Aqua Stone, Ancient Blood, Stem Cell	50,000 of 1,100,000 health.	Battle ends in 0 days 03h 20m.		\N
1952	1951	4	Roshi Jounin	f	100	https://www.ninjamanager.com/img/cards/upload/20/small/roshi-jounin.png	t	Magnetic Field, Robot Processor, Prototype Chakra Armor, Ceremonial Mask, Charm of Protection, Iron Sand	2,520,000 of 2,520,000 health.	Battle ends in 3 days 15h 56m.		0.0
2330	2329	5	Obito Jinchuriki Unstable Kage	t	8	https://www.ninjamanager.com/clans/warzones/god-tree	t	Warg Talisman, Snake Serum, Ceremonial Mask, Demon Wing, Quicklime, Dice	250,000 of 3,570,000 health.	Battle ends in 1 days 19h 38m.		\N
2051	2050	4	Han Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Chimera DNA, Aqua Stone, Ascension Ore, Roran Coin, Serpent Scales, Black Threads, Spider Web	180,000 of 8,820,000 health.	Battle ends in 6 days 14h 39m.	Uchiha Blood, Azoth, Quicklime, Essence Of Smoke, Essence Of Darkness	\N
1503	1502	3	Pakura Jounin	t	4	https://www.ninjamanager.com/clans/warzones/coast-of-lightning	t	Mind Recovery, Serpent Pike, Ancient Coin, Dharma Spirit, Steel, Hyuga Blood	70,000 of 2,520,000 health.	Battle ends in 4 days 19h 08m.	Poison Vial	\N
1978	1977	4	Obito Mask Kage	f	79	https://www.ninjamanager.com/img/cards/upload/20/small/obito-mask-kage.png	t	Electrum Coin, Snake Serum, Magma, Ice Shards, Granite, Ink Bottle, Fish Scales	5,580,000 of 7,140,000 health.	Battle ends in 1 days 21h 56m.		\N
2669	2668	5	Guy Fn Kage	f	79	https://www.ninjamanager.com/img/cards/upload/20/medium/guy-fn-kage.gif	t	Demonic Flute, Blade Control, Gate of Wonder, Czarwood, Holy Water, Quartz Crystal, Artificial Chakra Core, Antique Mask, Leather	146,930,000 of 187,200,000 health.	Battle ends in 0 days 00h 02m.	Magma, Ceremonial Mask, Nature Transformation Chakra, Demon Wing, Steel, Opal, Elvenskin	\N
2158	2157	4	Obito Mask Kage	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Yggdrasil Sap, Runic Talisman, Dharma Wheel, Uchiha Crest, Lion Fur	30,000 of 1,375,000 health.	Battle ends in 1 days 19h 26m.		\N
2548	2547	5	Hashirama Kage	f	77	https://www.ninjamanager.com/img/cards/upload/20/small/hashirama-kage.png	t	Chakra Exosuit, Inferno Artifact, Dragon Bone, Ceremonial Mask, Granite, Iron, Fish Scales	19,130,000 of 24,840,000 health.	Battle ends in 0 days 00h 05m.		\N
2400	2399	5	Top Transformed Buddha Summon	f	86	https://www.ninjamanager.com/img/cards/upload/20/small/top-transformed-buddha-summon.png	t	Golden Gravity, Robot Processor, Dragon Blood, Essence of Darkness, Roran Coin, Snakeskin, Clay	5,060,000 of 5,950,000 health.	Battle ends in 0 days 00h 02m.		\N
1954	1953	4	Han Jounin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Guiding Wings, Dragon Soul, Magma, Ice Shards, Uchiha Blood, Lava, Iron, Red Sand	90,000 of 4,200,000 health.	Battle ends in 7 days 13h 39m.		\N
56	55	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
57	56	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
58	57	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
59	58	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
60	59	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
61	60	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
62	61	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
63	62	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1824	1823	4	Crow Genjutsu Summon	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Imperial Jade, Holy Water, Charm of Protection, Roran Coin, Adamantine Bone, Dice, Puppet Part	80,000 of 3,360,000 health.	Battle ends in 4 days 15h 18m.		\N
2558	2557	5	Obito Jinchuriki Unstable Kage	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Osteoblast, Steam Stone, Salamander Venom, Iron Sand, Antique Mask, Bone	140,000 of 7,735,000 health.	Battle ends in 1 days 23h 20m.		\N
2274	2273	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Monkey King Fur, Tortoise Mask, Magma, Charm of Protection, Uchiha Crest, Cursed Feather	40,000 of 2,520,000 health.	Battle ends in 0 days 01h 37m.		\N
2428	2427	5	Kakashi Sharingan Jounin	t	3	https://www.ninjamanager.com/clans/warzones/god-tree	t	Reroll Counter, Dragon Bone, Thunder Stone, Talatat Stone, Genjutsu Pill, Quicklime	70,000 of 2,520,000 health.	Battle ends in 2 days 13h 05m.		\N
1872	1871	4	Neji Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Elemental Rune, Venoxin Mutagen, Soil Stone, Essence of Darkness, Artificial Chakra Core, Ivory, Dice, Clear Water	180,000 of 8,820,000 health.	Battle ends in 12 days 01h 41m.		\N
1910	1909	4	Gengetsu Kage	f	16	https://www.ninjamanager.com/img/cards/upload/20/small/gengetsu-kage.png	t	Yggdrasil Bark, Ascension Ore, Artificial Chakra Core, Lava, Ink Bottle	220,000 of 1,375,000 health.	Battle ends in 0 days 00h 01m.		\N
214	213	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
215	214	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
216	215	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
1151	1150	2	Temari Evolved Genin	t	3	https://www.ninjamanager.com/clans/warzones/land-of-iron	t	Soil Stone, Ceremonial Mask, Uchiha Crest, Hyuga Blood, Lava, Leather, Bone	60,000 of 2,475,000 health.	Battle ends in 2 days 01h 43m.	Bezoar, Poison Vial, Serpent Scales, Black Receiver, Mercury	\N
2401	2400	5	Hashirama Kage	f	57	https://www.ninjamanager.com/img/cards/upload/20/small/hashirama-kage.png	t	Spider Silk, Soil Stone, Azoth, Serpent Horn, Bronze Coin, Fish Scales	4,350,000 of 7,735,000 health.	Battle ends in 4 days 18h 56m.		\N
1903	1902	4	Nagato Kage	f	11	https://www.ninjamanager.com/img/cards/upload/20/small/nagato-kage.png	t	Dragon Soul, Snakeskin Suit, Aqua Stone, Charm of Protection, Mind Awakening Pill, Ghost Spirit, Lion Fur, Clay	940,000 of 8,820,000 health.	Battle ends in 14 days 04h 56m.		\N
2253	2252	5	Madara Rinne Sharingan Kage	f	77	https://www.ninjamanager.com/img/cards/upload/20/medium/madara-rinne-sharingan-kage.png	t	Blade Control, Eye Scope, Altuna Rune, Dojutsu Catalyst, Ascension Ore, Quicklime, Dice, Clear Water	67,070,000 of 87,360,000 health.	Battle ends in 0 days 00h 03m.	Curse Mark Pattern, Nature Transformation Chakra	\N
268	267	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
269	268	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
270	269	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
271	270	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
944	943	2	Naruto Wind Jounin	f	30	https://www.ninjamanager.com/img/cards/upload/20/medium/naruto-wind-jounin.gif	t	Firestorm Staff, Spider Silk, Ceremonial Mask, Salamander Venom, Iron Sand, Crow Feather, Wood	2,420,000 of 8,330,000 health.	Battle ends in 0 days 00h 00m.	Hyuga Blood, Celestial Dye, Essence Of Darkness	\N
2609	2608	5	Guy 8Gates Kage	f	93	https://www.ninjamanager.com/img/cards/upload/20/small/guy-8gates-kage.png	t	Beast Soul, Chimera DNA, Yggdrasil Bark, Adamantine Bone, Snakeskin, Rusty Spring	9,600,000 of 10,350,000 health.	Battle ends in 0 days 00h 03m.		\N
64	63	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
65	64	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
66	65	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
67	66	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
68	67	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
69	68	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
70	69	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
73	72	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
74	73	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
75	74	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
76	75	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
77	76	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
78	77	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
79	78	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
81	80	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
82	81	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
83	82	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
85	84	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
86	85	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
87	86	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
88	87	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
41	40	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
42	41	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
43	42	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
45	44	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
46	45	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
339	338	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
340	339	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
341	340	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
342	341	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
118	117	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
119	118	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
121	120	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
122	121	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
123	122	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
124	123	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
125	124	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
126	125	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
127	126	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
128	127	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
129	128	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
130	129	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
131	130	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
132	131	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
133	132	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
134	133	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
162	161	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
101	100	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
102	101	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
104	103	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
105	104	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
106	105	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
107	106	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
108	107	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
109	108	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
111	110	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
112	111	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
113	112	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
114	113	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
115	114	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
116	115	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
117	116	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
174	173	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
175	174	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
176	175	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
177	176	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
179	178	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
180	179	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
181	180	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
182	181	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
183	182	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
184	183	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
185	184	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
186	185	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
187	186	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
188	187	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
189	188	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
190	189	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
191	190	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
192	191	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
194	193	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
195	194	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
196	195	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
197	196	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
198	197	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
199	198	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
201	200	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
225	224	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
226	225	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
227	226	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
228	227	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
229	228	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
230	229	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
231	230	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
232	231	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
233	232	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
234	233	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
235	234	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
237	236	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
238	237	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
239	238	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
240	239	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
241	240	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
242	241	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
243	242	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
244	243	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
245	244	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
246	245	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
247	246	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
248	247	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
249	248	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
250	249	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
253	252	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
254	253	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
255	254	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
256	255	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
257	256	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
258	257	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
272	271	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
273	272	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
274	273	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
275	274	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
276	275	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
277	276	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
279	278	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
280	279	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
281	280	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
282	281	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
283	282	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
284	283	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
285	284	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
286	285	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
287	286	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
288	287	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
289	288	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
290	289	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
291	290	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
292	291	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
294	293	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
295	294	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
296	295	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
297	296	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
302	301	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
303	302	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
304	303	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
305	304	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
306	305	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
307	306	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
309	308	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
310	309	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
311	310	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
312	311	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
313	312	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
314	313	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
315	314	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
316	315	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
317	316	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
318	317	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
320	319	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
321	320	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
322	321	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
323	322	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
324	323	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
325	324	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
326	325	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
327	326	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
328	327	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
335	334	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
336	335	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
337	336	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
338	337	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
343	342	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
344	343	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
345	344	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
346	345	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
347	346	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
348	347	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
349	348	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
350	349	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
352	351	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
353	352	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
354	353	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
355	354	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
356	355	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
357	356	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
360	359	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
361	360	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
363	362	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
364	363	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
365	364	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
366	365	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
367	366	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
368	367	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
89	88	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
370	369	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
371	370	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
372	371	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
374	373	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
375	374	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
376	375	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
377	376	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
378	377	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
379	378	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
380	379	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
396	395	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
397	396	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
398	397	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
399	398	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
400	399	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
401	400	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
402	401	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
403	402	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
404	403	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
405	404	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
406	405	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
407	406	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
408	407	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
409	408	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
410	409	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
411	410	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
412	411	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
413	412	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
414	413	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
415	414	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
416	415	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
417	416	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
418	417	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
420	419	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
421	420	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
422	421	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
423	422	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
424	423	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
425	424	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
426	425	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
427	426	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
428	427	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
429	428	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
430	429	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
431	430	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
432	431	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
433	432	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
434	433	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
435	434	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
436	435	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
437	436	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
439	438	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
440	439	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
441	440	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
442	441	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
443	442	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
444	443	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
445	444	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
446	445	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
447	446	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
448	447	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
449	448	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
450	449	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
373	372	0	Naruto Chunin Genin	t	5	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Runic Talisman, Quicklime, Uchiha Blood, Anbu Paint, Ink Bottle	0 of 100,000 health.	Battle ends in 4 days 11h 10m.		\N
419	418	0	Itachi Kage	f	48	https://www.ninjamanager.com/img/cards/upload/20/medium/itachi-kage.png	t	Bionic Shinobi Gauntlet, Muscle Hardening, Lotus Amulet, Chimera Heart, Steel	1,500,000 of 3,150,000 health.	Battle ends in 0 days 00h 03m.		0.0
358	357	0	Orochimaru Sound Jounin	t	29	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Essence of Smoke$50, Stem Cell$25, Serpent Horn$17, Rune Word$13, Anbu Paint$10, Crow Feather$9, Chakra Plate$5	60,000 of 220,000 health.	Battle ends in 10 days 14h 03m.		2.05
330	329	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
331	330	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
332	331	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
333	332	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
47	46	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
48	47	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
50	49	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
51	50	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
53	52	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
54	53	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
55	54	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
252	251	0	Anko Fn Genin	t	46	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Ice Shards, Roran Coin, Ghost Spirit, Cursed Feather, Ink Bottle, Bronze Coin, Wolf Pelt	60,000 of 140,000 health.	Battle ends in 6 days 17h 26m.		\N
299	298	0	Rashomon Summon	t	4	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Elvenskin, Ironbark, Essence of Swiftness, Hyuga Blood, Mind Awakening Pill	10,000 of 375,000 health.	Battle ends in 2 days 15h 40m.		\N
44	43	0	Sasuke Fn Jounin	t	2	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Adamantine Erosion, Dragon Bone, Otsutsuki Cells, Celestial Dye, Quicklime, Cursed Feather, Bronze Coin, Wood	80,000 of 7,560,000 health.	Battle ends in 7 days 04h 10m.		\N
150	149	0	Manda Ii Summon	f	3	https://www.ninjamanager.com/img/cards/upload/20/small/manda-ii-summon.png	t	Charm of Protection$50, Essence of Swiftness$50, Quartz Crystal$50, Snake Eye$25, Iron Sand$25, Serpent Horn$17, Tainted Chakra$13, Spoiled Soldier Pill$8	30,000 of 1,440,000 health.	Battle ends in 1 days 09h 03m.		0.4
24	23	0	Ino Chunin Genin	t	5	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Runic Talisman$75, Ascension Ore$50, Essence of Darkness$50, Fuma Shuriken$49, Quicklime$25, Snake Eye$25, Dirty Dice$20, Black Receiver$17, Ivory$13	100,000 of 2,400,000 health.	Battle ends in 11 days 08h 04m.		1.59
99	98	0	Choji Chunin Genin	t	15	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Blight Rod, Dharma Wheel, Curse Mark Pattern, Roran Coin, Mercury	10,000 of 100,000 health.	Battle ends in 4 days 22h 09m.		\N
152	151	0	Sakura Chunin Genin	f	30	https://www.ninjamanager.com/img/cards/upload/20/small/sakura-chunin-genin.png	t	Magma$100, Ruby$50, Essence of Darkness$50, Quicklime$25, Berserker Enzyme$21, Prototype Chakra Gauntlet$20, Blood Vessel$17, Ivory$13	370,000 of 1,280,000 health.	Battle ends in 5 days 12h 29m.		3.57
49	48	0	Kiba Chunin Genin	f	89	https://www.ninjamanager.com/img/cards/upload/20/small/kiba-chunin-genin.png	t	Thunder Discharge$200, Anbu Vest$75, Biju Silk$75, Demon Wing$50, Refined Chakra Plate$42, Quicklime$25, Uchiha Crest$25, Berserker Enzyme$21, Ink Bottle$10	2,820,000 of 3,200,000 health.	Battle ends in 19 days 01h 29m.		2.67
25	24	0	Orochimaru Sound Jounin	t	3	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Iron Sand, Uchiha Crest, Lava, Snakeskin, Turtle Shell, Wolf Pelt, Clear Water	0 of 140,000 health.	Battle ends in 6 days 02h 21m.		\N
103	102	0	Meizu Evolved Genin	f	23	https://www.ninjamanager.com/img/cards/upload/20/small/meizu-evolved-genin.png	t	White Diamond$50, Essence of Darkness$50, Artificial Chakra Core$25	50,000 of 225,000 health.	Battle ends in 1 days 21h 29m.		5.42
100	99	0	Mubi Genin	t	3	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Ceremonial Mask, Yggdrasil Bark, Uchiha Blood, Serpent Horn, Snakeskin, Red Sand, Rusty Spring	50,000 of 2,200,000 health.	Battle ends in 1 days 11h 20m.	Berserker Enzyme, Tainted Chakra	\N
217	216	0	Orochimaru Snake Kage	f	34	https://www.ninjamanager.com/img/cards/upload/20/medium/orochimaru-snake-kage.png	t	Green Jumpsuit, Dragon Soul, Thunder Stone, Ruby, Panacea, Ghost Spirit, Black Threads, Chakra Plate	2,130,000 of 6,300,000 health.	Battle ends in 0 days 00h 00m.		0.0
3	2	0	Itachi Kage	f	70	https://www.ninjamanager.com/img/cards/upload/20/medium/itachi-kage.png	t	Winged Mask, Altuna Rune, Heart Container, Otsutsuki Cells, Robot Parts, Iron, Bone	58,360,000 of 84,525,000 health.	Battle ends in 0 days 00h 04m.	Ice Shards	0.0
154	153	0	Gaara 1T Jounin	t	12	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Steam Stone, Demon Wing, Nature Transformation Chakra, Jade, Curse Mark Pattern, Rune Word, Mercury, Anbu Paint	100,000 of 825,000 health.	Battle ends in 9 days 14h 54m.		\N
95	94	0	Neji Chunin Genin	t	5	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Charm of Protection, White Diamond, Zephyr Leaf	10,000 of 320,000 health.	Battle ends in 1 days 00h 51m.		\N
385	384	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
386	385	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
387	386	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
388	387	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
389	388	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
390	389	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
391	390	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
392	391	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
393	392	0	\N	\N	\N	\N	f	\N	\N	\N	\N	\N
301	300	0	Rock Lee Chunin Genin	f	7	https://www.ninjamanager.com/img/cards/upload/20/small/rock-lee-chunin-genin.png	t	Poison Coating$200, Celestial Dye$75, Ice Shards$50, Muga Silk$50, Soldier Pill$34, Hyuga Blood$25, Iron Sand$25, Ghost Spirit$13, Anbu Paint$10	220,000 of 3,200,000 health.	Battle ends in 13 days 12h 03m.		1.19
110	109	0	Rock Lee Chunin Genin	f	6	https://www.ninjamanager.com/img/cards/upload/20/small/rock-lee-chunin-genin.png	t	Self-destruction Laser$200, Elvenskin$100, Ghost Gloves$95, Ice Shards$50, Nature Transformation Chakra$50, Hyuga Blood$25, Black Receiver$17, Serpent Horn$17, Robot Parts$17	130,000 of 2,400,000 health.	Battle ends in 12 days 08h 32m.		2.1
293	292	0	Sasuke Chunin Genin	t	85	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Elvenskin$100, Ice Shards$50, Quartz Crystal$50, Curse Mark Pattern$25, Iron Sand$25	680,000 of 800,000 health.	Battle ends in 4 days 10h 32m.		0.87
236	235	0	Sasuke Chunin Genin	t	20	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Snake Stamina$200, White Diamond$50, Azoth$50, Refined Chakra Plate$42, Flak Jacket$25	160,000 of 800,000 health.	Battle ends in 1 days 23h 03m.		1.72
151	150	0	Sasuke Chunin Genin	f	6	https://www.ninjamanager.com/img/cards/upload/20/small/sasuke-chunin-genin.png	t	Poison Wound$200, Beast Sealing Chains$150, Biju Meat$75, Ruby$50, Charm of Protection$50	40,000 of 800,000 health.	Battle ends in 3 days 16h 29m.		10.73
298	297	0	Tayuya Jounin	t	91	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Wolf Mantle$125, Zephyr Leaf$50, Refined Chakra Plate$42, Hyuga Blood$25, Granite$17, Robot Parts$17, Black Receiver$17, Crow Feather$9	1,020,000 of 1,125,000 health.	Battle ends in 14 days 00h 32m.		2.58
750	749	1	Fuu Jinchuriki Genin	t	3	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Self-destruction Laser, Aqua Stone, Ghost Gloves, Bezoar, Quartz Crystal	20,000 of 800,000 health.	Battle ends in 0 days 18h 35m.	Anbu Paint	0.0
278	277	0	Tayuya Jounin	f	27	https://www.ninjamanager.com/img/cards/upload/20/small/tayuya-jounin.png	t	Poison Coating$200, Poison Puppet$113, Ice Shards$50	40,000 of 150,000 health.	Battle ends in 1 days 05h 29m.		42.07
1259	1258	2	Chimera Summon	f	8	https://www.ninjamanager.com/img/cards/upload/20/small/chimera-summon.png	t	Serpent Armor$225, Soil Stone$125, Ironbark$63, Jade$25, Berserker Enzyme$21, Serpent Scales$13, Wolf Pelt$5, Bone$3	230,000 of 3,025,000 health.	Battle ends in 4 days 17h 16m.		2.24
701	700	1	Ino Evolved Genin	f	6	https://www.ninjamanager.com/img/cards/upload/20/small/ino-evolved-genin.png	t	Molten Stone$125, Steel$63, Hero Water$53, White Diamond$50, Essence of Smoke$50, Uchiha Blood$25, Uchiha Crest$25, Prototype Chakra Gauntlet$20, Robot Parts$17, Antique Mask$10	180,000 of 3,200,000 health.	Battle ends in 9 days 03h 11m.		1.55
200	199	0	Shino Chunin Genin	f	4	https://www.ninjamanager.com/img/cards/upload/20/small/shino-chunin-genin.png	t	Elemental Puppet$100, Yggdrasil Bark$100, Biju Flame$75, Ice Shards$50, Berserker Enzyme$21, Robot Parts$17, Adamantine Bone$17, Rune Word$13	50,000 of 1,440,000 health.	Battle ends in 5 days 12h 29m.		2.14
2098	2097	4	Obito Cracked Jounin	t	\N	https://www.ninjamanager.com/clans/warzones/great-war-plains	f	\N	\N	\N	\N	\N
319	318	0	Shikamaru Chunin Genin	f	46	https://www.ninjamanager.com/img/cards/upload/20/small/shikamaru-chunin-genin.png	t	Ghost Gloves$95, Iron Sand$25, Granite$17, Serpent Horn$17, Ivory$13	50,000 of 100,000 health.	Battle ends in 4 days 18h 29m.		17.22
193	192	0	Neji Chunin Genin	t	90	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Chimera Gauntlets$206, Snake Bite$200, Runestone$125, Steel$63, Essence of Darkness$50	720,000 of 800,000 health.	Battle ends in 4 days 19h 32m.		11.64
351	350	0	Giant Spider Summon	t	41	https://www.ninjamanager.com/clans/warzones/forest-of-death	t	Snake Eye$25, Ivory$13, Jinchuriki Seal$13, Snakeskin$7, Lion Fur$7, Red Sand$5, Clay$4	120,000 of 300,000 health.	Battle ends in 14 days 16h 32m.		1.07
650	649	1	Shino Evolved Genin	f	30	https://www.ninjamanager.com/img/cards/upload/20/small/shino-evolved-genin.png	t	Green Jumpsuit$275, Biju Meat$75, Celestial Dye$75, Prototype Chakra Helmet$50, Muga Silk$50, Stem Cell$25, Berserker Enzyme$21, Serpent Horn$17, Lava$13	520,000 of 1,760,000 health.	Battle ends in 9 days 07h 28m.		0.56
952	951	2	Chojuro Evolved Genin	f	50	https://www.ninjamanager.com/img/cards/upload/20/small/chojuro-evolved-genin.png	t	Mind Recovery$400, Imperial Jade$250, Magma$100, Demon Wing$50, Wooden Talisman$25, Tainted Chakra$13, Lion Fur$7, Clay$4	4,490,000 of 8,820,000 health.	Battle ends in 15 days 00h 17m.		1.29
72	71	0	Aoda Summon	f	83	https://www.ninjamanager.com/img/cards/upload/20/small/aoda-summon.png	t	Yggdrasil Sap$150, Essence of Swiftness$50, Tanto$38, Uchiha Blood$25, Hyuga Blood$25, Cursed Feather$13, Ice Horn$5, Fish Scales$3	2,500,000 of 3,025,000 health.	Battle ends in 10 days 09h 11m.		1.62
359	358	0	Rock Lee Chunin Genin	f	5	https://www.ninjamanager.com/img/cards/upload/20/small/rock-lee-chunin-genin.png	t	Last Luck$200, Ghost Gloves$95, Jinchuriki Chakra$50, Nature Transformation Chakra$50, Ice Shards$50, Hyuga Blood$25, Mind Awakening Pill$25, Rune Word$13, Ghost Spirit$13	110,000 of 2,400,000 health.	Battle ends in 6 days 00h 11m.		4.46
395	394	0	Sakura Chunin Genin	f	39	https://www.ninjamanager.com/img/cards/upload/20/small/sakura-chunin-genin.png	t	Mind Recovery$400, Adamantine Kunai$244, Aero Stone$125, Azoth$50, Uchiha Blood$25, Serpent Scales$13, Lion Fur$7, Spider Web$5, Clay$4	1,600,000 of 4,125,000 health.	Battle ends in 3 days 16h 11m.		4.6
80	79	0	Anko Fn Genin	f	12	https://www.ninjamanager.com/img/cards/upload/20/small/anko-fn-genin.png	t	Snakeskin Slippers$75, Nature Transformation Chakra$50, Azoth$50, Muga Silk$50, Jade$25	90,000 of 800,000 health.	Battle ends in 3 days 02h 29m.		2.74
1628	1627	3	Ameyuri Kage	f	67	https://www.ninjamanager.com/img/cards/upload/20/small/ameyuri-kage.png	t	Steam Armor$900, Snake Serum$250, Elvenskin$100, Ironbark$63, Berserker Enzyme$21, Dice$9, Rusty Spring$3	8,360,000 of 12,495,000 health.	Battle ends in 4 days 00h 05m.		2.28
971	970	2	Phantom Dragon Summon	f	92	https://www.ninjamanager.com/img/cards/upload/20/small/phantom-dragon-summon.png	t	Robot Processor$250, Anbu Mask$133, Soil Stone$125, Ironbark$63, Mind Awakening Pill$25, Jinchuriki Seal$13, Bronze Coin$7, Leather$4	8,090,000 of 8,820,000 health.	Battle ends in 19 days 00h 35m.		0.93
1709	1708	3	Kinkaku Jounin	f	67	https://www.ninjamanager.com/img/cards/upload/20/small/kinkaku-jounin.png	t	Guiding Wings$400, Lotus Amulet$200, Ghost Orb$125, Quartz Crystal$50, Curse Mark Pattern$25, Adamantine Bone$17, Black Threads$9, Clear Water$4	4,500,000 of 6,720,000 health.	Battle ends in 11 days 10h 08m.		1.12
1251	1250	2	Zetsu Jounin	f	19	https://www.ninjamanager.com/img/cards/upload/20/small/zetsu-jounin.png	t	Thunder Stone$125, Ironbark$63, Mind Awakening Pill$25, Serpent Horn$17, Bronze Coin$7	250,000 of 1,375,000 health.	Battle ends in 0 days 14h 16m.		2.44
1053	1052	2	Chimera Summon	f	92	https://www.ninjamanager.com/img/cards/upload/20/small/chimera-summon.png	t	Adamantine Kunai$244, Arachne Web$125, Nature Transformation Chakra$50, Jade$25	1,000,000 of 1,100,000 health.	Battle ends in 1 days 09h 49m.		6.97
1201	1200	2	A Kage	f	100	https://www.ninjamanager.com/img/cards/upload/20/small/a-kage.png	t	Monkey King Fur$200, Thunder Stone$125, Essence of Smoke$50, Genjutsu Pill$45, Curse Mark Pattern$25, Tainted Chakra$13	2,520,000 of 2,520,000 health.	Battle ends in 5 days 21h 49m.		\N
1295	1294	2	Akatsuchi Evolved Jounin	f	93	https://www.ninjamanager.com/img/cards/upload/20/small/akatsuchi-evolved-jounin.png	t	Bionic Shinobi Gauntlet$350, Monkey King Fur$200, Salamander Venom$63, Quartz Crystal$50, Berserker Enzyme$21, Jinchuriki Seal$13, Puppet Part$5	1,790,000 of 1,925,000 health.	Battle ends in 1 days 15h 49m.		1.63
980	979	2	Samui Evolved Genin	f	63	https://www.ninjamanager.com/img/cards/upload/20/small/samui-evolved-genin.png	t	Spider Silk$250, Otsutsuki Cells$100, Essence of Smoke$50, Quicklime$25, Ink Bottle$10, Fish Scales$3	11,280,000 of 16,362,500 health.	Battle ends in 18 days 19h 49m.		5.28
1117	1116	2	Zetsu Kage	f	59	https://www.ninjamanager.com/img/cards/upload/20/medium/zetsu-kage.png	t	Fiery Mane$800, Warg Talisman$560, Ancient Coin$250, Steam Stone$125, Azoth$50, Roran Coin$25, Snakeskin$7, Wood$4	6,310,000 of 10,710,000 health.	Battle ends in 2 days 03h 49m.		\N
845	844	1	Ryuzetsu Jounin	t	14	https://www.ninjamanager.com/clans/warzones/demon-desert	t	Snake Stamina$200, Earth Shard$75, Steel$63, Muga Silk$50, Ascension Ore$50, Artificial Chakra Core$25, Stem Cell$25, Serpent Horn$17, Blood Vessel$17, Sparkless Earth Shard$15	230,000 of 1,760,000 health.	Battle ends in 9 days 04h 11m.		2.24
601	600	1	Fuu Jounin	f	29	https://www.ninjamanager.com/img/cards/upload/20/small/fuu-jounin.png	t	Weapons Quiver$94, Undying Mask$93, White Diamond$50, Ruby$50, Ice Shards$50, Quicklime$25, Curse Mark Pattern$25, Tainted Chakra$13, Ink Bottle$10	690,000 of 2,400,000 health.	Battle ends in 9 days 12h 28m.		1.5
1262	1261	2	Samui Evolved Genin	f	52	https://www.ninjamanager.com/img/cards/upload/20/small/samui-evolved-genin.png	t	Mind Recovery$400, Serpent Soul$167, Dragon Blood$100, Charm of Protection$50, Fuma Shuriken$49, Black Receiver$17, Rune Word$13, Wood$4, Rusty Spring$3	2,850,000 of 5,500,000 health.	Battle ends in 14 days 05h 16m.	Tainted Chakra, Mercury, Cursed Feather, Dharma Wheel, Uchiha Crest	5.26
1938	1937	4	Gedo Mazo Dormant Summon	t	3	https://www.ninjamanager.com/clans/warzones/great-war-plains	t	Artificial Rasengan, Heart Container, Soil Stone, Essence of Darkness, Roran Coin, Lava	60,000 of 2,520,000 health.	Battle ends in 3 days 06h 33m.		\N
1269	1268	2	Haku Chunin Genin	f	83	https://www.ninjamanager.com/img/cards/upload/20/small/haku-chunin-genin.png	t	Mind Recovery$400, Vajra$200, Holy Water$125, Biju Meat$75, Bezoar$50, Ivory$13, Ink Bottle$10, Jutsu Scroll$5, Rusty Spring$3	2,040,000 of 2,475,000 health.	Battle ends in 5 days 01h 16m.		3.39
1669	1668	3	Gari Jounin	f	97	https://www.ninjamanager.com/img/cards/upload/20/small/gari-jounin.png	t	Spider Silk$250, Soil Stone$125, Quartz Crystal$50, Genjutsu Pill$45, Mind Awakening Pill$25, Black Receiver$17	2,440,000 of 2,520,000 health.	Battle ends in 2 days 20h 08m.		3.59
1769	1768	3	Ameyuri Edo Jounin	f	99	https://www.ninjamanager.com/img/cards/upload/20/medium/ameyuri-edo-jounin.gif	t	Fiery Mane$800, Bone Pendant$550, Spider Gland$500, Venoxin Mutagen$210, Biju Coral$75, Mercury$13, Puppet Part$5	25,330,000 of 25,600,000 health.	Battle ends in 13 days 11h 08m.		1.6
362	361	0	Hinata Chunin Genin	f	62	https://www.ninjamanager.com/img/cards/upload/20/small/hinata-chunin-genin.png	t	Adamantine Erosion$400, Molten Stone$125, Elvenskin$100, Ruby$50, Hyuga Blood$25, Serpent Scales$13, Jutsu Scroll$5, Clay$4	2,530,000 of 4,125,000 health.	Battle ends in 11 days 01h 28m.	Curse Mark Pattern	3.44
438	437	0	Kabuto Jounin	f	7	https://www.ninjamanager.com/img/cards/upload/20/small/kabuto-jounin.png	t	Infectious Strike$200, Ironbark$63, Twisted Mask$54, Ruby$50, Roran Coin$25, Stem Cell$25, Poison Vial$17, Serpent Scales$13, Dice$9	70,000 of 1,125,000 health.	Battle ends in 11 days 03h 28m.		2.66
812	811	1	Neji Evolved Genin	f	15	https://www.ninjamanager.com/img/cards/upload/20/small/neji-evolved-genin.png	t	Poison Shuriken$175, Talatat Stone$75, Bezoar$50, Essence of Smoke$50, Uchiha Blood$25, Snake Eye$25	140,000 of 960,000 health.	Battle ends in 3 days 03h 28m.		4.55
2334	2333	5	Top Transformed Buddha Summon	f	95	https://www.ninjamanager.com/img/cards/upload/20/small/top-transformed-buddha-summon.png	t	Regeneration$800, Firestorm Staff$750, Damascus Steel$500, Lotus Amulet$200, Ancient Blood$84, Serpent Scales$13, Ice Horn$5	10,540,000 of 11,200,000 health.	Battle ends in 1 days 07h 57m.		5.34
\.


--
-- Data for Name: warzone_damage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.warzone_damage (player_id, warzone, damage, clan) FROM stdin;
244418372257841153	5	9493	GG
144912112060203008	4	7500	GG
144912112060203008	3	3750	HG
211282750135730176	6	14000	GG
288712595312082944	4	4500	HG
206588796198715392	2	10000	HG
171033233050304512	5	12500	GG
179802545705713674	4	13000	GG
425718321124737045	4	3000	GG
219240160649281536	1	100	GG
466654204019212290	6	13500	HG
197160269678379010	5	8500	GG
344717503441928203	2	7500	GG
145722488079253505	4	\N	GG
190516666998718464	3	4000	HG
275799181774422036	4	6500	HG
190516666998718464	3	3000	GG
211282750135730176	2	1500	HG
466654204019212290	6	17000	GG
319318987563466752	5	12000	GG
344717503441928203	5	9000	HG
288712595312082944	6	11000	GG
311279314098192385	6	20000	GG
304964079896756226	6	12000	GG
155090097799299083	4	8200	GG
449138085733728286	6	9300	GG
155090097799299083	6	10500	HG
304964079896756226	2	3000	HG
219240160649281536	3	2500	HG
\.


--
-- Name: item_location_sampleid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.item_location_sampleid_seq', 983, true);


--
-- Name: items_sampleid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.items_sampleid_seq', 66, true);


--
-- Name: material_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.material_id_seq', 757, true);


--
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.player_id_seq', 35, true);


--
-- Name: queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.queue_id_seq', 2553, true);


--
-- Name: recipe_sampleid_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipe_sampleid_seq', 1574, true);


--
-- Name: warzone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.warzone_id_seq', 2700, true);


--
-- Name: equipment equipment_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT equipment_name_key UNIQUE (name);


--
-- Name: equipment equipment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT equipment_pkey PRIMARY KEY (id);


--
-- Name: item_location item_location_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_location
    ADD CONSTRAINT item_location_pkey PRIMARY KEY (id);


--
-- Name: items items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- Name: player player_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.player
    ADD CONSTRAINT player_pkey PRIMARY KEY (id);


--
-- Name: queue queue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (sampleid);


--
-- Name: warzone_damage warzone_damage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warzone_damage
    ADD CONSTRAINT warzone_damage_pkey PRIMARY KEY (player_id, clan);


--
-- Name: warzone warzone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warzone
    ADD CONSTRAINT warzone_pkey PRIMARY KEY (id);


--
-- Name: item_location_name_key; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX item_location_name_key ON public.item_location USING btree (name);


--
-- Name: recipe fk_name_ingr_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_name_ingr_1 FOREIGN KEY (ingr_1) REFERENCES public.equipment(name);


--
-- Name: recipe fk_name_ingr_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_name_ingr_2 FOREIGN KEY (ingr_2) REFERENCES public.equipment(name);


--
-- Name: recipe fk_name_ingr_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_name_ingr_3 FOREIGN KEY (ingr_3) REFERENCES public.equipment(name);


--
-- Name: recipe fk_name_ingr_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_name_ingr_4 FOREIGN KEY (ingr_4) REFERENCES public.equipment(name);


--
-- Name: item_location fk_name_loc_equip; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.item_location
    ADD CONSTRAINT fk_name_loc_equip FOREIGN KEY (name) REFERENCES public.equipment(name);


--
-- Name: recipe fk_name_product; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT fk_name_product FOREIGN KEY (product) REFERENCES public.equipment(name);


--
-- Name: queue queue_material_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_material_id_fkey FOREIGN KEY (equipment_id) REFERENCES public.equipment(id);


--
-- PostgreSQL database dump complete
--

