import math
import time
import os
import datetime
from collections import Counter
import sqlalchemy
import difflib
from models import Location, Recipe, Warzone, Equipment, Queue, Player, Damage, Target
from discord.ext import commands, tasks
import discord
from sqlalchemy.orm import sessionmaker
from discord.utils import get

from dotenv import load_dotenv

load_dotenv()

# Change only the no_category default string
help_command = commands.DefaultHelpCommand(
    no_category='Commands'
)

intents = discord.Intents.all()
intents.message_content = True

# Create the bot and pass it the modified help_command
class MyBot(commands.Bot):
    async def setup_hook(self):
        print(1)
        
bot = MyBot(
    command_prefix=commands.when_mentioned_or('!'),
    help_command=help_command,
    intents=intents
)

@bot.listen()
async def on_ready():
    await bot.add_cog(reactions(bot))
    mytask.start() # important to start the loop
    veins_gg.start()
    targets_gg.start()
    targets_hg.start()


class reactions(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
 
    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        channel = await self.bot.fetch_channel(payload.channel_id)
        message = await channel.fetch_message(payload.message_id)
        context = await self.bot.get_context(message)
        context.channel = await self.bot.fetch_channel(BOT_COMMANDS_CHANNEL)

        # Find Dart reaction
        gg_reaction_1 = None
        gg_reaction_2 = None
        gg_reaction_3 = None
        hg_reaction_1 = None
        hg_reaction_2 = None
        hg_reaction_3 = None
        for reaction in message.reactions:
            if reaction.emoji == '1️⃣':
                gg_reaction_1 = reaction
            if reaction.emoji == '2️⃣':
                gg_reaction_2 = reaction
            if reaction.emoji == '3️⃣':
                gg_reaction_3 = reaction
            if reaction.emoji == '🇦':
                hg_reaction_1 = reaction
            if reaction.emoji == '🇧':
                hg_reaction_2 = reaction
            if reaction.emoji == '🇨':
                hg_reaction_3 = reaction

        # Proc commands when hitting 3 reactions on a message
        if payload.channel_id == ACTUAL_CHANNEL or payload.channel_id == BOT_DM_CHANNEL or payload.channel_id == BOT_COMMANDS_CHANNEL: 
            boss_name, zone = message.embeds[0].title.split(' - WZ')
            if gg_reaction_1 and gg_reaction_1.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'gg {zone} 0 {boss_name}')
            if gg_reaction_2 and gg_reaction_2.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'gg {zone} 1 {boss_name}')
            if gg_reaction_3 and gg_reaction_3.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'gg {zone} 2 {boss_name}')
            if hg_reaction_1 and hg_reaction_1.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'hg {zone} 0 {boss_name}')
            if hg_reaction_2 and hg_reaction_2.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'hg {zone} 1 {boss_name}')
            if hg_reaction_3 and hg_reaction_3.count == 3:
                await context.invoke(bot.get_command('targetInternal'), arg=f'hg {zone} 2 {boss_name}')


# engine = sqlalchemy.create_engine(f"postgres://byakugansql:ninjamanager", echo=True)
dbuser = os.getenv("DB_USER")
dbpass = os.getenv("DB_PASS")
database = os.getenv("DB_NAME")
dbport = os.getenv("DB_PORT")
engine = sqlalchemy.create_engine(
    f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=False)

# Channels
ACTUAL_CHANNEL = 837873317012045824
TESTING_CHANNEL = 473876541940695041
VEINS_CHANNEL = 1080262838515408966
GG_TARGETS_CHANNEL = 849748876247957505
HG_TARGETS_CHANNEL = 994008161323327681
HG_MESSAGE = 1080231518439473202
GG_MESSAGE = 1080226478387634277
VEINS_MESSAGE = 1080263689908793435
ROLE = 999467922202304512
BOT_ROLE = 1078499740406255640
BOT_DM_CHANNEL = 1078511442606694420
BOT_COMMANDS_CHANNEL = 960693299654582302

# Set global variables
clans = ['GG', 'HG']

material_list = []
with open('materials.txt', 'r') as f:
    for row in f:
        material_list.append(row.rstrip())
material_list.sort()

equipment_list = []
with open('equipment.txt', 'r') as f:
    for row in f:
        equipment_list.append(row.rstrip())
equipment_list.sort()

# Start DB Session
Session = sessionmaker(bind=engine)
db_session = Session()

# Class for storing the queue position information
class queue_pos:
    def __init__(self, mat_name, position, shop_qty=None, shop_val=None) -> None:
        self.mat_name = mat_name
        self.position = position
        self.shop_qty = shop_qty
        self.shop_val = shop_val

def calculate_clan_value(reward_name, val):
    # Check for entry in materials
    query_material = db_session.query(Equipment).filter(
        Equipment.name == reward_name).limit(1).first()

    # If in materials, check queue
    if query_material is not None:
        shop_val = float(query_material.shop_val)
        shop_qty = float(query_material.shop_qty)

        # If we_val, use it in calculations and check queues
        if query_material.we_val is not None:
            we_val = float(query_material.we_val)
            query_queue = db_session.query(Queue).filter(
                Queue.equipment_id == query_material.id).filter(Queue.clan == "GG").order_by(Queue.id)

            # First Check for queues, then sheck for shop values, then return if both are empty
            if len(query_queue.all()) >= 1:
                return math.ceil(((we_val / shop_val) * shop_val) * 2 * len(query_queue.all()))
            elif query_material.shop_qty > 0:
                return math.ceil((we_val / shop_val) * shop_val) * float(1/shop_qty)
            else:
                return math.ceil(((we_val / shop_val) * shop_val) * 2)

        # If no we_val and there are some in shop return the value * 1/qty, else return value * 2
        else:
            if query_material.shop_qty > 0:
                return math.ceil(float(val) * float(1/shop_qty))
            return val * 2

    # If none, add item to Items db. Return item value * 2 since there are none in shop
    db_session.add(
        Equipment(name=reward_name, url="", shop_qty=0, shop_val=val))
    db_session.commit()
    return val * 2


def rewards_to_list(rewards):
    reward_list = rewards.split(', ')
    print_out = ''
    gem_sum = 0
    clan_value_sum = 0

    for reward in reward_list:
        # Split rewards into name and value, then calculate clan value and tally
        reward_name, value = reward.split('$')
        val = int(value.replace(',', ''))
        clan_value = calculate_clan_value(reward_name, val)
        gem_sum += int(value.replace(',', ''))
        clan_value_sum += clan_value
        print_out = print_out + \
            f'\n- {reward_name}: {value} 💎 / {round(clan_value, 1)} 💵'
        # print_out = print_out + f'\n- {reward_name}'

    return print_out, gem_sum, clan_value_sum


def clan_value_to_readable(value):
    if value <= 1:
        valuation = "Awful"
    elif value <= 3:
        valuation = "Poor"
    elif value <= 7:
        valuation = "Average"
    elif value <= 10:
        valuation = "Good"
    elif value <= 15:
        valuation = "Great"
    else:
        valuation = "Godlike"

    return valuation


def report_boss(boss, is_new):
    # Set discord color based on warzone
    if boss.zone == 0:
        colorzone = discord.Color.green()
    elif boss.zone == 1:
        colorzone = discord.Color.blue()
    elif boss.zone == 2:
        colorzone = discord.Color.red()
    elif boss.zone == 3:
        colorzone = discord.Color.purple()
    elif boss.zone == 4:
        colorzone = discord.Color.gold()
    else:
        colorzone = discord.Color.teal()

    # Calculate boss title and health
    title_boss = f"{boss.name} - WZ{boss.zone+1}"
    health = boss.condition.split(' ')[2]
    embed = discord.Embed(
        title=title_boss,
        # description=f"{role_id.mention}",
        color=colorzone,
    )

    # Run commands to determine warzone valuation and sum rewards
    reward_output, gem_sum, clan_value_sum = rewards_to_list(boss.rewards)
    clan_value_sum = round(
        clan_value_sum / (int(health.replace(',', '')) / 10000), 2)
    valuation = clan_value_to_readable(clan_value_sum)

    # Update Boss Valuation in DB
    db_session.query(Warzone).filter(Warzone.grid == boss.grid).update(
        {Warzone.valuation: clan_value_sum})
    db_session.commit()

    # Check warzone rank
    total_entries = 0
    query_wazrone = db_session.query(Warzone).filter(
        Warzone.zone == boss.zone).filter(Warzone.valuation != None).filter(Warzone.defeated == False).order_by(Warzone.valuation.desc())
    for entry in query_wazrone:
        total_entries += 1
        if entry.grid == boss.grid:
            rank = total_entries

    # Set the remaining fields depending on whether or not the boss is new or not
    embed.set_thumbnail(url=boss.image)
    if not is_new:
        embed.add_field(
            name="*__Condition__*",
            value=f"{boss.condition}",
            inline=True,
        )
    else:
        embed.add_field(
            name="*__Total Health__*",
            value=f"{health}",
            inline=True,
        )
    embed.add_field(
        name="*__Duration__*",
        value=f"{boss.duration}",
        inline=True,
    )
    embed.add_field(
        name="*__Rewards__*",
        value=reward_output,
        inline=False
    )
    embed.add_field(
        name="*__Summary__*",
        value=f"\n- **Gem Total**: {gem_sum} 💎\n- **GG Clan Valuation**: {valuation} ({clan_value_sum})\n- **Warzone Rank**: {rank}/{total_entries}",
        inline=False
    )
    if not is_new:
        embed.add_field(
            name="*__Veins__*",
            value=f"-{boss.veins}"
        )
    return embed


def print_dict(dct):
    printed = []
    # for item, amount in dct.items():  # dct.iteritems() in Python 2
    for i in dct.most_common():
        printed.append("{} ({})\n".format(i[0], i[1]))
    return printed


@bot.command(brief="!untarget [gg/hg] [warzone(1-6)] [boss_name(exact)] (Removes Target)")
async def untarget(ctx, *, arg=''):
    # Check if user is Tsuki No Satori
    if not hasattr(ctx.author, 'roles') or ROLE not in [role.id for role in ctx.author.roles]:
        await ctx.channel.send(f"Tsuki No Satori+ role users only")
        return

    arg = arg.split()
    
    if len(arg) < 2: 
        await ctx.channel.send(f"Arguments provided incorrectly.")

    # Get Clan
    clan = arg[0].upper()

    if clan not in clans:
        await ctx.channel.send(f"Clan provided incorrectly.")
        return
    
    # Get Zone
    try:
        zone = int(arg[1])
    except:
        await ctx.channel.send(f"Zone provided incorrectly. Arguments must be seperated by ', '")
        return

    # Get Boss
    if len(arg[2:]) == 1: boss = arg[2:][0]
    else: boss = " ".join(arg[2:])

    query_boss_names = db_session.query(Warzone.name).filter(Warzone.name.isnot(None)).distinct().all()
    boss_name = difflib.get_close_matches(boss, [r for r, in query_boss_names])
    if not boss_name:
        await ctx.channel.send(f"Boss **{boss}** not found.")
        raise
    boss_name = boss_name[0]

    query_boss = db_session.query(Warzone).filter(Warzone.name == boss_name).filter(Warzone.zone == zone - 1).first()

    db_session.query(Target).filter(Target.boss_id == query_boss.id).filter(Target.clan == clan).delete()
    db_session.commit()
    await ctx.channel.send(f"**WZ{zone} {boss_name}** removed from targets")


@bot.command()
async def targetInternal(ctx, *, arg=''):
    # Check if user is Tsuki No Satori
    if not hasattr(ctx.author, 'roles') or (ROLE not in [role.id for role in ctx.author.roles] and BOT_ROLE not in [role.id for role in ctx.author.roles]):
        await ctx.channel.send(f"Tsuki No Satori+ role users only")
        return
    
    arg = arg.split()
    
    if len(arg) < 2: 
        await ctx.channel.send(f"Arguments provided incorrectly.")

    # Get Clan
    clan = arg[0].upper()

    if clan not in clans:
        await ctx.channel.send(f"Clan provided incorrectly.")
        return
    
    # Get Zone
    try:
        zone = int(arg[1])
    except:
        await ctx.channel.send(f"Zone provided incorrectly. Arguments must be seperated by ', '")
        return
    
    # Get Reward
    try:
        reward_num = int(arg[2])
    except:
        await ctx.channel.send(f"Reward provided incorrectly. Arguments must be seperated by ', '")
        return

    # Get Boss
    if len(arg[3:]) == 1: boss = arg[3:][0]
    else: boss = " ".join(arg[3:])

    query_boss_names = db_session.query(Warzone.name).filter(Warzone.name.isnot(None)).distinct().all()
    boss_name = difflib.get_close_matches(boss, [r for r, in query_boss_names])
    if not boss_name:
        await ctx.channel.send(f"Boss **{boss}** not found.")
        raise
    boss_name = boss_name[0]
    
    # Check zone
    if zone not in range(1, 7):
        await ctx.channel.send(f"Zone provided **{zone}** is not a number 1-6. Who are you, Vortex?")
        return

    # Query for equipment
    query_boss = db_session.query(Warzone).filter(Warzone.name == boss_name).filter(Warzone.zone == zone - 1).filter(Warzone.defeated == False).first()
    reward_list = query_boss.rewards.split(', ')
    reward_name, value = reward_list[reward_num].split('$')
    query_material = db_session.query(Equipment).filter(
        Equipment.name == reward_name).all()[0]

    # Locate Boss
    if not query_boss:
        await ctx.channel.send(f"Boss not found. Check name to make sure it is exact and it exists in warzone")
        return
    
    # Locate Target
    query_target = db_session.query(Target).filter(Target.boss_id == query_boss.id).filter(Target.clan == clan).first()
    if query_target:
        db_session.query(Target).filter(Target.boss_id == query_boss.id).filter(Target.clan == clan).update(
            {Target.equipment_id: query_material.id, Target.priority: 1})
        db_session.commit()
        await ctx.channel.send(f"**{boss_name}** updated")
        return

    # Add boss to targets
    target = Target()
    target.equipment_id = query_material.id
    target.boss_id = query_boss.id
    target.clan = clan
    target.priority = 1
    db_session.add(target)
    db_session.flush()
    db_session.commit()
    await ctx.channel.send(f"**{boss_name}** added to **{clan} WZ{zone}** targets for {query_material.name}!")


@bot.command(brief="!target [gg/hg] [warzone(1-6)] [boss_name(exact)] (Adds Target)")
async def target(ctx, *, arg=''):
    # Check if user is Tsuki No Satori
    if not hasattr(ctx.author, 'roles') or (ROLE not in [role.id for role in ctx.author.roles] and BOT_ROLE not in [role.id for role in ctx.author.roles]):
        await ctx.channel.send(f"Tsuki No Satori+ role users only")
        return
    
    arg = arg.split()
    
    if len(arg) < 2: 
        await ctx.channel.send(f"Arguments provided incorrectly.")

    # Get Clan
    clan = arg[0].upper()

    if clan not in clans:
        await ctx.channel.send(f"Clan provided incorrectly.")
        return
    
    # Get Zone
    try:
        zone = int(arg[1])
    except:
        await ctx.channel.send(f"Zone provided incorrectly. Arguments must be seperated by ', '")
        return

    # Get Boss
    if len(arg[2:]) == 1: boss = arg[2:][0]
    else: boss = " ".join(arg[2:])

    query_boss_names = db_session.query(Warzone.name).filter(Warzone.name.isnot(None)).distinct().all()
    boss_name = difflib.get_close_matches(boss, [r for r, in query_boss_names])
    if not boss_name:
        await ctx.channel.send(f"Boss **{boss}** not found.")
        raise
    boss_name = boss_name[0]
    
    # Check zone
    if zone not in range(1, 7):
        await ctx.channel.send(f"Zone provided **{zone}** is not a number 1-6. Who are you, Vortex?")
        return

    # Query for equipment
    query_boss = db_session.query(Warzone).filter(Warzone.name == boss_name).filter(Warzone.zone == zone - 1).filter(Warzone.defeated == False).first()
    reward_list = query_boss.rewards.split(', ')
    reward_name, value = reward_list[0].split('$')
    query_material = db_session.query(Equipment).filter(
        Equipment.name == reward_name).all()[0]


    # Locate Boss
    if not query_boss:
        await ctx.channel.send(f"Boss not found. Check name to make sure it is exact and it exists in warzone")
        return
    
    
    # Locate Target
    query_target = db_session.query(Target).filter(Target.boss_id == query_boss.id).filter(Target.clan == clan).first()
    if query_target:
        db_session.query(Target).filter(Target.boss_id == query_boss.id).filter(Target.clan == clan).update(
            {Target.equipment_id: query_material.id, Target.priority: 1})
        db_session.commit()
        await ctx.channel.send(f"**{boss_name}** updated")
        return

    # Add boss to targets
    target = Target()
    target.equipment_id = query_material.id
    target.boss_id = query_boss.id
    target.clan = clan
    target.priority = 1
    db_session.add(target)
    db_session.flush()
    db_session.commit()
    await ctx.channel.send(f"**{boss_name}** added to **{clan} WZ{zone}** targets!")


@tasks.loop(seconds=60)
async def targets_hg():
    await bot.wait_until_ready()
    channel = bot.get_channel(HG_TARGETS_CHANNEL)

    # Format Output
    embed = discord.Embed(
        title="Warzone Targets List",
        color=discord.Color.gold(),
    )

    zone = 5

    while zone > 0:
        # Get targets from clan
        query_targets = db_session.query(Target, Warzone, Equipment).filter(Target.boss_id == Warzone.id).filter(Target.equipment_id == Equipment.id).filter(
            Target.clan == 'HG').filter(Warzone.zone == zone - 1).order_by(Target.priority.asc()).all()

        # Hoshigakure
        text = ""
        for target, warzone, equipment in query_targets:
            remaining_health = int(warzone.condition.split(" ")[0].replace(',', ''))
            remaining_time = warzone.duration.replace("Battle ends in ", "").replace(".", "")
            reward_list = warzone.rewards.split(', ')
            damage_list = warzone.damage.split(', ')
            reward_position = -10
            clan_damage = -10
            clan_position = -10
            winning_clan = 'Hoshigakure'
            winning_clan_damage = -10

            # if no damage list
            if not warzone.damage:
                text = text + \
                    f"🟧 **{warzone.name} ({equipment.name}):** \n\xa0\xa0\xa0\xa0- No Damage dealt yet to target by any clans\n"
                continue

            # if random add to top of list
            if warzone.random:
                damage_list.insert(0, 'RANDOM$10,000,000')

            # find reward position
            for index, reward in enumerate(reward_list):
                reward_name, value = reward.split('$')
                if reward_name == equipment.name:
                    reward_position = index

            # if no reward
            if reward_position == -10:
                text = text + \
                    f"- **{warzone.name} ({equipment.name}):** Targetted Reward not found, Contact Apex\n"
                continue

            # find damage position
            for index, damage in enumerate(damage_list):
                clan, damage_val = damage.split('$')
                # our damage found
                if clan == 'Hoshigakure':
                    clan_damage = int(damage_val.replace(',', ''))
                    clan_position = index
                if index == reward_position:
                    winning_clan = clan
                    winning_clan_damage = int(damage_val.replace(',', ''))
            
            # NO DAMAGE DEALT
            if clan_damage == -10:
                # if no clan hit yet for reward:
                if winning_clan_damage == -10:
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- No HG damage, not yet damaged at this reward level.\n"    
                else:
                    if remaining_health < winning_clan_damage:
                        db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'GG').delete()
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- No HG damage, {winning_clan} dealt {format (winning_clan_damage, ',d')}\n"
                continue

            # IF ahead
            if clan_damage >= winning_clan_damage:                    
                if clan_position + 1 < len(damage_list):
                    next_clan, next_damage_val = damage_list[clan_position + 1].split('$')
                    next_damage_val = int(next_damage_val.replace(',', ''))
                    if remaining_health < clan_damage - next_damage_val:
                        db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'HG').delete()
                        # text = text + \
                        #     f"❤️‍🔥 **{warzone.name} ({equipment.name})** \n- {equipment.name} secured, no need to attack further\n" # TODO: ADD HOW MUCH NEEDED TO SECURE
                    else:
                        text = text + \
                            f"🟩 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- Ahead of {next_clan} by {format (clan_damage - next_damage_val, ',d')} \n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n" # TODO: ADD HOW MUCH NEEDED TO SECURE
                else:
                    text = text + \
                        f"🟩 **{warzone.name} ({equipment.name})** Ahead, dealt {format (clan_damage, ',d')} total damage! \n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n"
                continue
                    
            # If Behind
            if clan_damage < winning_clan_damage:
                if remaining_health < winning_clan_damage - clan_damage:
                    db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'HG').delete()
                else:
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- Behind {winning_clan} by {format (winning_clan_damage - clan_damage, ',d')} \n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n"
                continue

        embed.add_field(
            name=f"**Warzone {zone}:**",
            value=text,
            inline=False,
        )
        zone = zone - 1

    e = datetime.datetime.now()
    time = e.strftime("%b %d, %I:%M %p")
    db_session.commit()

    embed.add_field(
        name=f"**Last Updated:** {time} EST",
        value="",
        inline=False,
    )

    message = await channel.fetch_message(HG_MESSAGE)

    await message.edit(embed=embed)


@tasks.loop(seconds=60)
async def targets_gg():
    await bot.wait_until_ready()
    channel = bot.get_channel(GG_TARGETS_CHANNEL)

    # Format Output
    embed = discord.Embed(
        title="Warzone Targets List",
        color=discord.Color.gold(),
    )

    zone = 5

    while zone > 0:
        # Get targets from clan
        query_targets = db_session.query(Target, Warzone, Equipment).filter(Target.boss_id == Warzone.id).filter(Target.equipment_id == Equipment.id).filter(
            Target.clan == 'GG').filter(Warzone.zone == zone - 1).order_by(Target.priority.asc()).all()

        # Gekkogakure
        text = ""
        for target, warzone, equipment in query_targets:
            remaining_health = int(warzone.condition.split(" ")[0].replace(',', ''))
            remaining_time = warzone.duration.replace("Battle ends in ", "").replace(".", "")
            reward_list = warzone.rewards.split(', ')
            damage_list = warzone.damage.split(', ')
            reward_position = -10
            clan_damage = -10
            clan_position = -10
            winning_clan = 'Gekkogakure'
            winning_clan_damage = -10

            # if no damage list
            if not warzone.damage:
                text = text + \
                    f"🟧 **{warzone.name} ({equipment.name}):** \n\xa0\xa0\xa0\xa0- No Damage dealt yet to target by any clans\n"
                continue

            # if random add to top of list
            if warzone.random:
                damage_list.insert(0, 'RANDOM$10,000,000')

            # find reward position
            for index, reward in enumerate(reward_list):
                reward_name, value = reward.split('$')
                if reward_name == equipment.name:
                    reward_position = index

            # if no reward
            if reward_position == -10:
                text = text + \
                    f"- **{warzone.name} ({equipment.name}):** Targetted Reward not found, Contact Apex\n"
                continue

            # find damage position
            for index, damage in enumerate(damage_list):
                clan, damage_val = damage.split('$')
                # our damage found
                if clan == 'Gekkogakure':
                    clan_damage = int(damage_val.replace(',', ''))
                    clan_position = index
                if index == reward_position:
                    winning_clan = clan
                    winning_clan_damage = int(damage_val.replace(',', ''))
            
            # NO DAMAGE DEALT
            if clan_damage == -10:
                # if no clan hit yet for reward:
                if winning_clan_damage == -10:
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- No GG damage, not yet damaged at this reward level.\n"    
                else:
                    # Check to make sure reward is still acheiveable
                    if remaining_health < winning_clan_damage:
                        db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'GG').delete()
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- No GG damage, {winning_clan} dealt {format (winning_clan_damage, ',d')}\n"
                continue

            # IF ahead
            if clan_damage >= winning_clan_damage:                    
                if clan_position + 1 < len(damage_list):
                    next_clan, next_damage_val = damage_list[clan_position + 1].split('$')
                    next_damage_val = int(next_damage_val.replace(',', ''))
                    if remaining_health < clan_damage - next_damage_val:
                        db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'GG').delete()
                        # text = text + \
                        #     f"❤️‍🔥 **{warzone.name} ({equipment.name})** \n- {equipment.name} secured, no need to attack further\n" # TODO: ADD HOW MUCH NEEDED TO SECURE
                    else:
                        text = text + \
                            f"🟩 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- Ahead of {next_clan} by {format (clan_damage - next_damage_val, ',d')} \n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n\n" # TODO: ADD HOW MUCH NEEDED TO SECURE
                else:
                    text = text + \
                        f"🟩 **{warzone.name} ({equipment.name})** Ahead, dealt {format (clan_damage, ',d')} total damage!\n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n"
                continue
                    
            # If Behind
            if clan_damage < winning_clan_damage:
                if remaining_health < winning_clan_damage - clan_damage:
                    db_session.query(Target).filter(Target.boss_id == target.boss_id).filter(Target.clan == 'GG').delete()
                else:
                    text = text + \
                        f"🟥 **{warzone.name} ({equipment.name})** \n\xa0\xa0\xa0\xa0- Behind {winning_clan} by {format (winning_clan_damage - clan_damage, ',d')} \n\xa0\xa0\xa0\xa0- {format (remaining_health, ',d')} HP/{remaining_time} remaining!\n\n"
                continue

        embed.add_field(
            name=f"**Warzone {zone}:**",
            value=text,
            inline=False,
        )
        zone = zone - 1

    e = datetime.datetime.now()
    time = e.strftime("%b %d, %I:%M %p")
    db_session.commit()

    embed.add_field(
        name=f"**Last Updated:** {time} EST",
        value="",
        inline=False,
    )

    message = await channel.fetch_message(GG_MESSAGE)

    await message.edit(embed=embed)


@bot.command(brief="!wz [1-6] (Shows all bosses in a warzone ranked by value to clan)")
async def wz(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = int(arg.split()[0])
    if zone not in range(1, 7):
        await ctx.channel.send(f"Zone provided **{zone}** is not a number 1-6. Who are you, Vortex?")
        return

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz1 (Shows all bosses in a warzone ranked by value to clan)")
async def wz1(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 1

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz2 (Shows all bosses in a warzone ranked by value to clan)")
async def wz2(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 2

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz3 (Shows all bosses in a warzone ranked by value to clan)")
async def wz3(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 3

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz4 (Shows all bosses in a warzone ranked by value to clan)")
async def wz4(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 4

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz5 (Shows all bosses in a warzone ranked by value to clan)")
async def wz5(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 5

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!wz6 (Shows all bosses in a warzone ranked by value to clan)")
async def wz6(ctx, *, arg="0"):
    # Get zone and insure it's in range
    zone = 6

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone - 1).order_by(Warzone.valuation.desc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **{boss.name}: {clan_value_to_readable(boss.valuation)}** *[{boss.valuation} value]* *({boss.health_percentage}% health)*\n"

    # Send embed
    embed = discord.Embed(
        title=f"Zone {zone} Bosses ordered by clan value",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(help="Report boss status", brief="Show all alive bosses from a specific zone")
async def status(ctx, *, arg="0"):
    """ !status (spams bot, do not run)"""
    await ctx.channel.send("The current enemies on the battlefield are:")
    if arg == "0":
        # For each Warzone, reports each boss and self reacts to allow others to easily vote
        for zone in range(6):
            await ctx.channel.send(f"--- *Warzone {zone+1}* ---")
            query_boss = db_session.query(Warzone).filter(
                Warzone.name != '', Warzone.defeated == False, Warzone.zone == zone)
            for boss in query_boss:
                result = report_boss(boss, False)
                message = await ctx.channel.send(embed=result)
                await message.add_reaction('1️⃣')
                await message.add_reaction('2️⃣')
                await message.add_reaction('3️⃣')
                await message.add_reaction('🇦')
                await message.add_reaction('🇧')
                await message.add_reaction('🇨')
            time.sleep(2)

    # If warzone argument is provided
    else:
        await ctx.channel.send(f"--- *Warzone {arg}* ---")
        query_boss = db_session.query(Warzone).filter(
            Warzone.name != '', Warzone.defeated == False, Warzone.zone == int(arg)-1)
        for boss in query_boss:
            result = report_boss(boss, False)
            message = await ctx.channel.send(embed=result)
            await message.add_reaction('1️⃣')
            await message.add_reaction('2️⃣')
            await message.add_reaction('3️⃣')
            await message.add_reaction('🇦')
            await message.add_reaction('🇧')
            await message.add_reaction('🇨')


@tasks.loop(seconds=600)
async def veins_gg():
    await bot.wait_until_ready()
    channel = bot.get_channel(VEINS_CHANNEL)

    # Get all bosses in a warzone descending by valuation
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == False, Warzone.veins != '').order_by(Warzone.zone.asc())

    # Format return string
    text = ""
    for boss in query_boss:
        text = text + \
            f"- **WZ{boss.zone + 1} {boss.name}: ** {boss.veins}\n"

    # Send embed
    embed = discord.Embed(
        title=f"GG Veins on Alive Bosses",
        description=text,
        color=discord.Color.gold(),
    )

    e = datetime.datetime.now()
    time = e.strftime("%b %d, %I:%M %p")

    embed.add_field(
        name=f"**Last Updated:** {time} EST",
        value="",
        inline=False,
    )

    message = await channel.fetch_message(VEINS_MESSAGE)

    await message.edit(embed=embed)


@tasks.loop(seconds=14400)
async def periodic_report():
    channel = bot.get_channel(ACTUAL_CHANNEL)
    await channel.send("Periodic report")
    query_boss = db_session.query(Warzone).filter(
        Warzone.name != '', Warzone.defeated == 0)
    await channel.send("The current enemies on the battlefield are:")
    for boss in query_boss:
        result = report_boss(boss, True)
        message = await channel.send(embed=result)
        await message.add_reaction('1️⃣')
        await message.add_reaction('2️⃣')
        await message.add_reaction('3️⃣')
        await message.add_reaction('🇦')
        await message.add_reaction('🇧')
        await message.add_reaction('🇨')



@tasks.loop(seconds=45)
async def mytask():
    print("searching for new bosses...")
    await bot.wait_until_ready()
    channel = bot.get_channel(ACTUAL_CHANNEL)
    # role_id = get(channel.guild.roles, name='SAIZENSEN NO SENSHI')
    db_session = Session()
    query_new = db_session.query(Warzone).filter(
        Warzone.reported == False, Warzone.name != '', Warzone.defeated == False)
    for boss in query_new:
        print("New boss found!")
        await channel.send(f"A new danger has appeared on the battlefield!")
        result = report_boss(boss, True)
        message = await channel.send(embed=result)
        await message.add_reaction('1️⃣')
        await message.add_reaction('2️⃣')
        await message.add_reaction('3️⃣')
        await message.add_reaction('🇦')
        await message.add_reaction('🇧')
        await message.add_reaction('🇨')
        boss.reported = 1
    db_session.commit()


# Recipe and find system

async def check_equipment_input(ctx, equipment):
    # if no equipment listed
    if equipment == []:
        await ctx.channel.send(f"Equipment not provided. Try again")
        raise

    equipment = ' '.join(equipment)
    equipment_name = difflib.get_close_matches(equipment, equipment_list)
    if not equipment_name:
        await ctx.channel.send(f"Equipment **{equipment}** not found.")
        raise
    equipment_name = equipment_name[0]

    return equipment_name


@bot.command(brief="!craft [item] (shows crafting recipes for an item)")
async def craft(ctx, *, arg=''):
    # NOTE: Deep not currently functioning. Will fix later.
    try:
        if arg.split()[0].upper() == '-DEEP':
            deep = True
            equipment_name = await check_equipment_input(ctx, arg.split()[1:])
        else:
            deep = False
            equipment_name = await check_equipment_input(ctx, arg.split()[0:])
    except:
        return

    # Pull Equipment Recipes and check for no length
    equipment_list = db_session.query(Recipe).filter(
        Recipe.product == equipment_name).order_by(Recipe.product_qty.desc())

    if len(equipment_list.all()) == 0:
        material_selected = db_session.query(Location).filter(
            Location.name == equipment_name).order_by(Location.chance.desc())
        if len(material_selected.all()) == 0:
            await ctx.channel.send('Item is either a ninja or shop item. If this is incorrect, ping Apex on the discord')
            return
        await ctx.channel.send("Recipe not found but hunting locations are available:")
        await ctx.invoke(bot.get_command('find'), arg=arg)
        return

    # Check if any exist in shop and add to print out if they do
    text = ""
    equipment = db_session.query(Equipment).filter(
        Equipment.name == equipment_name).first()
    if equipment.shop_qty > 0:
        text = text + \
            f"**Clan Shop:** {equipment.shop_val if equipment.shop_val else equipment.shop_val} 💎 *({equipment.shop_qty} available)*\n\n"

    # Format text and send
    for entry in equipment_list:
        if entry.product_qty > 1:
            text = text + \
                f"**{entry.product} x{entry.product_qty} **\n- {entry.qty_1}x {entry.ingr_1}"
        else:
            text = text + \
                f"**{entry.product}**\n- {entry.qty_1}x {entry.ingr_1}"
        if entry.ingr_2:
            text = text + f"\n- {entry.qty_2}x {entry.ingr_2}"
        if entry.ingr_3:
            text = text + f"\n- {entry.qty_3}x {entry.ingr_3}"
        if entry.ingr_4:
            text = text + f"\n- {entry.qty_4}x {entry.ingr_4}"
        text = text + "\n\n"

    embed = discord.Embed(
        title=f"{equipment_name} Recipes",
        description=text,
        color=discord.Color.gold(),
    )
    await ctx.channel.send(embed=embed)

    # if deep:
    #     if entry.ingr_1 in material_list:
    #         await ctx.invoke(bot.get_command('find'), arg=entry.ingr_1)
    #     else:
    #         await ctx.invoke(bot.get_command('craft'), arg=entry.ingr_1)
    #     if entry.ingr_2:
    #         if entry.ingr_2 in material_list:
    #             await ctx.invoke(bot.get_command('find'), arg=entry.ingr_2)
    #         else:
    #             await ctx.invoke(bot.get_command('craft'), arg=entry.ingr_2)
    #     if entry.ingr_3:
    #         if entry.ingr_3 in material_list:
    #             await ctx.invoke(bot.get_command('find'), arg=entry.ingr_3)
    #         else:
    #             await ctx.invoke(bot.get_command('craft'), arg=entry.ingr_3)
    #     if entry.ingr_4:
    #         if entry.ingr_4 in material_list:
    #             await ctx.invoke(bot.get_command('find'), arg=entry.ingr_4)
    #         else:
    #             await ctx.invoke(bot.get_command('craft'), arg=entry.ingr_4)


@bot.command(brief="!find [item] (shows locations for an item on the WM)")
async def find(ctx, *, arg=''):
    try:
        equipment_name = await check_equipment_input(ctx, arg.split()[0:])
    except:
        return

    # Pull Equipment Locations and check for no length
    material_selected = db_session.query(Location).filter(
        Location.name == equipment_name).order_by(Location.chance.desc())

    # If there is no location, then check for a recipe, send error if no recipe either.
    if len(material_selected.all()) == 0:
        equipment_list = db_session.query(Recipe).filter(
            Recipe.product == equipment_name).order_by(Recipe.product_qty.desc())
        if len(equipment_list.all()) == 0:
            await ctx.channel.send('Item is either a ninja or shop item. If this is incorrect, ping Apex on the discord')
            return
        await ctx.channel.send("Locations not found but crafting recipes are available:")
        await ctx.invoke(bot.get_command('craft'), arg=arg)
        return

    # Check if any exist in shop and add to print out if they do
    text = ""
    equipment = db_session.query(Equipment).filter(
        Equipment.name == equipment_name).first()
    if equipment.shop_qty > 0:
        text = text + \
            f"- **Clan Shop:** {equipment.shop_val} 💎 *({equipment.shop_qty} available)*\n"

    # Format text for every location found
    for entry in material_selected:
        text = text + f"- {entry.location}: {entry.chance}%"
        if entry.difficulty:
            text = text + f" ***{entry.difficulty}+***"
        if entry.note:
            text = text + f" ***({entry.note})***"
        text = text + "\n"

    # Send embed
    embed = discord.Embed(
        title=f"{equipment_name} Locations",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!uses [item] (shows uses for an item)")
async def uses(ctx, *, arg=''):
    try:
        equipment_name = await check_equipment_input(ctx, arg.split()[0:])
    except:
        return

    # Check for ingr_1
    equipment_list = db_session.query(Recipe).filter(
        Recipe.ingr_1 == equipment_name)

    text=""

    # Format text for every location found
    for entry in equipment_list:
        text = text + \
            f"- **{entry.product}**: {entry.qty_1}x {entry.ingr_1}\n"

        # Check for ingr_2
    equipment_list = db_session.query(Recipe).filter(
        Recipe.ingr_2 == equipment_name)

    # Format text for every location found
    for entry in equipment_list:
        text = text + \
            f"- **{entry.product}**: {entry.qty_2}x {entry.ingr_2}\n"

        # Check for ingr_1
    equipment_list = db_session.query(Recipe).filter(
        Recipe.ingr_3 == equipment_name)

    # Format text for every location found
    for entry in equipment_list:
        text = text + \
            f"- **{entry.product}**: {entry.qty_3}x {entry.ingr_3}\n"

        # Check for ingr_1
    equipment_list = db_session.query(Recipe).filter(
        Recipe.ingr_4 == equipment_name)

    # Format text for every location found
    for entry in equipment_list:
        text = text + \
            f"- **{entry.product}**: {entry.qty_4}x {entry.ingr_4}\n"

    if text == "":
        text = "**None**"

    # Send embed
    embed = discord.Embed(
        title=f"{equipment_name} Uses",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


# Queue system

def get_url(material):
    url = "https://www.ninjamanager.com/img/material/large/"
    url += material.name.replace(' ', '-').lower()
    url += ".png"
    return url


def show_queue(material, clan, players):
    image_url = get_url(material)
    if clan == 'GG':
        colorzone = discord.Color.gold()
    else:
        colorzone = discord.Color.red()
    embed = discord.Embed(
        title=f"{material.name} ({clan})",
        # description=f"GG",
        color=colorzone,
    )
    # embed.set_author(name="boss")
    embed.set_thumbnail(url=image_url)
    value_text = ""
    for i, entry in enumerate(players):
        value_text = value_text+f"**{i+1}.** {entry}\n"

    if value_text == "":
        value_text = "**Empty**"

    embed.add_field(
        name=f"Queue Order",
        value=value_text,
        inline=False,
    )
    return embed


async def check_first_in_queue(discord_id, clan, material_name):
    # Find material in db or None if no material found
    material_selected = db_session.query(Equipment).filter(
        Equipment.name == material_name).first()

    if material_selected:
        # Query to get the queue for the material
        query_queue = db_session.query(Queue).filter(
            Queue.equipment_id == material_selected.id).filter(Queue.clan == clan).order_by(Queue.id)

        # If Queue does not exist, return false
        if query_queue.first() is None:
            return False

        # If Queue does exist, return a boolean check for if discord Id's match
        query_player = db_session.query(Player).filter(
            Player.discord_id == query_queue[0].player_id).first()
        return query_player.discord_id == discord_id

    # Code should not reach here, fail safe not to crash the bot
    else:
        print(f"ERROR: ITEM {material_name} NOT FOUND IN DB")
        return False


async def check_input(ctx, arg):
    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel
    # If gg or hg not provided
    if arg == '' or arg.split()[0].upper() not in clans:
        # Request gg or hg and set variables
        await ctx.channel.send(f"Which clan? GG or HG?")
        msg = await bot.wait_for("message", check=check)
        if msg.content.lower() != "gg" and msg.content.lower() != "hg":
            await ctx.channel.send(f"That's not HG or GG moron. Try the command again")
            raise
        else:
            clan = msg.content.upper()
            material = arg.split()[0:]

    else:
        clan = arg.split()[0].upper()
        material = arg.split()[1:]
        # If no material listed after giving gg or hg
        if material == []:
            await ctx.channel.send(f"Material not provided. Try again")
            raise

    # Return material name and clan
    material = ' '.join(material)
    material_name = difflib.get_close_matches(material, material_list)
    if not material_name:
        await ctx.channel.send(f"Material **{material}** not found.")
        raise
    material_name = material_name[0]

    return material_name, clan


@bot.command(brief="!join [gg/hg] [material_name] (joins you at the end of the queue for an item)")
async def join(ctx, *, arg=''):
    query_player = db_session.query(Player).filter(
        Player.discord_id == ctx.author.id).first()
    # Add player to database with display_name if it's the first time
    if not query_player:
        db_session.add(Player(discord_id=ctx.author.id,
                       display_name=ctx.author.display_name))
        db_session.commit()

    # Check input
    try:
        material_name, clan = await check_input(ctx, arg)
    except:
        return

    # Query for queue and equipment
    query_material = db_session.query(Equipment).filter(
        Equipment.name == material_name).all()[0]
    query_queue = db_session.query(Queue).filter(Queue.player_id == ctx.author.id).filter(
        Queue.clan == clan).filter(Queue.equipment_id == query_material.id).first()

    # Make sure the user is not already in the queue and that the material was found
    if query_queue:
        await ctx.channel.send(f"You are already in that list.")
        return
    if not query_material:
        await ctx.channel.send(f"Material not found.")
        return

    # Add user to queue
    queue = Queue()
    queue.equipment_id = query_material.id
    queue.player_id = ctx.author.id
    queue.clan = clan
    db_session.add(queue)
    db_session.flush()
    db_session.commit()
    await ctx.channel.send(f"**{ctx.author.display_name}** has joined the **{clan}** queue for **{material_name}**")
    await ctx.invoke(bot.get_command('show'), arg=f'{clan} {material_name}')


@bot.command(brief="!show [gg/hg] [material_name] (shows you the queue for an item)")
async def show(ctx, *, arg=''):
    # Check input
    try:
        material_name, clan = await check_input(ctx, arg)
    except:
        return

    # Query for equipment
    material_selected = db_session.query(Equipment).filter(
        Equipment.name == material_name).first()

    # Print queue for user
    if material_selected:
        query_queue = db_session.query(Queue).filter(
            Queue.equipment_id == material_selected.id).filter(Queue.clan == clan).order_by(Queue.id)
        players = []
        for entry in query_queue:
            query_player = db_session.query(Player).filter(
                Player.discord_id == entry.player_id).first()
            players.append(query_player.display_name)
        result = show_queue(material_selected, clan, players)
        await ctx.channel.send(embed=result)


@bot.command(brief="!leave [gg/hg] [material_name] (leaves the queue)")
async def leave(ctx, *, arg=''):
    # Check Input
    try:
        material_name, clan = await check_input(ctx, arg)
    except:
        return

    # Query for the queue
    query_material = db_session.query(Equipment).filter(
        Equipment.name == material_name)
    delete_queue = db_session.query(Queue).filter(Queue.player_id == ctx.author.id).filter(
        Queue.equipment_id == query_material[0].id).filter(Queue.clan == clan).delete()

    # Make sure the query queue exists
    if delete_queue == 0:
        await ctx.channel.send(f"**{ctx.author.display_name}** is not in the queue for **{material_name}**")
        await ctx.invoke(bot.get_command('show'), arg=f'{clan} {material_name}')
        return

    # Print out and commit changes
    await ctx.channel.send(f"**{ctx.author.display_name}** has left the **{clan}** queue for **{material_name}**")
    db_session.commit()


@bot.command(brief="!claim [gg/hg] [material_name] (leaves and rejoins the queue if you are first in line)")
async def claim(ctx, *, arg=''):
    player_name = ctx.author.display_name

    # Check input
    try:
        material_name, clan = await check_input(ctx, arg)
    except:
        return

    # Check to make sure user is first inqueue
    if not await check_first_in_queue(ctx.message.author.id, clan, material_name):
        await ctx.channel.send(f"**{player_name}** is not first in queue for **{material_name}** ({clan}).")
        await ctx.invoke(bot.get_command('show'), arg=f'{clan} {material_name}')
        return

    # If theya re allow them to claim and leave/rejoin
    await ctx.channel.send(f"**{player_name}** has claimed **{material_name}** ({clan}).")
    await ctx.invoke(bot.get_command('leave'), arg=f'{clan} {material_name}')
    await ctx.invoke(bot.get_command('join'), arg=f'{clan} {material_name}')


@bot.command(brief="!listall (shows you all materials in game if you need to join a queue)")
async def listall(ctx):
    """ !listall (shows you all materials in game if you need to join a queue) """
    content = (', '.join(material_list))
    divider = int(len(material_list)/2)

    await ctx.channel.send(', '.join(material_list[:divider]))
    await ctx.channel.send(', '.join(material_list[divider:]))


@bot.command(brief="!checkclaim (checks for any claimable materials meaning they are available in shop and you are 1st in queue)")
async def checkclaim(ctx, *, arg=''):
    for clan in clans:
        # Pull queues and queue positions
        query_queue = db_session.query(Queue).filter(
            Queue.clan == clan).order_by(Queue.id).all()
        my_queues = []
        my_positions = [1 for i in range(10000)]
        for entry in query_queue:
            if entry.player_id == ctx.author.id:
                query_material = db_session.query(Equipment).filter(
                    Equipment.id == entry.equipment_id).first()
                my_queues.append(queue_pos(
                    query_material.name,
                    my_positions[entry.equipment_id],
                    query_material.shop_qty,
                    query_material.shop_val))
            else:
                my_positions[entry.equipment_id] = my_positions[entry.equipment_id] + 1

        # Sort each queue by position and make print out has each item that they are first in and has a shop qty
        my_queues.sort(key=lambda x: x.position)
        print_out = f'**{clan} Claimable Shop Items:**'
        for i in range(len(my_queues)):
            if my_queues[i].position == 1 and my_queues[i].shop_qty > 0 and clan != 'HG':
                print_out = print_out + \
                    f'\n- **{my_queues[i].mat_name}:** \n- Cost: {my_queues[i].shop_val} 💎  \n- In Shop: {my_queues[i].shop_qty}'

        # If there are queues print the print out, else print that none are available
        if my_queues != []:
            await ctx.channel.send(print_out)


@bot.command(brief="!listhot [gg/hg] (shows you all active queues for gg/hg)")
async def listhot(ctx, *, arg=''):
    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel
    # if gg or hg not provided, same as check input but without material
    if arg == '' or arg.split()[0].upper() not in clans:
        await ctx.channel.send(f"Which clan? GG or HG?")
        msg = await bot.wait_for("message", check=check)
        if msg.content.lower() != "gg" and msg.content.lower() != "hg":
            await ctx.channel.send(f"That's not HG or GG moron. Try the command again")
            return
        else:
            clan = msg.content.upper()
    else:
        clan = arg.split()[0].upper()

    # Query all queues and list how many are in each
    query_queue = db_session.query(Queue).filter(Queue.clan == clan).all()
    hotlist = []
    for entry in query_queue:
        query_material = db_session.query(Equipment).filter(
            Equipment.id == entry.equipment_id).first()
        hotlist.append(query_material.name)
    counted = Counter(hotlist)
    nice = print_dict(counted)
    await ctx.channel.send(f'Most queued items for {clan}:')
    await ctx.channel.send(''.join(nice))


@bot.command(brief="!listme (lists your positions in both gg/hg queues)")
async def listme(ctx, *, arg=''):
    for clan in clans:
        # Get queue and positions
        query_queue = db_session.query(Queue).filter(
            Queue.clan == clan).order_by(Queue.id).all()
        my_queues = []
        # If materials go past this in ID it needs to be updated
        my_positions = [1 for i in range(10000)]
        for entry in query_queue:
            if entry.player_id == ctx.author.id:
                query_material = db_session.query(Equipment).filter(
                    Equipment.id == entry.equipment_id).first()
                my_queues.append(queue_pos(query_material.name,
                                 my_positions[entry.equipment_id]))
            else:
                my_positions[entry.equipment_id] = my_positions[entry.equipment_id] + 1

        # Sort by position and format print out
        my_queues.sort(key=lambda x: x.position)
        print_out = f'**{clan} list:**'
        for i in range(len(my_queues)):
            print_out = print_out + \
                f'\n- {my_queues[i].mat_name} (#{my_queues[i].position})'

        # If there are queues print the print out, else print that none are available
        if my_queues != []:
            await ctx.channel.send(print_out)
        else:
            await ctx.channel.send(f'**{clan} list:** *Empty*')


# Damage System

async def check_damage_args(ctx, arg):
    # Check args
    if arg == "":
        await ctx.channel.send(f"Argument not provided. Try again")
        raise

    # Check clans input
    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel
    # if gg or hg not provided
    if arg == '' or arg.split()[0].upper() not in clans:
        await ctx.channel.send(f"Which clan? GG or HG?")
        msg = await bot.wait_for("message", check=check)
        if msg.content.lower() != "gg" and msg.content.lower() != "hg":
            await ctx.channel.send(f"That's not HG or GG moron. Try the command again")
            raise
        else:
            clan = msg.content.upper()
            input = int(arg.split()[0].replace(',', ''))
    else:
        clan = arg.split()[0].upper()
        if len(arg.split()) < 2:
            await ctx.channel.send(f"Argument not provided. Try again")
            raise
        input = int(arg.split()[1].replace(',', ''))
        # If no material listed after giving gg or hg
        if input == []:
            await ctx.channel.send(f"Argument not provided. Try again")
            raise

    return clan, input


@bot.command(brief="!setwz [gg/hg] [wz(1-6)] (Sets your highest reachable wz)")
async def setwz(ctx, *, arg=''):
    # Check and process args
    try:
        clan, zone = await check_damage_args(ctx, arg)
    except:
        return

    # Check zone range
    if zone not in range(1, 7):
        await ctx.channel.send(f"Zone provided **{zone}** is not a number 1-6. Who are you, Vortex?")
        return

    # Query for player
    query_player = db_session.query(Player).filter(
        Player.discord_id == ctx.author.id).first()
    # Add player to database with display_name if it's the first time
    if not query_player:
        db_session.add(Player(discord_id=ctx.author.id,
                       display_name=ctx.author.display_name))
        db_session.commit()

    # Queue for query damage
    query_damage = db_session.query(Damage).filter(Damage.player_id == ctx.author.id).filter(
        Damage.clan == clan).first()

    # Add damage to db
    if not query_damage:
        db_session.add(Damage(player_id=ctx.author.id,
                       warzone=zone, clan=clan))
    else:
        # Update Warzone in DB
        db_session.query(Damage).filter(Damage.player_id == ctx.author.id).filter(Damage.clan == clan).update(
            {Damage.warzone: zone})

    db_session.commit()
    await ctx.channel.send(f"**{ctx.author.display_name}** has set their highest warzone for **{clan}** as WZ{zone}")


@bot.command(brief="!setdamage [gg/hg] [damage(100-100000)] (Sets your average warzone damage per hit)")
async def setdamage(ctx, *, arg=''):
    # Check and process args
    try:
        clan, damage = await check_damage_args(ctx, arg)
    except:
        return

    # Check zone range
    if damage not in range(100, 100000):
        await ctx.channel.send(f"Damage provided **{damage}** is not between 100 - 100,000. Try again.")
        return

    # Query for player
    query_player = db_session.query(Player).filter(
        Player.discord_id == ctx.author.id).first()
    # Add player to database with display_name if it's the first time
    if not query_player:
        db_session.add(Player(discord_id=ctx.author.id,
                       display_name=ctx.author.display_name))
        db_session.commit()

    # Queue for query damage
    query_damage = db_session.query(Damage).filter(Damage.player_id == ctx.author.id).filter(
        Damage.clan == clan).first()

    # Add damage to db
    if not query_damage:
        db_session.add(Damage(player_id=ctx.author.id,
                       damage=damage, clan=clan))
    else:
        # Update Warzone in DB
        db_session.query(Damage).filter(Damage.player_id == ctx.author.id).filter(Damage.clan == clan).update(
            {Damage.damage: damage})

    db_session.commit()
    await ctx.channel.send(f"**{ctx.author.display_name}** has set their average damage per hit for **{clan}** as {damage}")


@bot.command(brief="!showdamage [gg/hg]")
async def showdamage(ctx, *, arg="0"):
    # Check clans input
    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel
    # if gg or hg not provided, same as check input but without material
    if arg == '' or arg.split()[0].upper() not in clans:
        await ctx.channel.send(f"Which clan? GG or HG?")
        msg = await bot.wait_for("message", check=check)
        if msg.content.lower() != "gg" and msg.content.lower() != "hg":
            await ctx.channel.send(f"That's not HG or GG moron. Try the command again")
            return
        else:
            clan = msg.content.upper()
    else:
        clan = arg.split()[0].upper()

    # Format Output
    embed = discord.Embed(
        title="Warzone Damage List",
        color=discord.Color.gold(),
    )

    zone = 6

    while zone > 0:
        # Get damage from clan
        query_damage = db_session.query(Damage).filter(
            Damage.clan == clan, Damage.warzone >= zone)
        members = len(query_damage.all())

        # Set variables for members and damage total
        if members != 0:
            damage_total = db_session.query(sqlalchemy.func.sum(Damage.damage).label("sum")).filter(
                Damage.clan == clan, Damage.warzone >= zone)[0].sum
        else:
            damage_total = 0

        embed.add_field(
            name=f"**Warzone {zone}:**",
            value=f"- **{members}** people can reach\n- **{damage_total}** Total Damage\n- **{damage_total * 10}** Damage/100 CE\n- **{int(damage_total * 14.4)}** Damage/Day",
            inline=False,
        )
        zone = zone - 1

    await ctx.channel.send(embed=embed)


@bot.command(brief="!listdamage [gg/hg]")
async def listdamage(ctx, *, arg="0"):
    # Check clans input
    def check(msg):
        return msg.author == ctx.author and msg.channel == ctx.channel
    # if gg or hg not provided, same as check input but without material
    if arg == '' or arg.split()[0].upper() not in clans:
        await ctx.channel.send(f"Which clan? GG or HG?")
        msg = await bot.wait_for("message", check=check)
        if msg.content.lower() != "gg" and msg.content.lower() != "hg":
            await ctx.channel.send(f"That's not HG or GG moron. Try the command again")
            return
        else:
            clan = msg.content.upper()
    else:
        clan = arg.split()[0].upper()

    # Format Output
    text = ""

    # Get damage from clan
    query_damage = db_session.query(Damage).filter(
        Damage.clan == clan).order_by(Damage.damage.desc())

    for entry in query_damage:
        query_player = db_session.query(Player).filter(
            Player.discord_id == entry.player_id).first()
        text = text + \
            f"- **{query_player.display_name}:** {entry.damage} Damage, WZ{entry.warzone}\n"

    embed = discord.Embed(
        title="Warzone Damage Ordered High to Low",
        description=text,
        color=discord.Color.gold(),
    )

    await ctx.channel.send(embed=embed)


@bot.command(brief="!rename (rename you in queue)")
async def rename(ctx, *, arg=''):
    player = db_session.query(Player).filter(
        Player.discord_id == ctx.author.id).first()
    old_name = player.display_name
    if arg == '':
        new_name = ctx.author.display_name
    else:
        new_name = arg
    player.display_name = new_name
    db_session.commit()
    await ctx.channel.send(f'{old_name} is now {new_name}')

# Run bot
# mytask.start()
# veins_gg.start()
# targets_gg.start()
# targets_hg.start()
bot.run(os.getenv("DISCORD_TOKEN"))

#For Discord.py 2.0+
# async def main():
#     async with bot:
#         print("hello")
#         await bot.add_cog(reactions(bot))
#         print("hello")
#         await bot.start(os.getenv("DISCORD_TOKEN"))
#         print("hello")

# asyncio.run(main())
