-- -- Adds item table
-- CREATE TABLE IF NOT EXISTS public.items (
-- 	id bigint DEFAULT nextval('items_sampleid_seq'::regclass) NOT NULL,
-- 	name character varying,
-- 	url character varying,
-- 	shop_qty integer DEFAULT 0 NOT NULL,
-- 	shop_val integer DEFAULT 0 NOT NULL,
-- 	PRIMARY KEY(id)
-- );

-- Adds column to material tables
ALTER TABLE public.material ADD COLUMN we_val integer NULL DEFAULT NULL;
ALTER TABLE public.material ADD COLUMN shop_qty integer NOT NULL DEFAULT '0';
ALTER TABLE public.material ADD COLUMN shop_val integer NOT NULL DEFAULT '0';
UPDATE public.material SET shop_val = CEILING(CAST(we_val AS float) / CAST(2 as float));

-- Update Warzone Table to add valuation
ALTER TABLE public.warzone ADD COLUMN valuation numeric NULL DEFAULT NULL;

-- Fill values into material table
UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 1;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 2;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 3;

UPDATE
	public.material
SET
	we_val = 20
WHERE
	id = 5;

UPDATE
	public.material
SET
	we_val = 167
WHERE
	id = 6;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 7;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 8;

UPDATE
	public.material
SET
	we_val = 20
WHERE
	id = 9;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 10;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 11;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 12;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 13;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 14;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 15;

UPDATE
	public.material
SET
	we_val = 1250
WHERE
	id = 16;

UPDATE
	public.material
SET
	we_val = 1400
WHERE
	id = 17;

UPDATE
	public.material
SET
	we_val = 42
WHERE
	id = 18;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 19;

UPDATE
	public.material
SET
	we_val = 17
WHERE
	id = 20;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 21;

UPDATE
	public.material
SET
	we_val = 6
WHERE
	id = 23;

UPDATE
	public.material
SET
	we_val = 13
WHERE
	id = 24;

UPDATE
	public.material
SET
	we_val = 1200
WHERE
	id = 25;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 26;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 27;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 28;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 29;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 30;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 31;

UPDATE
	public.material
SET
	we_val = 7
WHERE
	id = 32;

UPDATE
	public.material
SET
	we_val = 7
WHERE
	id = 33;

UPDATE
	public.material
SET
	we_val = 17
WHERE
	id = 34;

UPDATE
	public.material
SET
	we_val = 17
WHERE
	id = 35;

UPDATE
	public.material
SET
	we_val = 625
WHERE
	id = 36;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 37;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 38;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 39;

UPDATE
	public.material
SET
	we_val = 14
WHERE
	id = 40;

UPDATE
	public.material
SET
	we_val = 625
WHERE
	id = 41;

UPDATE
	public.material
SET
	we_val = 333
WHERE
	id = 42;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 43;

UPDATE
	public.material
SET
	we_val = 17
WHERE
	id = 44;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 45;

UPDATE
	public.material
SET
	we_val = 416
WHERE
	id = 47;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 48;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 49;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 50;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 51;

UPDATE
	public.material
SET
	we_val = 6
WHERE
	id = 52;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 53;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 54;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 55;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 56;

UPDATE
	public.material
SET
	we_val = 2000
WHERE
	id = 58;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 59;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 60;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 61;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 62;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 63;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 64;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 66;

UPDATE
	public.material
SET
	we_val = 20
WHERE
	id = 67;

UPDATE
	public.material
SET
	we_val = 14
WHERE
	id = 68;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 70;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 71;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 72;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 73;

UPDATE
	public.material
SET
	we_val = 8
WHERE
	id = 74;

UPDATE
	public.material
SET
	we_val = 1667
WHERE
	id = 75;

UPDATE
	public.material
SET
	we_val = 14
WHERE
	id = 76;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 77;

UPDATE
	public.material
SET
	we_val = 3000
WHERE
	id = 78;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 79;

UPDATE
	public.material
SET
	we_val = 1100
WHERE
	id = 80;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 81;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 82;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 83;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 84;

UPDATE
	public.material
SET
	we_val = 750
WHERE
	id = 85;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 86;

UPDATE
	public.material
SET
	we_val = 833
WHERE
	id = 87;

UPDATE
	public.material
SET
	we_val = 833
WHERE
	id = 88;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 89;

UPDATE
	public.material
SET
	we_val = 1250
WHERE
	id = 90;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 91;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 92;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 93;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 94;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 95;

UPDATE
	public.material
SET
	we_val = 83
WHERE
	id = 96;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 97;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 98;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 99;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 100;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 101;

UPDATE
	public.material
SET
	we_val = 150
WHERE
	id = 103;

UPDATE
	public.material
SET
	we_val = 6
WHERE
	id = 104;

UPDATE
	public.material
SET
	we_val = 125
WHERE
	id = 106;

UPDATE
	public.material
SET
	we_val = 1667
WHERE
	id = 107;

UPDATE
	public.material
SET
	we_val = 33
WHERE
	id = 108;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 109;

UPDATE
	public.material
SET
	we_val = 333
WHERE
	id = 110;

UPDATE
	public.material
SET
	we_val = 1200
WHERE
	id = 111;

UPDATE
	public.material
SET
	we_val = 1400
WHERE
	id = 112;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 113;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 114;

UPDATE
	public.material
SET
	we_val = 13
WHERE
	id = 115;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 116;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 117;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 119;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 120;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 121;

UPDATE
	public.material
SET
	we_val = 125
WHERE
	id = 122;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 123;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 124;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 125;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 126;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 127;

UPDATE
	public.material
SET
	we_val = 150
WHERE
	id = 128;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 129;

UPDATE
	public.material
SET
	we_val = 11
WHERE
	id = 131;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 132;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 133;

UPDATE
	public.material
SET
	we_val = 420
WHERE
	id = 134;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 135;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 136;

UPDATE
	public.material
SET
	we_val = 10
WHERE
	id = 137;

UPDATE
	public.material
SET
	we_val = 8
WHERE
	id = 138;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 139;

UPDATE
	public.material
SET
	we_val = 200
WHERE
	id = 140;

UPDATE
	public.material
SET
	we_val = 300
WHERE
	id = 141;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 142;

UPDATE
	public.material
SET
	we_val = 400
WHERE
	id = 144;

UPDATE
	public.material
SET
	we_val = 250
WHERE
	id = 145;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 147;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 148;

UPDATE
	public.material
SET
	we_val = 50
WHERE
	id = 149;

UPDATE
	public.material
SET
	we_val = 300
WHERE
	id = 150;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 152;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 153;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 154;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 4;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 22;

UPDATE
	public.material
SET
	we_val = 100
WHERE
	id = 46;

UPDATE
	public.material
SET
	we_val = 1250
WHERE
	id = 57;

UPDATE
	public.material
SET
	we_val = 2400
WHERE
	id = 65;

UPDATE
	public.material
SET
	we_val = 125
WHERE
	id = 69;

UPDATE
	public.material
SET
	we_val = 20
WHERE
	id = 73;

UPDATE
	public.material
SET
	we_val = 25
WHERE
	id = 102;

UPDATE
	public.material
SET
	we_val = 1400
WHERE
	id = 105;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 118;

UPDATE
	public.material
SET
	we_val = 1500
WHERE
	id = 130;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 143;

UPDATE
	public.material
SET
	we_val = 500
WHERE
	id = 146;

UPDATE
	public.material
SET
	we_val = 1000
WHERE
	id = 151;

-- Insert items into material table
INSERT INTO public.material (name, url, we_val, shop_qty, shop_val) VALUES
('Bezoar', '', 100, 0, 50),
('Essence if Origin', '', 100, 0, 50),
('Hyuuga Blood', '', 50, 0, 25),
('Ice Shard', '', 100, 0, 50),
('Rune Ward', '', 100, 0, 50),
('Serpent Burst', '', NULL, 0, 200),
('Azoth', '', NULL, 0, 50),
('Lucky Talisman', '', NULL, 2, 100),
('Fire Shard', '', NULL, 3, 75),
('Dirty Dice', '', NULL, 6, 20),
('Reroll Counter', '', NULL, 1, 400),
('Sparkless Wind Shard', '', NULL, 4, 15),
('Undying Mask', '', NULL, 1, 93),
('Sparkless Lightning Shard', '', NULL, 2, 15),
('Bronze Token', '', NULL, 5, 25),
('Earth Shard', '', NULL, 1, 75),
('Sparkless Earth Shard', '', NULL, 2, 15),
('Genjutsu Pill', '', NULL, 7, 45),
('Thunder God Kunai', '', NULL, 1, 56),
('Vajra', '', NULL, 1, 200),
('Prototype Chakra Gauntlet', '', NULL, 1, 20),
('Chimera Gloves', '', NULL, 4, 75),
('Ghost Gloves', '', NULL, 3, 95),
('Anbu Mask', '', NULL, 2, 133),
('Sparkless Fire Shard', '', NULL, 2, 15),
('Flak Jacket', '', NULL, 3, 25),
('Tortoise Carapace', '', NULL, 1, 200),
('Detonating Clay', '', NULL, 1, 125),
('Prototype Chakra Helmet', '', NULL, 4, 50),
('Bone Pendant', '', NULL, 1, 550),
('Weapons Belt', '', NULL, 1, 19),
('Illusionary Entrance', '', NULL, 2, 200),
('Lightning Shard', '', NULL, 2, 75),
('Snakeskin Slippers', '', NULL, 1, 75),
('Thunder Discharge', '', NULL, 1, 200),
('Thunder Rod', '', NULL, 2, 103),
('Harden', '', NULL, 2, 200),
('Poison Puppet', '', NULL, 1, 113),
('Illusion Pendant', '', NULL, 3, 55),
('Sparkless Water Shard', '', NULL, 1, 15),
('Serpent Thrust', '', NULL, 1, 200),
('Turtle Defense', '', NULL, 1, 200),
('Artificial Rasengan', '', NULL, 2, 400),
('Snake Stamina', '', NULL, 2, 200),
('Shuriken', '', NULL, 1, 15),
('Chimera Gauntlets', '', NULL, 1, 206),
('Hero Water', '', NULL, 1, 53),
('Chimera Burst', '', NULL, 1, 200),
('Tanto', '', NULL, 2, 38),
('Water Shard', '', NULL, 1, 75),
('Blight Rod', '', NULL, 2, 108),
('Poison Coating', '', NULL, 1, 200),
('Magnetic Field', '', NULL, 1, 400),
('Weapons Quiver', '', NULL, 1, 94),
('Spoiled Soldier Pill', '', NULL, 1, 8),
('Jade Awl', '', NULL, 1, 250),
('Twisted Mask', '', NULL, 1, 54),
('Frozen Havoc', '', NULL, 1, 200),
('Sour Hero Water', '', NULL, 0, 11),
('Dire Howl', '', NULL, 0, 200),
('Charm of Protection', '', NULL, 0, 50),
('Fuma Shuriken', '', NULL, 0, 49),
('Anbu Paint', '', NULL, 0, 10),
('Soldier Pill', '', NULL, 0, 34);

-- IMPORTANT: Alter table rename material to equipment
ALTER TABLE material RENAME TO equipment;
ALTER TABLE public.queue RENAME COLUMN material_id TO equipment_id;
ALTER INDEX material_pkey RENAME TO equipment_pkey;

-- Add table location
-- CREATE TABLE IF NOT EXISTS public.item_location (
-- 	id bigint DEFAULT nextval('item_location_sampleid_seq'::regclass) NOT NULL,
-- 	name character varying NOT NULL,
-- 	"location" character varying NOT NULL,
-- 	chance numeric NOT NULL,
-- 	PRIMARY KEY(id)
-- );

-- COMMIT;


-- ALTER TABLE public.item_location ADD COLUMN difficulty character varying NULL;
-- ALTER TABLE public.item_location ADD COLUMN note character varying NULL;

-- Remove Equipment duplicates
DELETE FROM public.equipment WHERE id = 117;
DELETE FROM public.equipment WHERE id = 3;
DELETE FROM public.equipment WHERE id = 61;
ALTER TABLE equipment ADD UNIQUE (name);

-- Insert for findable LW's into equipment
INSERT INTO public.equipment (name, url, we_val, shop_qty, shop_val) VALUES
('Arcane Grimoire', '', NULL, 0, 1500),
('Bashosen', '', NULL, 0, 1500),
('Benihisago', '', NULL, 0, 1500),
('Chakra Fruit', '', NULL, 0, 1500),
('Chimera Cloak', '', NULL, 0, 1500),
('Crustacean Knuckles', '', NULL, 0, 1500),
('Cursed Chakram', '', NULL, 0, 1500),
('Cursed Scroll', '', NULL, 0, 1500),
('Dark Orb', '', NULL, 0, 1500),
('Demonic Flute', '', NULL, 0, 1500),
('Eye Scope', '', NULL, 0, 1500),
('Firestorm Staff', '', NULL, 0, 1500),
('Ice Brand', '', NULL, 0, 1500),
('Kabutowari', '', NULL, 0, 1500),
('Kiba', '', NULL, 0, 1500),
('Kohaku no Johei', '', NULL, 0, 1500),
('Kokinjo', '', NULL, 0, 1500),
('Kubikiribocho', '', NULL, 0, 1500),
('Kurosawa', '', NULL, 0, 1500),
('Kusanagi', '', NULL, 0, 1500),
('Mage Masher', '', NULL, 0, 1500),
('Molten Hammer', '', NULL, 0, 1500),
('Nuibari', '', NULL, 0, 1500),
('Nunoboko', '', NULL, 0, 1500),
('Occult Grimoire', '', NULL, 0, 1500),
('Oxygen Mask', '', NULL, 0, 1500),
('Rhi Knuckles', '', NULL, 0, 1500),
('Samehada', '', NULL, 0, 1500),
('Sealing Sword Scroll', '', NULL, 0, 1500),
('Shibuki', '', NULL, 0, 1500),
('Shichiseiken', '', NULL, 0, 1500),
('Sinister Shuriken', '', NULL, 0, 1500),
('Skeletal Staff', '', NULL, 0, 1500),
('Spore Gauntlet', '', NULL, 0, 1500),
('Steam Armor', '', NULL, 0, 1500),
('Stone of Gelel', '', NULL, 0, 1500),
('Totsuka', '', NULL, 0, 1500),
('White Light Blade', '', NULL, 0, 1500),
('Yagyu Shuriken', '', NULL, 0, 1500),
('Yata Mirror', '', NULL, 0, 1500);


-- Add LW's into equipment db
INSERT INTO public.equipment (name, url, we_val, shop_qty, shop_val) VALUES
('Abaddons Armory', '', NULL, 0, 1500),
('Arachnes Arbalest', '', NULL, 0, 1500),
('Arctic Battlearmor', '', NULL, 0, 1500),
('Argent White Sword', '', NULL, 0, 1500),
('Artificial Bloodline Core', '', NULL, 0, 1500),
('Bahamuts Mane', '', NULL, 0, 1500),
('Blades of Kali', '', NULL, 0, 1500),
('Bloodsealed Talisman', '', NULL, 0, 1500),
('Bone Armor', '', NULL, 0, 1500),
('Bone Pendant', '', NULL, 0, 1500),
('Chakra Exosuit', '', NULL, 0, 1500),
('Chakra Shield', '', NULL, 0, 1500),
('Cursed Ring', '', NULL, 0, 1500),
('Daedalus Cuirass', '', NULL, 0, 1500),
('Divine Khakkara', '', NULL, 0, 1500),
('Dracolichs Phylactery', '', NULL, 0, 1500),
('Electrum Coin', '', NULL, 0, 1500),
('Fafnirs Scales', '', NULL, 0, 1500),
('Fenrirs Fang', '', NULL, 0, 1500),
('Ghost Cape', '', NULL, 0, 1500),
('Gungnir', '', NULL, 0, 1500),
('Heart of Anbu', '', NULL, 0, 1500),
('Heart of Cloud', '', NULL, 0, 1500),
('Heart of Leaf', '', NULL, 0, 1500),
('Heart of Mist', '', NULL, 0, 1500),
('Heart of Neko', '', NULL, 0, 1500),
('Heart of Rain', '', NULL, 0, 1500),
('Heart of Root', '', NULL, 0, 1500),
('Heart of Sand', '', NULL, 0, 1500),
('Heart of Sound', '', NULL, 0, 1500),
('Heart of Stone', '', NULL, 0, 1500),
('Heretical God Seed', '', NULL, 0, 1500),
('Hugins Talon', '', NULL, 0, 1500),
('Icarus Wings', '', NULL, 0, 1500),
('Inksealed Talisman', '', NULL, 0, 1500),
('Jade Emeperors Pagoda', '', NULL, 0, 1500),
('Jibrils Garb', '', NULL, 0, 1500),
('Jingu Bang', '', NULL, 0, 1500),
('Jörmungandrs Skull', '', NULL, 0, 1500),
('Krakens Tentacle', '', NULL, 0, 1500),
('Leviathans Fin', '', NULL, 0, 1500),
('Lichs Vestment', '', NULL, 0, 1500),
('Mangekyo Seal', '', NULL, 0, 1500),
('MBF-02', '', NULL, 0, 1500),
('Midgard Mail', '', NULL, 0, 1500),
('Minotaurs Horn', '', NULL, 0, 1500),
('Mjölnir', '', NULL, 0, 1500),
('Munins Talon', '', NULL, 0, 1500),
('Nibelungs Ring', '', NULL, 0, 1500),
('Niddhoggrs Claw', '', NULL, 0, 1500),
('Origin Bone Scepter', '', NULL, 0, 1500),
('Pandemonium Flute', '', NULL, 0, 1500),
('Piezoelectric Clock', '', NULL, 0, 1500),
('Python Robe', '', NULL, 0, 1500),
('Rhinoceros Shell', '', NULL, 0, 1500),
('Scrolls of Sutra', '', NULL, 0, 1500),
('Skull Helmet', '', NULL, 0, 1500),
('Snakeskin Seal', '', NULL, 0, 1500),
('Snakeskin Shako', '', NULL, 0, 1500),
('Sunpierce Sword', '', NULL, 0, 1500),
('Tiamats Tiara', '', NULL, 0, 1500),
('Valhalla Helm', '', NULL, 0, 1500),
('Warg Talisman', '', NULL, 0, 1500),
('Websealed Talisman', '', NULL, 0, 1500),
('Winged Mask', '', NULL, 0, 1500),
('Xiantians Divine Axe', '', NULL, 0, 1500);


-- Add more equipment into table
INSERT INTO public.equipment (name, url, we_val, shop_qty, shop_val) VALUES
('Alkahest', '', NULL, 0, 1),
('Panacea', '', NULL, 0, 25),
('Dao Dragon Pill', '', NULL, 0, 1500),
('Monkey Tail', '', NULL, 0, 1500),
('Pyhton Longevity Serum', '', NULL, 0, 1500),
('Zephyr Leaf', '', 100, 0, 50),
('Monkey King Fur', '', 400, 0, 200),
('Divine Iron', '', NULL, 0, 1500),
('Reincarnation Ether', '', 3000, 0, 1500),
('Czarwood', '', NULL, 0, 1500),
('Level 8 Giant Spider', '', NULL, 0, 1500),
('Level 8 Wolf', '', NULL, 0, 1500),
('Level 20 Hanabi', '', NULL, 0, 1500),
('Level 20 Hiashi', '', NULL, 0, 1500),
('Level 28 Neji', '', NULL, 0, 1500),
('Level 33 Hinata', '', NULL, 0, 1500),
('Level 25 Hashirama', '', NULL, 0, 1500),
('Level 10 Mangekyou Sharingan', '', NULL, 0, 1500),
('Level 10 Crow', '', NULL, 0, 1500),
('Level 10 Genjutsu Crow', '', NULL, 0, 1500),
('Level 20 Kakashi', '', NULL, 0, 1500),
('Level 27 Shisui', '', NULL, 0, 1500),
('Level 28 Sasuke', '', NULL, 0, 1500),
('Level 33 Itachi', '', NULL, 0, 1500),
('Level 37 Obito', '', NULL, 0, 1500),
('Level 43 Tobi', '', NULL, 0, 1500),
('Level 9 Lesser Cursed Seal BL', '', NULL, 0, 1500),
('Level 7 Chimera', '', NULL, 0, 1500),
('Level 5 Giant Snake', '', NULL, 0, 1500),
('Level 5 Wolf', '', NULL, 0, 1500),
('Level 8 Life Dragon', '', NULL, 0, 1500),
('Level 10 Chomei', '', NULL, 0, 1500),
('Level 10 Kokuo', '', NULL, 0, 1500),
('Level 10 Shukaku', '', NULL, 0, 1500),
('Level 10 Son Goku', '', NULL, 0, 1500),
('Level 10 Mecha Mukade', '', NULL, 0, 1500),
('Level 10 Umibozu', '', NULL, 0, 1500),
('Level 10 Saiken', '', NULL, 0, 1500),
('Level 20 Sakumo', '', NULL, 0, 1500),
('Level 20 A', '', NULL, 0, 1500),
('Level 20 Hiruzen', '', NULL, 0, 1500),
('Level 20 Mei', '', NULL, 0, 1500),
('Level 20 Nekomata', '', NULL, 0, 1500),
('Level 20 Hanzo', '', NULL, 0, 1500),
('Level 20 Danzo', '', NULL, 0, 1500),
('Level 20 Chiyo', '', NULL, 0, 1500),
('Level 20 Orochimaru', '', NULL, 0, 1500),
('Level 20 Onoki', '', NULL, 0, 1500);


-- Insert all craftable items into db
INSERT INTO public.equipment (name, url, we_val, shop_qty, shop_val) VALUES
('Adamantine Kunai', '', NULL, 0, 1500),
('Anbu Mask', '', NULL, 0, 1500),
('Anbu Vest', '', NULL, 0, 1500),
('Bionic Shinobi Gauntlet', '', NULL, 0, 1500),
('Blight Rod', '', NULL, 0, 1500),
('Bronze Token', '', NULL, 0, 1500),
('Chimera Gauntlets', '', NULL, 0, 1500),
('Chimera Gloves', '', NULL, 0, 1500),
('Cold Kunai', '', NULL, 0, 1500),
('Detonating Clay', '', NULL, 0, 1500),
('Dirty Dice', '', NULL, 0, 1500),
('Elemental Puppet', '', NULL, 0, 1500),
('Flak Jacket', '', NULL, 0, 1500),
('Ghost Gloves', '', NULL, 0, 1500),
('Green Jumpsuit', '', NULL, 0, 1500),
('Illusion Pendant', '', NULL, 0, 1500),
('Jade Awl', '', NULL, 0, 1500),
('Jade Dagger', '', NULL, 0, 1500),
('Lucky Talisman', '', NULL, 0, 1500),
('Mecha Puppet', '', NULL, 0, 1500),
('Poison Puppet', '', NULL, 0, 1500),
('Poison Shuriken', '', NULL, 0, 1500),
('Prototype Chakra Armor', '', NULL, 0, 1500),
('Prototype Chakra Gauntlet', '', NULL, 0, 1500),
('Prototype Chakra Helmet', '', NULL, 0, 1500),
('Serpent Armor', '', NULL, 0, 1500),
('Serpent Pike', '', NULL, 0, 1500),
('Shuriken', '', NULL, 0, 1500),
('Snakeskin Slippers', '', NULL, 0, 1500),
('Snakeskin Suit', '', NULL, 0, 1500),
('Tainted Shuriken', '', NULL, 0, 1500),
('Thunder Rod', '', NULL, 0, 1500),
('Tortoise Mask', '', NULL, 0, 1500),
('Turtle Shield', '', NULL, 0, 1500),
('Twisted Mask', '', NULL, 0, 1500),
('Undying Mask', '', NULL, 0, 1500),
('Vajra', '', NULL, 0, 1500),
('Lightning Shard', '', NULL, 0, 1500),
('Wolf Mantle', '', NULL, 0, 1500);








-- Add fk for location table
ALTER TABLE IF EXISTS public.item_location
	ADD CONSTRAINT fk_name_loc_equip
	FOREIGN KEY (name)
	REFERENCES public.equipment (name);

INSERT INTO public.item_location (name, location, chance, difficulty, note) VALUES
('Adamantine Bone', 'Immortal Beast Cavern - Giant Multi-Headed Dog', 15, NULL, NULL),
('Aero Stone', 'Forbidden Temple: Outer - Young and Temari', 2, NULL, NULL),
('Anbu Paint', 'ANBU Hideout 3/7', 25, NULL, NULL),
('Ancient Blood', 'Blood Prison 11/13', 3, NULL, NULL),
('Ancient Coin', 'Akatsuki Temple 8/10', .50, NULL, 'Increased Drop on Hard/Impo'),
('Antique Mask', 'Blood Prison 7/13', 25, NULL, NULL),
('Artificial Chakra Core', 'Snow Country 7/7', 8, NULL, NULL),
('Ascension Ore', 'Forbidden Temple: Inner - Madara', 12, NULL, NULL),
('Berserker Enzyme', 'Frozen Island 5/10', 10, NULL, NULL),
('Bezoar', 'Island Turtle - Hachibis Hut 6/6', 3, NULL, NULL),
('Black Receiver', 'Island Turtle - Nagato', 21, NULL, NULL),
('Black Threads', 'Akatsuki Temple 8/10', 36, NULL, NULL),
('Blood Vessel', 'Forbidden Temple: Outer - Karin', 18, NULL, NULL),
('Bone', 'Forest of Death 2/8', 80, NULL, NULL),
('Bronze Coin', 'Hot Water Village 13/13', 40, NULL, NULL),
('Celestial Dye', 'Ruins of Roran - Hard Path Sara', 3, NULL, NULL),
('Celestial Dye', 'Kaguya Castle 1/6', 6, NULL, 'Increases 1% each Difficulty'),
('Chakra Plate', 'Snow Country 4/7', 50, NULL, NULL),
('Chimera DNA', 'Mount Shumisen 5/12', 1, NULL, NULL),
('Chimera Heart', 'Mount Shumisen 11/12', 2, NULL, 'Increases 0.25% each Difficulty'),
('Clay', 'Ruins of Roran 3/13', 70, NULL, NULL),
('Clear Water', 'Hot Water Village 9/13', 60, NULL, NULL),
('Cracked Anbu Mask', 'Frozen Island 8/10', 24, NULL, NULL),
('Cracked Anbu Mask', 'ANBU Hideout 4/7', 35, NULL, NULL),
('Crow Feather', 'ANBU Hideout 6/7', 30, NULL, NULL),
('Curse Mark Pattern', 'Forest of Death 1/8', 8, NULL, NULL),
('Cursed Feather', 'Moryo Shrine 13/13', 20, NULL, NULL),
('Dark Chakra Eel', 'Moryo Shrine 2/13', 35, NULL, NULL),
('Deathly Dust', 'The Sealed World 8/12', 1.5, NULL, NULL),
('Demon Wing', 'Blood Prison 13/13', 5, NULL, NULL),
('Dharma Wheel', 'Hidden Leaf - Fire Monastery 1/6', 10, 'Extreme', NULL),
('Dharma Wheel', 'Kamuis Dimension', 15, 'Extreme', NULL),
('Dice', 'Blood Prison 4/13', 30, NULL, NULL),
('Dojutsu Catalyst', 'Kamuis Dimension 5/10', 1, NULL, 'Increases 1% each Difficulty'),
('Dragon Blood', 'Forbidden Temple: Center - Kabuto', 6, 'Hard', NULL),
('Dragon Soul', 'Ryuichi Cave 10/10', 1.75, NULL, 'Increases 0.25% each Difficulty'),
('Essence of Darkness', 'Frozen Island 7/10', 4, NULL, NULL),
('Essence of Darkness', 'Mount Shumisen 12/12', 3, NULL, NULL),
('Essence of Origin', 'Forbidden Temple: Inner - Naruto', 6, 'Forbidden', NULL),
('Essence of Smoke', 'Cafe Gero Gero - Gamabunta', 12, NULL, NULL),
('Essence of Swiftness', 'Mount Shumisen 12/12', 3, NULL, NULL),
('Essence of Swiftness', 'Island Turtle - Hachibis Hut 4/6', 6, NULL, NULL),
('Fish Scales', 'Hot Water Village 1/13', 80, NULL, NULL),
('Ghost Spirit', 'Hot Water Village 5/13', 25, NULL, NULL),
('Granite', 'Moryo Shrine 1/13', 15, NULL, NULL),
('Gundam Core', 'Ruins of Roran 7/13', 0.40, NULL, 'Increased drop chance on Medium and Extreme'),
('Hashiramas DNA', 'Mountains Graveyard 5/10', 0.20, NULL, 'Increases to 0.40% on Medium'),
('Holy Water', 'Valley of the End 1/4', 2.80, NULL, NULL),
('Holy Water', 'Mount Shumisen 7/12 - Hardpath (Stolen Hero Water)', 2, NULL, NULL),
('Hyuuga Blood', 'Forbidden Temple: Outer - Young and Adorable', 4, NULL, 'Increases to 6% on easy'),
('Hyuuga Blood', 'Forbidden Temple: Outer - Young and Shy', 8, 'Medium', 'Increases to 10% on hard'),
('Hyuuga Blood', 'Forbidden Temple: Outer - Young and Neji', 12, 'Extreme', NULL),
('Ice Horn', 'Frozen Island 1/10', 40, NULL, NULL),
('Ice Shard', 'Frozen Island - Hard Path Haku', 5, NULL, NULL),
('Indras Chakra', 'Forbidden Temple: Center - Madara', 0.25, 'Hard', NULL),
('Ink Bottle', 'Forbidden Temple: Outer - Young and Sai', 30, NULL, NULL),
('Iron', 'Snow Country 5/7', 28, NULL, NULL),
('Iron', 'Land of Iron 2/11', 45, NULL, NULL),
('Ivory', 'Island Turtle - Hachibis Hut 5/6', 24, NULL, NULL),
('Jade', 'Hot Water Village 7/13', 10, 'Medium', NULL),
('Jutsu Scroll', 'Forbidden Temple: Outer  - Young and Tenten', 60, NULL, NULL),
('Lava', 'Moryo Shrine 5/13', 25, NULL, NULL),
('Leather', 'Frozen Island 2/10', 40, NULL, NULL),
('Lion Fur', 'Mount Shumisen 2/12', 35, NULL, NULL),
('Mercury', 'Forbidden Temple: Outer - Young and Miserable', 24, NULL, NULL),
('Mind Awakening Pill', 'Land of Iron 6/11', 14, NULL, NULL),
('Mind Awakening Pill', 'Forbidden Temple: Outer - Young and Ambitious', 12, NULL, NULL),
('Muga Silk', 'Snow Country - Hard Path Koyuki', 4, NULL, NULL),
('Muga Silk', 'Ruins of Roran 2/13', 5, NULL, NULL),
('Nature Transformation Chakra', 'Land of Iron 10/11', 8, NULL, NULL),
('Nature Transformation Chakra', 'Forbidden Temple: Outer - Young and Yamato', 6, NULL, NULL),
('Octopus Arm', 'Fort Jinchuuriki - Killer Bee, Blue B', 0.1, NULL, '100% on first drop'),
('Otsutsuki Cells', 'Kaguyas Castle 3/6', 2, NULL, 'Increases by 1% each Difficulty'),
('Poison Vial', 'Frozen Island 3/10', 12, NULL, NULL),
('Puppet Part', 'Ruins of Roran 13/13', 50, NULL, NULL),
('Quartz Crystal', 'Ruins of Roran 8/13', 5, NULL, NULL),
('Quicklime', 'Moryo Shrine 6/13', 10, NULL, NULL),
('Red Sand', 'Ruins of Roran 4/13', 50, NULL, NULL),
('Robot Parts', 'Ruins of Roran 6/13', 15, NULL, NULL),
('Roran Coin', 'Ruins of Roran 5/13', 10, NULL, NULL),
('Ruby', 'Forbidden Temple: Outer - Young and Sakura', 6, NULL, NULL),
('Rune Ward', 'Snow Country 6/7', 16, 'Medium', NULL),
('Rusty Spring', 'Ruins of Roran 1/13', 80, NULL, NULL),
('Salamander Venom', 'Land of Iron 7/11', 4.5, NULL, 'Increases 0.5% each Difficulty'),
('Serpent Horn', 'Moryo Shrine 8/13', 15, NULL, NULL),
('Serpent Scales', 'Moryo Shrine 4/13', 20, NULL, NULL),
('Serpent Soul', 'Moryo Shrine 12/13', 1.5, NULL, NULL),
('Snake Eye', 'Forest of Death 3/8', 4, NULL, NULL),
('Snake Eye', 'Ryuichi Cave 5/10', 22, NULL, NULL),
('Snake Serum', 'Forest of Death 8/8', 0.8, NULL, NULL),
('Snake Serum', 'Ryuichi Cave 8/10', 2, NULL, 'Increases 0.25% each Difficulty'),
('Snakeskin', 'Forest of Death 3/8', 16, NULL, NULL),
('Snakeskin', 'Forest of Death 6/8', 32, NULL, NULL),
('Snakeskin', 'Ryuichi Cave 6/10', 75, NULL, NULL),
('Soil Stone', 'Forbidden Temple: Outer - Young and Hated', 2.4, NULL, NULL),
('Spider Web', 'Ruins of Roran 11/13', 50, NULL, NULL),
('Steam Stone', 'Hot Water Village 3/13', 2, NULL, NULL),
('Stem Cell', 'Forbidden Temple: Outer - Young and Kabuto', 15, NULL, NULL),
('Stem Cell', 'Frozen Island 4/11', 7, 'Medium', NULL),
('Storage Cell Key', 'Island Turtle - Hachibis Hut', 100, NULL, 'Varies'),
('Susanoo Residue', 'Mountains Graveyard 6/10', 1.5, NULL, 'Up to 3.5% on later difficulties'),
('Tainted Chakra', 'Blood Prison 12/13', 20, NULL, NULL),
('Thunder Stone', 'Bloody Mist Village 5/12', 2, NULL, NULL),
('Turtle Shell', 'Hot Water Village 4/13', 45, NULL, NULL),
('Uchiha Blood', 'Forbidden Temple: Outer - Young and Tainted', 4, NULL, '6% on Easy'),
('Uchiha Blood', 'Forbidden Temple: Outer - Young and Itachi', 8, 'Medium', '10% on Hard'),
('Uchiha Blood', 'Forbidden Temple: Outer - Young and Vengeful', 12, 'Extreme', NULL),
('Uchiha Crest', 'Moryo Shrine 11/13', 10, NULL, NULL),
('Uchiha Crest', 'Moryo Shrine 11/13 - Hard Path', 15, NULL, NULL),
('White Diamond', 'Mount Shumisen 1/12', 5, NULL, NULL),
('Wolf Pelt', 'Snow Country 2/7', 40, NULL, NULL),
('Wolf Pelt', 'Land of Iron 1/11', 72, NULL, NULL),
('Wood', 'ANBU Hideout 2/7', 70, NULL, NULL),
('Wood', 'The Sealed World 7/12', 88, NULL, NULL),
('Zephyr Leaf', 'ANBU Hideout 1/7', 5, NULL, NULL);

-- Insert findable LW's into item location
INSERT INTO public.item_location (name, location, chance, difficulty, note) VALUES
('Arcane Grimoire', 'Island Turtle 2/15', 0.12, 'Hard', NULL),
('Bashosen', 'The Sealed World 2/11', 0.08, NULL, 'Drops to 0.04 in Medium'),
('Benihisago', 'Mount Shumisen 10/12', 0.06, 'Easy', NULL),
('Chakra Fruit', 'God Tree 5/5', 0.01, NULL, NULL),
('Chimera Cloak', 'Mount Shumisen 3/12', 0.06, NULL, NULL),
('Crustacean Knuckles', 'Immortal Beast Cavern 1/16', 0.12, 'Easy', NULL),
('Cursed Chakram', 'Forest of Death 5/8', 0.03, NULL, NULL),
('Cursed Scroll', 'Forest of Death 8/8 - Hard Path (Kabuto)', 0.06, NULL, NULL),
('Dark Orb', 'Frozen Island 6/10', 0.2, NULL, NULL),
('Demonic Flute', 'Ryuichi Cave 4/10', 0.1, NULL, NULL),
('Eye Scope', 'Akatsuki Temple 2/11', 0.1, NULL, NULL),
('Firestorm Staff', 'Moryo Shrine 9/13', 0.17, NULL, NULL),
('Ice Brand', 'Frozen Island 9/11', 0.15, NULL, NULL),
('Kabutowari', 'Tower of God - Jinin (2nd Mission)', 100, NULL, NULL),
('Kiba', 'Tower of God - Ameyuri (2nd Mission)', 100, NULL, NULL),
('Kohaku no Johei', 'God Tree 1/5', 0.1, 'Extreme', NULL),
('Kokinjo', 'Mount Shumisen 10/12', 0.03, 'Hard', NULL),
('Kubikiribocho', 'Tower of God - Zabuza (2nd Mission)', 100, NULL, NULL),
('Kurosawa', 'Land of Iron 11/11', 0.05, NULL, NULL),
('Kusanagi', 'Ryuichi Cave Easy Path 2/10', 0.08, 'Medium', NULL),
('Mage Masher', 'Blood Prison 10/13', 0.08, NULL, NULL),
('Molten Hammer', 'Bloody Mist Village 1/12 - Jinin', 0.2, NULL, NULL),
('Nuibari', 'Tower of God - Kushimaru (2nd Mission)', 100, NULL, NULL),
('Nunoboko', 'Kamuis Dimension 10/10', 0.03, 'Impossible', NULL),
('Occult Grimoire', 'Moryo Shrine 10/13', 0.12, 'Easy', NULL),
('Oxygen Mask', 'Hot Water Village 6/13', 0.17, NULL, NULL),
('Rhi Knuckles', 'Immortal Beast Cavern 6/16', 0.1, 'Hard', NULL),
('Samehada', 'Tower of God - Fuguki (2nd Mission)', 100, NULL, NULL),
('Sealing Sword Scroll ', 'Tower of God - Mangetsu (2nd Mission)', 100, NULL, NULL),
('Shibuki', 'Tower of God - Jinpachi (2nd Mission)', 100, NULL, NULL),
('Shichiseiken', 'The Sealed World 2/11', 0.04, NULL, NULL),
('Sinister Shuriken', 'Bloody Mist Village 6/12 - Kushimaru', 0.12, 'Easy', NULL),
('Skeletal Staff', 'Mountains Graveyard 7/10', 0.1, NULL, NULL),
('Spore Gauntlet', 'Akatsuki Temple 7/11', 0.1, NULL, NULL),
('Steam Armor', 'Ruins of Roran 10/13', 0.1, NULL, NULL),
('Stone of Gelel', 'Ruins of Roran 9/13', 0.1, NULL, NULL),
('Totsuka', 'Mountains Graveyard 8/10', 0.03, 'Extreme', NULL),
('White Light Blade', 'ANBU Hideout 7/7', 0.08, NULL, NULL),
('Yagyu Shuriken', 'Bloody Mist Village 4/12 - Jinpachi', 0.12, 'Hard', NULL),
('Yata Mirror', 'Mountains Graveyard 8/10', 0.06, 'Hard', 'Drops to 0.03 on Extreme');

-- Create table for recipe
CREATE TABLE IF NOT EXISTS public.recipe (
	sampleid bigint DEFAULT nextval('recipe_sampleid_seq'::regclass) NOT NULL,
	product character varying NOT NULL,
	product_qty integer NOT NULL,
	ingr_1 character varying NOT NULL,
	qty_1 integer NOT NULL,
	ingr_2 character varying,
	qty_2 integer,
	ingr_3 character varying,
	qty_3 integer,
	ingr_4 character varying,
	qty_4 integer,
	PRIMARY KEY(sampleid)
);

COMMIT;

-- Add fk for recipe table table
ALTER TABLE IF EXISTS public.recipe
	ADD CONSTRAINT fk_name_product
	FOREIGN KEY (product)
	REFERENCES public.equipment (name);
	
ALTER TABLE IF EXISTS public.recipe
	ADD CONSTRAINT fk_name_ingr_1
	FOREIGN KEY (ingr_1)
	REFERENCES public.equipment (name);
	
ALTER TABLE IF EXISTS public.recipe
	ADD CONSTRAINT fk_name_ingr_2
	FOREIGN KEY (ingr_2)
	REFERENCES public.equipment (name);
	
ALTER TABLE IF EXISTS public.recipe
	ADD CONSTRAINT fk_name_ingr_3
	FOREIGN KEY (ingr_3)
	REFERENCES public.equipment (name);
	
ALTER TABLE IF EXISTS public.recipe
	ADD CONSTRAINT fk_name_ingr_4
	FOREIGN KEY (ingr_4)
	REFERENCES public.equipment (name);
 
-- Insert into Recipes for Materials
 INSERT INTO public.recipe (product, product_qty, ingr_1, qty_1, ingr_2, qty_2, ingr_3, qty_3, ingr_4, qty_4) VALUES
('Ice Shard', 1, 'Ice Horn', 15, 'Alkahest', 1, NULL, NULL, NULL, NULL),
('Ironbark', 1, 'Alkahest', 1, 'Iron', 2, 'Wood', 8, NULL, NULL),
('Refined Chakra Plate', 1, 'Alkahest', 1, 'Chakra Plate', 10, NULL, NULL, NULL, NULL),
('Runic Talisman', 1, 'Alkahest', 1, 'Rune Ward', 5, 'Wooden Talisman', 1, NULL, NULL),
('Spider Gland', 1, 'Level 8 Giant Spider', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Talatat Stone', 1, 'Alkahest', 1, 'Quicklime', 3, NULL, NULL, NULL, NULL),
('Wolf Eye', 1, 'Level 8 Wolf', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Imperial Jade', 1, 'Alkahest', 1, 'Jade', 10, NULL, NULL, NULL, NULL),
('Byakugan Eye', 1, 'Panacea', 1, 'Level 20 Hanabi', 1, NULL, NULL, NULL, NULL),
('Byakugan Eye', 1, 'Panacea', 1, 'Level 20 Hiashi', 1, NULL, NULL, NULL, NULL),
('Byakugan Eye', 2, 'Panacea', 1, 'Level 28 Neji', 1, NULL, NULL, NULL, NULL),
('Byakugan Eye', 3, 'Panacea', 1, 'Level 33 Hinata', 1, NULL, NULL, NULL, NULL),
('Mangekyo Eye', 10, 'Panacea', 1, 'Level 10 Mangekyou Sharingan', 1, NULL, NULL, NULL, NULL),
('Hashiramas DNA', 1, 'Panacea', 1, 'Level 25 Hashirama', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 1, 'Panacea', 1, 'Level 10 Crow', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 1, 'Panacea', 1, 'Level 10 Genjutsu Crow', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 1, 'Panacea', 1, 'Level 20 Kakashi', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 2, 'Panacea', 1, 'Level 27 Shisui', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 2, 'Panacea', 1, 'Level 28 Sasuke', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 3, 'Panacea', 1, 'Level 33 Itachi', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 4, 'Panacea', 1, 'Level 37 Obito', 1, NULL, NULL, NULL, NULL),
('Sharingan Eye', 5, 'Panacea', 1, 'Level 43 Tobi', 1, NULL, NULL, NULL, NULL),
('Iron Sand', 1, 'Alkahest', 1, 'Iron', 2, 'Red Sand', 2, NULL, NULL),
('Wooden Talisman', 1, 'Alkahest', 1, 'Ink Bottle', 1, 'Wood', 4, NULL, NULL),
('Angel Feather', 1, 'Alkahest', 1, 'Cursed Feather', 10, 'Holy Water', 3, NULL, NULL),
('Beast Soul', 1, 'Azoth', 1, 'Chimera DNA', 1, 'Dragon Soul', 1, 'Serpent Soul', 1),
('Celestial Silk', 1, 'Alkahest', 1, 'Celestial Dye', 4, 'Muga Silk', 6, NULL, NULL),
('Mimirwater', 1, 'Alkahest', 1, 'Holy Water', 2, 'Yggdrasil Sap', 2, NULL, NULL),
('Stone Tablet', 1, 'Curse Mark Pattern', 4, 'Otsutsuki Cells', 3, 'Soil Stone', 1, 'Uchiha Crest', 5),
('Curse Mark DNA', 10, 'Level 9 Lesser Cursed Seal BL', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Reincarnation Ether', 1, 'Ascension Ore', 10, 'Azoth', 1, 'Beast Soul', 1, 'Mimirwater', 1),
('Altuna Rune', 1, 'Azoth', 1, 'Runestone', 6, NULL, NULL, NULL, NULL),
('Atlantic Artifact', 1, 'Aqua Stone', 4, 'Azoth', 1, 'Babylonian Stone', 1, NULL, NULL),
('Bolt Artifact', 1, 'Azoth', 1, 'Babylonian Stone', 1, 'Thunder Stone', 4, NULL, NULL),
('Czarwood', 1, 'Ancient Coin', 1, 'Darkwood', 1, 'Yliaster', 1, NULL, NULL),
('Divine Iron', 1, 'Angel Feather', 1, 'Damascus Steel', 1, 'Yliaster', 1, NULL, NULL),
('Tundra Artifact', 1, 'Azoth', 1, 'Babylonian Stone', 1, 'Ice Shard', 10, NULL, NULL),
('Gaia Artifact', 1, 'Azoth', 1, 'Babylonian Stone', 1, 'Soil Stone', 4, NULL, NULL),
('Heart Container', 1, 'Alkahest', 1, 'Black Threads', 30, NULL, NULL, NULL, NULL),
('Inferno Artifact', 1, 'Azoth', 1, 'Molten Stone', 4, 'Babylonian Stone', 1, NULL, NULL),
('Osteoblast', 1, 'Alkahest', 1, 'Stem Cell', 10, 'Bone', 50, NULL, NULL),
('Osteoclast', 1, 'Alkahest', 1, 'Stem Cell', 10, 'Blood Vessel', 10, NULL, NULL),
('Chimera Heart', 2, 'Level 7 Chimera', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('Snakeskin', 25, 'Level 5 Giant Snake', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('Wolf Pelt', 50, 'Level 5 Wolf', 1, NULL, NULL, NULL, NULL, NULL, NULL),
('Holy Water', 1, 'Alkahest', 1, 'Clear Water', 35, NULL, NULL, NULL, NULL),
('Ouroboros Stone', 1, 'Azoth', 1, 'Dragon Blood', 4, 'Mercury', 18, 'Snake Eye', 8),
('Spirit Container', 1, 'Heart Container', 2, 'Yliaster', 1, NULL, NULL, NULL, NULL),
('Babylonian Stone', 1, 'Alkahest', 1, 'Iron Sand', 5, 'Roran Coin', 5, NULL, NULL),
('Dragon Bone', 1, 'Alkahest', 1, 'Adamantine Bone', 3, 'Bone', 30, 'Dragon Blood', 2),
('Monkey King Fur', 1, 'Alkahest', 1, 'Bezoar', 2, 'Lion Fur', 15, NULL, NULL),
('Robot Processor', 1, 'Alkahest', 1, 'Robot Parts', 15, NULL, NULL, NULL, NULL),
('Spider Silk', 1, 'Alkahest', 1, 'Muga Silk', 2, 'Spider Web', 30, NULL, NULL),
('Venoxin Mutagen', 1, 'Alkahest', 1, 'Berserker Enzyme', 4, 'Poison Vial', 4, 'Salamander Venom', 1),
('Lifestream', 1, 'Panacea', 1, 'Level 8 Life Dragon', 1, NULL, NULL, NULL, NULL),
('Dharma Spirit', 1, 'Alkahest', 1, 'Dharma Wheel', 5, 'Ghost Spirit', 2, NULL, NULL),
('Python Longevity Serum', 1, 'Yliaster', 1, 'Snake Serum', 1, 'Snakeskin', 30, 'Snake Eye', 6),
('Yggdrasil Sap', 1, 'Alkahest', 1, 'Yggdrasil Bark', 1, 'Zephyr Leaf', 1, NULL, NULL),
('Aero Stone', 1, 'Alkahest', 1, 'Granite', 2, 'Zephyr Leaf', 2, NULL, NULL),
('Aqua Stone', 1, 'Alkahest', 1, 'Granite', 2, 'Clear Water', 25, NULL, NULL),
('Arachne Web', 1, 'Alkahest', 1, 'Spider Web', 25, NULL, NULL, NULL, NULL),
('Beetle Tail', 1, 'Panacea', 1, 'Level 10 Chomei', 1, NULL, NULL, NULL, NULL),
('Damascus Steel', 1, 'Azoth', 1, 'Steel', 10, NULL, NULL, NULL, NULL),
('Dao Dragon Pill', 1, 'Alkahest', 1, 'Dragon Blood', 4, 'Mind Awakening Pill', 10, NULL, NULL),
('Elvenskin', 1, 'Alkahest', 1, 'Leather', 12, 'Zephyr Leaf', 1, NULL, NULL),
('Fiver Tail', 1, 'Level 10 Kokuo', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Ghost Orb', 1, 'Alkahest', 1, 'Ghost Spirit', 10, NULL, NULL, NULL, NULL),
('Iron Tail', 1, 'Level 10 Shukaku', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Molten Stone', 1, 'Alkahest', 1, 'Granite', 2, 'Magma', 1, NULL, NULL),
('Monkey Tail', 1, 'Panacea', 1, 'Level 10 Son Goku', 1, NULL, NULL, NULL, NULL),
('Runestone', 1, 'Alkahest', 1, 'Rune Ward', 5, 'Talatat Stone', 1, NULL, NULL),
('Ryumyaku Source', 1, 'Level 10 Mecha Mukade', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Sea Gods Tear', 1, 'Level 10 Umibozu', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Slime Tail', 1, 'Level 10 Saiken', 1, 'Panacea', 1, NULL, NULL, NULL, NULL),
('Soil Stone', 1, 'Alkahest', 1, 'Clay', 30, 'Granite', 2, NULL, NULL),
('Steel', 1, 'Alkahest', 1, 'Iron', 10, NULL, NULL, NULL, NULL),
('Ceremonial Mask', 1, 'Alkahest', 1, 'Antique Mask', 4, 'Ironbark', 1, NULL, NULL),
('Darkwood', 1, 'Azoth', 1, 'Ironbark', 7, NULL, NULL, NULL, NULL),
('Magma', 1, 'Alkahest', 1, 'Lava', 4, 'Granite', 3, NULL, NULL),
('Opal', 1, 'Alkahest', 1, 'Quartz Crystal', 1, 'White Diamond', 1, NULL, NULL),
('Yggdrasil Bark', 1, 'Alkahest', 1, 'Ironbark', 1, 'Rune Ward', 4, NULL, NULL);

-- Add item recipies into db
INSERT INTO public.recipe (product, product_qty, ingr_1, qty_1, ingr_2, qty_2, ingr_3, qty_3, ingr_4, qty_4) VALUES
('Adamantine Kunai', 1, 'Adamantine Bone', 18, 'Steel', 3, NULL, NULL, NULL, NULL),
('Anbu Mask', 1, 'Anbu Paint', 10, 'Cracked Anbu Mask', 20, NULL, NULL, NULL, NULL),
('Anbu Vest', 1, 'Anbu Paint', 10, 'Flak Jacket', 1, NULL, NULL, NULL, NULL),
('Bionic Shinobi Gauntlet', 1, 'Gundam Core', 1, 'Jutsu Scroll', 20, 'Prototype Chakra Gauntlet', 1, NULL, NULL),
('Blight Rod', 1, 'Poison Vial', 8, 'Wood', 20, NULL, NULL, NULL, NULL),
('Bronze Token', 1, 'Bronze Coin', 7, NULL, NULL, NULL, NULL, NULL, NULL),
('Chimera Gauntlets', 1, 'Lion Fur', 20, 'Steel', 5, NULL, NULL, NULL, NULL),
('Chimera Gloves', 1, 'Lion Fur', 14, 'Leather', 12, NULL, NULL, NULL, NULL),
('Cold Kunai', 1, 'Ice Horn', 20, 'Iron', 7, NULL, NULL, NULL, NULL),
('Detonating Clay', 1, 'Clay', 28, 'Lava', 4, 'Quicklime', 4, NULL, NULL),
('Dirty Dice', 1, 'Dice', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('Elemental Puppet', 1, 'Iron Sand', 2, 'Red Sand', 10, 'Puppet Part', 20, NULL, NULL),
('Flak Jacket', 1, 'Leather', 12, NULL, NULL, NULL, NULL, NULL, NULL),
('Ghost Gloves', 1, 'Leather', 10, 'Ghost Spirit', 12, NULL, NULL, NULL, NULL),
('Green Jumpsuit', 1, 'Elvenskin', 2, 'Essence of Swiftness', 2, 'Muga Silk', 5, NULL, NULL),
('Illusion Pendant', 1, 'Runic Talisman', 1, 'Dark Chakra Eel', 5, NULL, NULL, NULL, NULL),
('Jade Awl', 1, 'Opal', 1, 'Jade', 16, NULL, NULL, NULL, NULL),
('Jade Dagger', 1, 'Ruby', 2, 'Jade', 16, NULL, NULL, NULL, NULL),
('Lucky Talisman', 1, 'Dice', 10, 'Wooden Talisman', 4, NULL, NULL, NULL, NULL),
('Mecha Puppet', 1, 'Robot Processor', 1, 'Robot Parts', 30, NULL, NULL, NULL, NULL),
('Poison Puppet', 1, 'Puppet Part', 10, 'Poison Vial', 8, 'Iron Sand', 2, NULL, NULL),
('Poison Shuriken', 1, 'Salamander Venom', 1, 'Shuriken', 1, 'Snake Serum', 1, NULL, NULL),
('Prototype Chakra Armor', 1, 'Iron', 10, 'Artificial Chakra Core', 8, 'Chakra Plate', 20, NULL, NULL),
('Prototype Chakra Gauntlet', 1, 'Chakra Plate', 10, NULL, NULL, NULL, NULL, NULL, NULL),
('Prototype Chakra Helmet', 1, 'Chakra Plate', 20, 'Iron', 8, NULL, NULL, NULL, NULL),
('Serpent Armor', 1, 'Serpent Scales', 26, 'Steel', 2, NULL, NULL, NULL, NULL),
('Serpent Pike', 1, 'Dark Chakra Eel', 10, 'Ironbark', 3, 'Serpent Horn', 17, NULL, NULL),
('Shuriken', 1, 'Iron', 4, NULL, NULL, NULL, NULL, NULL, NULL),
('Snakeskin Slippers', 1, 'Leather', 10, 'Snakeskin', 16, NULL, NULL, NULL, NULL),
('Snakeskin Suit', 1, 'Snake Eye', 1, 'Leather', 50, 'Snakeskin', 30, NULL, NULL),
('Tainted Shuriken', 1, 'Shuriken', 1, 'Tainted Chakra', 8, NULL, NULL, NULL, NULL),
('Thunder Rod', 1, 'Thunder Stone', 1, 'Wood', 20, NULL, NULL, NULL, NULL),
('Tortoise Mask', 1, 'Antique Mask', 1, 'Lava', 4, 'Turtle Shell', 18, NULL, NULL),
('Turtle Shield', 1, 'Turtle Shell', 15, NULL, NULL, NULL, NULL, NULL, NULL),
('Twisted Mask', 1, 'Antique Mask', 7, 'Tainted Chakra', 3, NULL, NULL, NULL, NULL),
('Undying Mask', 1, 'Cracked Anbu Mask', 1, 'Ghost Orb', 1, 'Ice Shard', 1, NULL, NULL),
('Vajra', 1, 'Lightning Shard', 1, 'White Diamond', 5, NULL, NULL, NULL, NULL),
('Wolf Mantle', 1, 'Wolf Pelt', 50, NULL, NULL, NULL, NULL, NULL, NULL);


-- Add LW recipes into db
INSERT INTO public.recipe (product, product_qty, ingr_1, qty_1, ingr_2, qty_2, ingr_3, qty_3, ingr_4, qty_4) VALUES
('Abaddons Armory', 1, 'Ancient Blood', 1, 'Darkwood', 2, 'Demon Wing', 15, 'Tainted Shuriken', 1),
('Arachnes Arbalest', 1, 'Arachne Web', 10, 'Darkwood', 4, 'Salamander Venom', 10, 'Spider Gland', 1),
('Arctic Battlearmor', 1, 'Refined Chakra Plate', 20, 'Tundra Artifact', 1, NULL, NULL, NULL, NULL),
('Argent White Sword', 1, 'Ascension Ore', 15, 'Ghost Orb', 5, 'Thunder Stone', 14, NULL, NULL),
('Artificial Bloodline Core', 1, 'Hyuuga Blood', 12, 'Artificial Chakra Core', 6, 'Uchiha Blood', 12, NULL, NULL),
('Bahamuts Mane', 1, 'Chimera DNA', 3, 'Chimera Heart', 4, 'Dragon Soul', 4, 'Lion Fur', 100),
('Blades of Kali', 1, 'Ancient Blood', 15, 'Damascus Steel', 2, 'Dharma Spirit', 15, 'Essence of Darkness', 20),
('Bloodsealed Talisman', 1, 'Blood Vessel', 30, 'Chimera Heart', 3, 'Wooden Talisman', 5, NULL, NULL),
('Bone Armor', 1, 'Bone', 150, 'Adamantine Bone', 20, 'Ghost Spirit', 40, NULL, NULL),
('Bone Pendant', 1, 'Osteoclast', 1, 'Bone', 20, 'Runic Talisman', 1, NULL, NULL),
('Chakra Exosuit', 1, 'Artificial Chakra Core', 1, 'Black Receiver', 12, 'Chakra Shield', 1, 'Robot Processor', 2),
('Chakra Shield', 1, 'Steel', 5, 'Refined Chakra Plate', 7, NULL, NULL, NULL, NULL),
('Cursed Ring', 1, 'Ascension Ore', 1, 'Curse Mark DNA', 3, NULL, NULL, NULL, NULL),
('Daedalus Cuirass', 1, 'Curse Mark Pattern', 5, 'Damascus Steel', 2, 'Molten Stone', 5, 'Elvenskin', 8),
('Divine Khakkara', 1, 'Dharma Spirit', 10, 'Gaia Artifact', 2, 'Holy Water', 10, 'Lotus Amulet', 6),
('Dracolichs Phylactery', 1, 'Ancient Blood', 25, 'Deathly Dust', 10, 'Dragon Bone', 8, 'Ghost Orb', 10),
('Electrum Coin', 1, 'Ancient Coin', 1, 'Bronze Coin', 4, 'Roran Coin', 4, NULL, NULL),
('Fafnirs Scales', 1, 'Dragon Soul', 2, 'Ruby', 20, 'Runestone', 6, 'Serpent Scales', 50),
('Fenrirs Fang', 1, 'Damascus Steel', 1, 'Warg Talisman', 1, 'Wolf Eye', 1, NULL, NULL),
('Ghost Cape', 1, 'Wolf Mantle', 1, 'Muga Silk', 8, NULL, NULL, NULL, NULL),
('Gungnir', 1, 'Runestone', 10, 'Runic Talisman', 10, 'Yggdrasil Bark', 10, 'Ouroboros Stone', 3),
('Heart of Stone', 1, 'Heart Container', 1, 'Level 20 Onoki', 1, 'Black Threads', 12, 'Talatat Stone', 3),
('Heart of Sound', 1, 'Heart Container', 1, 'Level 20 Orochimaru', 1, 'Black Threads', 12, 'Curse Mark Pattern', 10),
('Heart of Sand', 1, 'Heart Container', 1, 'Level 20 Chiyo', 1, 'Black Threads', 12, 'Red Sand', 50),
('Heart of Root', 1, 'Heart Container', 1, 'Level 20 Danzo', 1, 'Black Threads', 12, 'Uchiha Blood', 5),
('Heart of Rain', 1, 'Heart Container', 1, 'Level 20 Hanzo', 1, 'Black Threads', 12, 'Aqua Stone', 2),
('Heart of Neko', 1, 'Heart Container', 1, 'Level 20 Nekomata', 1, 'Black Threads', 12, 'Lion Fur', 35),
('Heart of Mist', 1, 'Heart Container', 1, 'Level 20 Mei', 1, 'Black Threads', 12, 'Steam Stone', 2),
('Heart of Leaf', 1, 'Heart Container', 1, 'Level 20 Hiruzen', 1, 'Black Threads', 12, 'Zephyr Leaf', 5),
('Heart of Cloud', 1, 'Heart Container', 1, 'Level 20 A', 1, 'Black Threads', 12, 'Thunder Stone', 2),
('Heart of Anbu', 1, 'Heart Container', 1, 'Level 20 Sakumo', 1, 'Black Threads', 12, 'Anbu Mask', 1),
('Heretical God Seed', 1, 'Bolt Artifact', 4, 'Gaia Artifact', 4, 'Inferno Artifact', 2, 'Spirit Container', 2),
('Hugins Talon', 1, 'Rune Word', 10, 'Ruby', 4, 'Crow Feather', 25, NULL, NULL),
('Icarus Wings', 1, 'Elvenskin', 10, 'Demon Wing', 20, 'Arachne Web', 4, 'Angel Feather', 3),
('Inksealed Talisman', 1, 'Ink Bottle', 50, 'Chimera Heart', 3, 'Wooden Talisman', 5, NULL, NULL),
('Jade Emeperors Pagoda', 1, 'Celestial Silk', 3, 'Czarwood', 2, 'Imperial Jade', 16, 'Lotus Amulet', 3),
('Jibrils Garb', 1, 'Angel Feather', 2, 'Celestial Silk', 3, 'Charm of Protection', 5, 'Vajra', 1),
('Jingu Bang', 1, 'Monkey Tail', 1, 'Czarwood', 4, 'Monkey King Fur', 6, 'Hashiramas DNA', 4),
('Jörmungandrs Skull', 1, 'Mimirwater', 4, 'Ouroboros Stone', 2, 'Runestone', 8, 'Serpent Soul', 3),
('Krakens Tentacle', 1, 'Atlantic Artifact', 1, 'Ink Bottle', 100, 'Octopus Arm', 2, 'Sea Gods Tear', 1),
('Leviathans Fin', 1, 'Fish Scales', 100, 'Sea Gods Tear', 1, 'Serpent Soul', 2, NULL, NULL),
('Lichs Vestment', 1, 'Spider Silk', 2, 'Deathly Dust', 5, 'Ghost Spirit', 60, NULL, NULL),
('Mangekyo Seal', 1, 'Ascension Ore', 1, 'Mangekyo Eye', 3, NULL, NULL, NULL, NULL),
('MBF-02', 1, 'Ryumyaku Source', 1, 'Gundam Core', 2, 'Steel', 12, 'Robot Parts', 50),
('Midgard Mail', 1, 'Runestone', 4, 'Elvenskin', 3, NULL, NULL, NULL, NULL),
('Minotaurs Horn', 1, 'Sea Gods Tear', 1, 'Gaia Artifact', 3, 'Serpent Horn', 15, 'Osteoblast', 2),
('Mjölnir', 1, 'Yggdrasil Bark', 15, 'Altuna Rune', 2, 'Ascension Ore', 40, 'Bolt Artifact', 4),
('Munins Talon', 1, 'Rune Word', 10, 'Opal', 2, 'Crow Feather', 25, NULL, NULL),
('Nibelungs Ring', 1, 'Opal', 20, 'Ouroboros Stone', 2, 'Runic Talisman', 5, NULL, NULL),
('Niddhoggrs Claw', 1, 'Beast Soul', 1, 'Venoxin Mutagen', 4, 'Yggdrasil Bark', 5, NULL, NULL),
('Origin Bone Scepter', 1, 'Dragon Bone', 18, 'Essence of Origin', 15, 'Dao Dragon Pill', 2, 'Beast Soul', 6),
('Pandemonium Flute', 1, 'Darkwood', 1, 'Deathly Dust', 6, 'Serpent Horn', 50, 'Slime Tail', 1),
('Piezoelectric Clock', 1, 'Ancient Coin', 1, 'Iron Sand', 10, 'Quartz Crystal', 14, 'Rusty Spring', 16),
('Python Robe', 1, 'Serpent Soul', 2, 'Serpent Scales', 40, 'Snakeskin', 40, 'Spider Silk', 2),
('Rhinoceros Shell', 1, 'Beetle Tail', 1, 'Dice', 90, 'Gaia Artifact', 1, 'Turtle Shell', 90),
('Scrolls of Sutra', 1, 'Aero Stone', 10, 'Dharma Spirit', 15, 'Essence of Smoke', 20, 'Jutsu Scroll', 400),
('Skull Helmet', 1, 'Osteoblast', 3, 'Ghost Orb', 6, 'Ancient Coin', 6, NULL, NULL),
('Snakeskin Seal', 1, 'Snakeskin', 120, 'Snake Serum', 5, NULL, NULL, NULL, NULL),
('Snakeskin Shako', 1, 'Snake Serum', 2, 'Snake Eye', 5, 'Snakeskin', 50, NULL, NULL),
('Sunpierce Sword', 1, 'Ruby', 7, 'Magma', 12, NULL, NULL, NULL, NULL),
('Tiamats Tiara', 1, 'Lifestream', 1, 'Serpent Soul', 10, 'Clear Water', 300, 'Babylonian Stone', 4),
('Valhalla Helm', 1, 'Ceremonial Mask', 4, 'Rune Ward', 30, NULL, NULL, NULL, NULL),
('Warg Talisman', 1, 'Ancient Blood', 4, 'Bone', 30, 'Wolf Pelt', 20, NULL, NULL),
('Websealed Talisman', 1, 'Spider Web', 100, 'Wooden Talisman', 5, 'Chimera Heart', 3, NULL, NULL),
('Winged Mask', 1, 'Twisted Mask', 1, 'Demon Wing', 10, 'Dark Chakra Eel', 20, NULL, NULL),
('Xiantians Divine Axe', 1, 'Ivory', 100, 'Python Longevity Serum', 3, 'Divine Iron', 2, 'Dao Dragon Pill', 5);

