import os
from dotenv import load_dotenv
from sqlalchemy import (
    Boolean,
    Column,
    Integer,
    BigInteger,
    String,
    Float,
    ForeignKey,
)
# from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import sqlalchemy

Base = declarative_base()

# Warzone detection


class Warzone(Base):
    __tablename__ = "warzone"
    id = Column(Integer, primary_key=True)
    grid = Column(Integer)
    zone = Column(Integer)
    name = Column(String)
    defeated = Column(Boolean)
    health_percentage = Column(Integer)
    image = Column(String)
    reported = Column(Boolean)
    rewards = Column(String)
    veins = Column(String)
    condition = Column(String)
    duration = Column(String)
    valuation = Column(Float)
    damage = Column(String)
    random = Column(Boolean)


class Target(Base):
    __tablename__ = "target"
    id = Column(Integer, primary_key=True)
    boss_id = Column(Integer, ForeignKey("warzone.id"))
    equipment_id = Column(Integer, ForeignKey("equipment.id"))
    priority = Column(Integer)
    clan = Column(String)


# Queue system

class Player(Base):
    __tablename__ = "player"
    id = Column(Integer, primary_key=True)
    discord_id = Column(BigInteger)
    display_name = Column(String)


class Equipment(Base):
    __tablename__ = "equipment"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    url = Column(String)
    we_val = Column(Integer)
    shop_qty = Column(Integer)
    shop_val = Column(Integer)


class Location(Base):
    __tablename__ = "item_location"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    location = Column(String)
    chance = Column(Integer)
    difficulty = Column(String)
    note = Column(String)


class Recipe(Base):
    __tablename__ = "recipe"
    sampleid = Column(Integer, primary_key=True)
    product = Column(String)
    product_qty = Column(Integer)
    ingr_1 = Column(String)
    qty_1 = Column(Integer)
    ingr_2 = Column(String)
    qty_2 = Column(Integer)
    ingr_3 = Column(String)
    qty_3 = Column(Integer)
    ingr_4 = Column(String)
    qty_4 = Column(Integer)


class Queue(Base):
    __tablename__ = "queue"
    id = Column(Integer, primary_key=True)
    player_id = Column(BigInteger)
    equipment_id = Column(Integer, ForeignKey("equipment.id"))
    clan = Column(String)


# Damage system

class Damage(Base):
    __tablename__ = "warzone_damage"
    player_id = Column(Integer, primary_key=True)
    warzone = Column(Integer)
    damage = Column(Integer)
    clan = Column(String)


if __name__ == "__main__":
    load_dotenv()
    print("starting")
    dbuser = os.getenv("DB_USER")
    dbpass = os.getenv("DB_PASS")
    database = os.getenv("DB_NAME")
    dbport = os.getenv("DB_PORT")
    engine = sqlalchemy.create_engine(
        f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=True)

    Session = sessionmaker(bind=engine)
    session = Session()
    Base.metadata.create_all(engine)

    # Populate materials
    # with open('materials.txt', 'r') as mat_file:
    #    for mat in mat_file:
    #        name = mat.rstrip()
    #        material = Material(name=name)
    #        session.add(material)
    # session.commit()

    # Populate empty warzones
    grid_size = 450
    for j in range(6):
        for i in range(grid_size):
            test = Warzone(grid=i+j*grid_size, zone=j, reported=0)
            session.add(test)
            print(i, j)

    session.commit()
