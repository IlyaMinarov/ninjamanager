#!/usr/bin/python

# -*- coding: utf-8 -*-

import time
import os
import asyncio
import sys
from typing import List
from inspect import iscoroutinefunction
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from dotenv import load_dotenv
from sqlalchemy.orm import sessionmaker
import sqlalchemy
from models import Equipment, Warzone, Target


# TODO: CHANGE NAME IF USED, SAME AS PYTHON BUILT IN LIBRARY
# def inspect():
#     bars = browser.find_elements(By.CLASS_NAME, 'c-bar__text')
#     cur_arena = bars[0].text.split("\n/")[0]
#     max_arena = bars[0].text.split("\n/")[1]
#     cur_world = bars[1].text.split("\n/")[0]
#     max_world = bars[1].text.split("\n/")[1]
#     resources = browser.find_elements(By.CLASS_NAME, 'header-team__resource')
#     gold = resources[0].text
#     print('O', "Arena:", 'P', cur_arena, "/", max_arena,
#           'O', "   World", 'P', cur_world, "/", max_world,
#           'O', "   Gold:", 'P', gold, 'W')


zones = {0: "forest-of-death",
         1: "demon-desert",
         2: "land-of-iron",
         3: "coast-of-lightning",
         4: "great-war-plains"}


# ---------------- #
# Helper Functions #
# ---------------- #

async def call_func_every_x_seconds(timeout: int, func):
    '''Calls a function every `timeout` seconds forever. Should be used with `asyncio` event loop.'''
    while True:
        await asyncio.sleep(timeout)
        if iscoroutinefunction(func):
            await func()
        else:
            func()


def wait_for(timeout: int, ec, *params):
    '''Waits `timeout` seconds for an element to exist on the webpage. Returns `None` if no element exists after the timeout.'''
    try:
        return WebDriverWait(browser, timeout).until(ec(*params))
    except:
        return None


def wait_for_element(by, selector: str, timeout: int = 10) -> WebElement:
    '''Waits `timeout` seconds (default of 10) for an element to exist on the webpage. Returns `None` if no element exists after the timeout.'''
    return wait_for(timeout, EC.presence_of_element_located, (by, selector))


def get_element(from_element_or_browser, by, selector) -> WebElement:
    '''Gets an element safely without throwing an exception. Returns `None` if no element found.'''
    try:
        return from_element_or_browser.find_element(by, selector)
    except:
        return None


def get_elements(from_element_or_browser, by, selector) -> List[WebElement]:
    '''Gets elements safely without throwing an exception. Returns empty list if no elements found.'''
    try:
        return from_element_or_browser.find_elements(by, selector)
    except:
        return []


# ---------------- #
# Scrape Functions #
# ---------------- #


def login(user: str, password: str, debug: bool) -> bool:
    if debug:
        print(f'Starting login process using {user}.')

    # Open the page.
    browser.get('https://www.ninjamanager.com/account/login')

    # Get the email/username field.
    login_field = wait_for_element(By.ID, 'input-login')
    if login_field is None:
        return False
    login_field.send_keys(user)

    # Get the password field.
    password_field = wait_for_element(By.ID, 'input-password')
    if password_field is None:
        return False
    password_field.send_keys(password)

    if debug:
        print('Submitting login form.')

    # Submit the field.
    password_field.submit()

    if debug:
        print('Waiting for URL change for login.')

    wait_for(30, EC.url_changes, 'https://www.ninjamanager.com/account/login/nm')

    if debug:
        print('Getting the body and checking if correctly logged in.')

    # Wait for the response.
    body = wait_for_element(By.TAG_NAME, 'body')
    if body is None:
        return False

    return 'Successfully logged in!' in body.text


def shop_scan():
    print('shop scan running!')

    # Open the page.
    browser.get('https://www.ninjamanager.com/clans/shop')

    # Wait until available button loaded, then click it.
    available_button = wait_for_element(
        By.CSS_SELECTOR, '.c-filter-button[data-filter="available"]')
    if available_button is None:
        return
    available_button.click()

    # Get the shop list.
    shop_list_element = wait_for_element(By.ID, 'shop-list')
    if shop_list_element is None:
        return

    # Get all the shop elements and loop through them.
    shop_elements: list[WebElement] = shop_list_element.find_elements(
        By.XPATH, './*')
    shop_items_dict = {}
    for shop_element in shop_elements:
        # Get the item name.
        name_element = get_element(shop_element,
                                   By.CLASS_NAME, 'c-item__name')
        name = name_element.text if name_element is not None else ''

        # If this item is already in items, just add 1 to quantity.
        if name in shop_items_dict:
            shop_items_dict[name]['qty'] += 1
            continue

        # Get the cost.
        cost_element = get_element(shop_element,
                                   By.CSS_SELECTOR, '.c-shop-box__cost > .c-resource > span')
        cost = cost_element.text if name_element is not None else '-1'

        # Get the image.
        image_element = get_element(shop_element,
                                    By.CSS_SELECTOR, '.c-item__pic > img')
        image_url = image_element.get_attribute(
            'src') if image_element is not None else ''

        # Append item.
        shop_items_dict[name] = {
            'item': name,
            'img_url': image_url,
            'qty': 1,
            'value': int(cost),
        }

    # Convert to list.
    shop_items = list(shop_items_dict.values())

    # Query all equipment and update quantity to 0 if not in shop_items
    # Note: I looked into options to only update those not existing in shop but this option
    # simply gave the best performance. Setting all to 0 works just fine.
    db_session.query(Equipment).update({Equipment.shop_qty: 0})

    # For each item in the shop, check that it exists and update qty or insert if it doesn't exist
    for item in shop_items:
        # Check for item in equipment
        query_items = db_session.query(Equipment).filter(
            Equipment.name == item["item"]).limit(1).one_or_none()

        # if item does not exist, add it. If item exists, update qty and value
        if query_items is None:
            db_session.add(Equipment(
                name=item["item"], url="", shop_qty=item["qty"], shop_val=item["value"], we_val=item["value"] * 2))

        else:
            db_session.query(Equipment).filter(Equipment.name == item["item"]).update(
                {Equipment.shop_qty: item["qty"], Equipment.shop_val: item["value"]})

    # Commit to db
    db_session.commit()
    print("commit complete")

    # Uncomment to make a file for all items.
    # with open('test.txt', 'w+') as handle:
    #     handle.writelines([f'item: {i["item"]} | qty: {i["qty"]} | value: {i["value"]} | type: {i["type"]} | img_url: {i["img_url"]}\r\n' for i in shop_items])


def warzone_scan():
    for zone in zones:
        print(f"Scanning warzone {zones[zone]}")

        # Get the webpage.
        url = f'https://www.ninjamanager.com/clans/warzones/{zones[zone]}'
        browser.get(url)

        # Wait for the container (so we do not have a hard coded wait).
        wait_for_element(By.CLASS_NAME, 'p-warzones__squares')

        # Get the boss elements and loop through them.
        all_bosses = get_elements(browser, By.CLASS_NAME, 'p-warzones-grid__text')
        for boss in all_bosses:
            # If not defeated, just continue.
            if (boss.text != 'DEFEATED' and boss.text != 'ENDED...'):
                continue

            boss = get_element(boss, By.XPATH, '../..')

            # Get the name of the boss from the image.
            image_element = get_element(boss, By.TAG_NAME, 'img')
            name = ''
            if image_element is not None:
                image_url = image_element.get_attribute('src')
                name = image_url.split('/')[-1][:-4].split('-')
                name = ' '.join(name).title()

            # Get grid ID.
            grid_id = boss.get_attribute('data-gridid')
            # Update the warzone.
            db_session.query(Warzone) \
                .filter(Warzone.grid == grid_id) \
                .filter(Warzone.zone == zone) \
                .update({
                    Warzone.defeated: 1,
                    Warzone.name: name,
                    Warzone.image: url
                })
            
            # Check targets
            boss = db_session.query(Warzone) \
                .filter(Warzone.grid == grid_id).first()
                        
            db_session.query(Target).filter(Target.boss_id == boss.id).delete()

            db_session.commit()

        # Get all alive bosses.
        alive_bosses = get_elements(
            browser, By.CLASS_NAME, 'p-warzones-grid__health-fill')
        print(f'Total of {len(alive_bosses)} bosses are alive')

        # Loop through the bosses.
        for idx, boss in enumerate(alive_bosses):
            # Check if stale element.
            try:
                boss.get_attribute('style')
            except:
                boss = get_elements(browser, By.CLASS_NAME,
                                    'p-warzones-grid__health-fill')[idx]

            # Get the grid ID.
            grid_id_element = get_element(boss, By.XPATH, '../../..')
            grid_id = grid_id_element.get_attribute(
                'data-gridid') if grid_id_element is not None else ''

            # Get the health.
            health = boss.get_attribute('style').split()[-1][:-2]

            # Get the boss name.
            image_container = get_element(boss, By.XPATH, '../..')
            name = ''
            boss_image_url = ''
            if image_container is not None:
                image_element = get_element(
                    image_container, By.TAG_NAME, 'img')
                if image_element is not None:
                    boss_image_url = image_element.get_attribute('src')
                    name = boss_image_url.split('/')[-1][:-4].split('-')
                    name = ' '.join(name).title()

            # Try to click the boss.
            try:
                image_container.click()
                time.sleep(3)
                image_container.click()
            except Exception as e:
                print(e)
                print(f'exception here, cant click boss: {name}')
                continue

            # Get the rewards container.
            rewards_container = wait_for_element(
                By.CLASS_NAME, 'p-warzones-rewards')
            if rewards_container is None:
                continue

            # Get items/values.
            item_elements = get_elements(
                rewards_container, By.CLASS_NAME, 'c-item__name')
            value_elements = get_elements(
                rewards_container, By.CLASS_NAME, 'h-color-clan')

            item_names = [item.text for item in item_elements]
            value_names = [value.text for value in value_elements]

            # Create an item string with the value of the item.
            items_str = ', '.join(
                [f'{item_names[i]}${value_names[i]}' for i, _ in enumerate(value_names)])

            # Get all of the damage amounts
            clan_name_elements = get_elements(
                rewards_container, By.CLASS_NAME, 'c-user-box__user')
            damage_elements = get_elements(
                rewards_container, By.CLASS_NAME, 'p-warzones-rewards-box__health-text')

            clan_names = [item.text for item in clan_name_elements]
            damage_names = [value.text for value in damage_elements]

            # Create an item string with the value of the item.
            damage_str = ', '.join(
                [f'{clan_names[i]}${damage_names[i]}' for i, _ in enumerate(damage_names)])

            random_check = get_elements(
                rewards_container, By.CLASS_NAME, 'p-warzones-rewards-box__place')[0].text == '?'
            
            # Get all the veins.
            veins = []
            veins_name = []
            vein_container = get_element(
                browser, By.CLASS_NAME, 'p-warzones__squares')
            if vein_container is not None:
                veins = get_elements(
                    vein_container, By.CSS_SELECTOR, '.c-item__pic > img')

            # Loop through the veins.
            for vein in veins:
                src = vein.get_attribute('src')
                name_vein = src.split('/')[-1][:-4].split("-")
                name_vein = ' '.join(name_vein).title()
                veins_name.append(name_vein)

            veins_str = ', '.join(veins_name)

            # Get the condition and duration.
            infos = get_elements(browser, By.CLASS_NAME,
                                 'p-warzones-sidebar__info')
            condition = ''
            duration = ''
            for info in infos:
                if 'Approximately' in info.text:
                    condition = info.text.partition(' ')[2]
                elif 'Battle ends' in info.text:
                    duration = info.text
                elif 'Low Health' in info.text:
                    condition = 'Low Health!'

            # Get the boss from the database.
            temp_boss = db_session \
                .query(Warzone)\
                .filter(Warzone.grid == grid_id) \
                .filter(Warzone.zone == zone) \
                .first()

            # If grid ID updated with new name.
            if temp_boss.name != name:
                print('Grid ID updated with new boss')
                temp_boss.reported = 0

            # Set variables collected for the boss.
            temp_boss.health_percentage = health
            temp_boss.name = name
            temp_boss.image = boss_image_url
            temp_boss.defeated = 0
            temp_boss.rewards = items_str
            temp_boss.veins = veins_str
            temp_boss.condition = condition
            temp_boss.duration = duration
            temp_boss.damage = damage_str
            temp_boss.random = random_check

            # Commit the boss changes.
            #db_session.query(Warzone).filter(Warzone.grid == grid_id).filter(Warzone.zone == zone).update({Warzone.health_percentage: health, Warzone.name: name, Warzone.image: url_img, Warzone.defeated: 0, Warzone.rewards: items_str, Warzone.veins: veins_str, Warzone.condition: condition, Warzone.duration: duration})
            db_session.commit()
            # print("commit complete")

            # Go back to the warzone page and make sure to wait for the element again.
            browser.get(url)
            wait_for_element(By.CLASS_NAME, 'p-warzones__squares')


if __name__ == '__main__':
    # Load the dotenv and get specific values within.
    load_dotenv()
    user = os.getenv("NM_USER")
    password = os.getenv("NM_PASS")  # getpass.getpass()
    dbuser = os.getenv("DB_USER")
    dbpass = os.getenv("DB_PASS")
    database = os.getenv("DB_NAME")
    dbport = os.getenv("DB_PORT")

    # Start the database engine/session.
    engine = sqlalchemy.create_engine(
        f"postgres://{dbuser}:{dbpass}@localhost:{dbport}/{database}", echo=False)

    Session = sessionmaker(bind=engine)
    db_session = Session()

    # tmin = 1200  # minimum time for the loop, in seconds
    # tmax = 1320  # maximum time for the loop, in seconds

    # Start headless chrome.
    opts = Options()
    opts.add_argument('--headless')
    opts.add_argument('--no-sandbox')
    opts.add_argument('--disable-dev-shm-usage')
    browser = Chrome(options=opts)

    # Login.
    logged_in = login(user, password, debug=True)
    if logged_in:
        print('Logged in!')
    else:
        print('Failed login! Exiting program.')
        browser.close()
        sys.exit(1)

    # Get the asyncio event loop and run scraping functions infinitely.
    tasks = []
    try:
        loop = asyncio.get_event_loop()
        tasks = [
            # set to slightly off of 1 minute in case Udon actually looks into the bot haha
            loop.create_task(call_func_every_x_seconds(890, warzone_scan)),
            #loop.create_task(call_func_every_x_seconds(12588, shop_scan))
        ]
        loop.run_until_complete(asyncio.wait(tasks))
    finally:
        # Close loop and browser after done. Has to be in try due to other errors.
        try:
            for task in tasks:
                task.cancel()
            loop.close()
            browser.close()
        except Exception as e:
            print(e)            
            pass

    #url = f'https://www.ninjamanager.com/clans/warzones/land-of-iron'
    #main_page = browser.get(url)
    #alive = browser.find_elements(By.CLASS_NAME, 'p-warzones-grid__health-fill')
