Byakugan
========

Byakugan is a Discord Bot that scans the NinjaManager game.

Current Features
----------------
- Scans for any change in the warzone map
- Report specific warzone returning all bosses that are alive

Initial Setup
-------------

On Ubuntu:

- apt update
- apt install postgresql python3-pip python3-psycopg2
- install google-chrome
- Download chromiumdriver and move it to the PATH folder
- git clone https://gitlab.com/guilindner/byakugan.git
- cd byakugan
- pip install -r requirements.txt --user
- sudo -u postgres createdb ninjamanager
- sudo -u postgres psql
- \password postgres
- exit
- cp dotenv .env
- Add information to the .env file
- python3 models.py


Now using tmux, leave both apps running
- tmux
- python3 web_scrap.py
- python3 byakugan.py

For long running term
- cp both nm_byakugan and nm_webscrap to /lib/systemd/system/
- systemctl enable nm_byakugan
- systemctl enable nm_webscrap

Check the database:
- sudo -u postgres psql
